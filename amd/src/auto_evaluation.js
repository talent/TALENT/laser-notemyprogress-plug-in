define([
  "local_notemyprogress/vue",
  "local_notemyprogress/vuetify",
  "local_notemyprogress/axios",
  "local_notemyprogress/moment",
  "local_notemyprogress/pagination",
  "local_notemyprogress/chartstatic",
  "local_notemyprogress/pageheader",
  "local_notemyprogress/modulesform",
  "local_notemyprogress/helpdialog",
  "local_notemyprogress/alertify",
], function (
  Vue,
  Vuetify,
  Axios,
  Moment,
  Pagination,
  ChartStatic,
  PageHeader,
  ModulesForm,
  HelpDialog,
  Alertify
) {
  "use strict";

  function init(content) {
    // //console.log(content);
    Vue.use(Vuetify);
    Vue.component("pagination", Pagination);
    Vue.component("chart", ChartStatic);
    Vue.component("pageheader", PageHeader);
    Vue.component("modulesform", ModulesForm);
    Vue.component("helpdialog", HelpDialog);
    let vue = new Vue({
      delimiters: ["[[", "]]"],
      el: "#auto_evaluation",
      vuetify: new Vuetify(),
      data() {
        return {
          strings: content.strings,
          groups: content.groups,
          userid: content.userid,
          courseid: content.courseid,
          timezone: content.timezone,
          render_has: content.profile_render,
          loading: false,
          errors: [],
          pages: content.pages,

          indicators: content.indicators,
          resources_access_colors: content.resources_access_colors,
          inverted_time_colors: content.inverted_time_colors,
          inverted_time: content.indicators.inverted_time,
          hours_sessions: content.indicators.hours_sessions,
          sections: content.indicators.sections,
          sections_map: null,
          week_progress: 0,
          resource_access_categories: [],
          resource_access_data: [],
          modules_dialog: false,

          help_dialog: false,
          help_contents: [],
          auto_eval_answers: content.auto_eval_answers,
          saving_loader: false,
          auto_eval_saved: content.auto_eval_saved,
        };
      },
      beforeMount() {},
      mounted() {
        document.querySelector("#auto_evaluation-loader").style.display = "none";
        document.querySelector("#auto_evaluation").style.display = "block";
      },
      methods: {

        action_save_auto_evaluation() {
          if (this.auto_eval_saved) {
            console.log("update")
            this.update_auto_eval();
          } else {
            console.log("save")
            this.save_auto_eval();
          }
        },

        save_auto_eval() {
          var data = {
            action: "saveautoeval",
            userid: this.userid,
            courseid: this.courseid,
            auto_eval_answers: this.auto_eval_answers
          };

          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              //console.log("then");
              if (response.status == 200 && response.data.ok) {
                this.auto_eval_saved = true;
                //console.log(response.data.data.responsemetareflexion.days);
                console.log("if");
                Alertify.success(this.strings.message_save_successful);
                //course_module.modalopened = false;
              } else {
                console.log("else");
                Alertify.error(this.strings.api_error_network);
              }
            })
            .catch((e) => {
              console.log("catch");
              this.saving_loader = false;
              Alertify.error(this.strings.api_error_network);
            })
            .finally(() => {
              console.log("finally");
              this.saving_loader = false;
            });
        },

        update_auto_eval() {
          var data = {
            action: "updateautoeval",
            userid: this.userid,
            courseid: this.courseid,
            auto_eval_answers: this.auto_eval_answers
          };

          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              //console.log("then");
              if (response.status == 200 && response.data.ok) {
                //console.log(response.data.data.responsemetareflexion.days);
                console.log("if");
                Alertify.success(this.strings.message_update_successful);
                //course_module.modalopened = false;
              } else {
                console.log("else");
                Alertify.error(this.strings.api_error_network);
              }
            })
            .catch((e) => {
              console.log("catch");
              this.saving_loader = false;
              Alertify.error(this.strings.api_error_network);
            })
            .finally(() => {
              console.log("finally");
              this.saving_loader = false;
            });
        },

        addLogsIntoDB(action, objectName, objectType, objectDescription) {
          let data = {
            courseid: content.courseid,
            userid: content.userid,
            action: "addLogs",
            sectionname: "STUDENT_STUDY_SESSIONS",
            actiontype: action,
            objectType: objectType,
            objectName: objectName,
            currentUrl: document.location.href,
            objectDescription: objectDescription,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
              }
            })
            .catch((e) => {});
        },
        get_help_content() {
          var help_contents = [];
          var help = new Object();
          help.title = this.strings.page_title_auto_eval;
          help.description = this.strings.description_auto_eval;
          help_contents.push(help);
          return help_contents;
        },
      },
    });
  }

  return {
    init: init,
  };
});
