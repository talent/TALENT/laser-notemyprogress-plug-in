define([
        'highcharts',
        'highcharts/highcharts-3d',
        'highcharts/highcharts-more',
        'highcharts/modules/heatmap',
        'highcharts/modules/exporting',
        'highcharts/modules/export-data',
        'highcharts/modules/accessibility',
        'highcharts/modules/no-data-to-display',
        ],
    function(Highcharts) {
    return {
        template: `<div v-bind:id="container"></div>`,
        props: ['container', 'chart', 'lang'],
        data() {
            return {
            }
        },
        mounted() {
            let self = this;
            (this.lang) && Highcharts.setOptions({
                lang: this.lang,
                credits: { enabled: false },
                exporting: {
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                text: this.lang.downloadPNG,
                                onclick: function () {
                                    this.exportChart({
                                        type: 'image/png'
                                    });
                                    self.$parent.$root.addLogsIntoDB("downloaded", self.$el.id, "chart", "A chart");
                                }
                            },{
                                text: this.lang.downloadJPEG,
                                onclick: function () {
                                    this.exportChart({
                                        type: 'image/jpeg'
                                    });
                                    self.$parent.$root.addLogsIntoDB("downloaded", self.$el.id, "chart", "A chart");
                                }
                            },{
                                text: this.lang.downloadPDF,
                                onclick: function () {
                                    this.exportChart({
                                        type: 'application/pdf'
                                    });
                                    self.$parent.$root.addLogsIntoDB("downloaded", self.$el.id, "chart", "A chart");
                                }
                            },{
                                text: this.lang.downloadSVG,
                                onclick: function () {
                                    this.exportChart({
                                        type: 'image/svg+xml'
                                    });
                                    self.$parent.$root.addLogsIntoDB("downloaded", self.$el.id, "chart", "A chart");
                                }
                            },{
                                text: this.lang.downloadXLS,
                                onclick: function () {
                                    this.downloadXLS();
                                    self.$parent.$root.addLogsIntoDB("downloaded", self.$el.id, "chart", "A chart");
                                }
                            },{
                                text: this.lang.downloadCSV,
                                onclick: function () {
                                    this.downloadCSV();
                                    self.$parent.$root.addLogsIntoDB("downloaded", self.$el.id, "chart", "A chart");
                                }
                            }],
                            symbol: 'menuball',
                            symbolStroke: '#118AB2'
                        }
                    }
                }
            });
            this._highchart = Highcharts.chart(this.container, this.chart);

            Highcharts.event
            let chartLegends = this._highchart.legend.allItems;
            let action = "";
            let objectName = "";
            let objectType = "";
            let objectDescription = "";
            chartLegends.forEach(filter => {
                filter.legendGroup.element.addEventListener('click', function() {
                    action = "filtered";
                    objectName = self.$el.id;
                    objectType = "chart";
                    objectDescription = "Filtered a part of the "+objectName+" chart";
                    if(typeof self.$parent.$root.addLogsIntoDB === "function") {
                        self.$parent.$root.addLogsIntoDB(action, objectName, objectType, objectDescription);
                    }
                })
            })
        },
        watch: {
            chart: {
                deep: true,
                handler(chart) {
                    this._highchart.update(chart);
                    let chartLegends = this._highchart.legend.allItems;
                    let self = this;
                    let action = "";
                    let objectName = "";
                    let objectType = "";
                    let objectDescription = "";
                    chartLegends.forEach(filter => {
                        filter.legendGroup.element.addEventListener('click', function() {
                            action = "filtered";
                            objectName = self.$el.id;
                            objectType = "chart";
                            objectDescription = "Filtered a part of the "+objectName+" chart";
                            if(typeof self.$parent.$root.addLogsIntoDB === "function") {
                                self.$parent.$root.addLogsIntoDB(action, objectName, objectType, objectDescription);
                            }
                        })
                    })
                },
            }
        }

    };
});