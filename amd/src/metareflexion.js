define([
  "local_notemyprogress/vue",
  "local_notemyprogress/vuetify",
  "local_notemyprogress/axios",
  "local_notemyprogress/alertify",
  "local_notemyprogress/pagination",
  "local_notemyprogress/chartdynamic",
  "local_notemyprogress/pageheader",
], function (
  Vue,
  Vuetify,
  Axios,
  Alertify,
  Pagination,
  ChartStatic,
  Pageheader
) {
  "use strict";

  // function clone(obj) {
  //   return JSON.parse(JSON.stringify(obj));
  // }

  function init(content) {
    Vue.use(Vuetify);
    Vue.component("pagination", Pagination);
    Vue.component("chart", ChartStatic);
    Vue.component("pageheader", Pageheader);

    const vue = new Vue({
      delimiters: ["[[", "]]"],
      el: "#metareflexion",
      vuetify: new Vuetify(),
      data: {
        test: true,
        module_groups: content.module_groups,
        indicators: content.indicators,
        resources_access_colors: content.resources_access_colors,
        inverted_time_colors: content.inverted_time_colors,
        inverted_time: content.indicators.inverted_time,
        hours_sessions: content.indicators.hours_sessions,
        sections: content.indicators.sections,
        sections_map: null,
        week_progress: 0,
        resource_access_categories: [],
        resource_access_data: [],
        modules_dialog: false,
        help_dialog: false,
        help_contents: [],

        disabled_form: false,
        groups: content.groups,
        students_planification: content.students_planification,
        selected_week: null,
        paginator_week: null,
        saved_planification: false,
        strings: content.strings,
        userid: content.userid,
        courseid: content.courseid,
        loading: false,
        compare_with_course: false,
        current_week: content.cmcurrentweeknew,
        week_schedule: content.week_schedule,

        data_report_meta_days: content.data_report_days,
        data_report_meta_hours: content.data_report_hours,
        data_report_meta_goals: content.data_report_goals,

        data_report_meta_questions: content.data_report_hours_questions,

        // data_report_meta_classroom: content.data_report_classroom.average_classroom,
        //data_report_meta_of_classroom: content.data_report_classroom.average_of_classroom,
        data_report_meta_last_week: content.report_last_week,

        status_planning: content.status_planning,
        course_report_hours: content.course_report_hours,
        past_week: content.lastweek,
        render_has: content.profile_render,
        dialog: false,
        days_week: [
          content.strings.currentweek_day_lun,
          content.strings.currentweek_day_mar,
          content.strings.currentweek_day_mie,
          content.strings.currentweek_day_jue,
          content.strings.currentweek_day_vie,
          content.strings.currentweek_day_sab,
          content.strings.currentweek_day_dom,
        ],
        active_tab: 0,
        icons: {
          calendar: content.calendar_icon,
        },
        tabs_header: [
          {
            name: content.strings.tab_1,
            id: 1,
            teacher_can_view: false,
            student_can_view: true,
          },
          {
            name: content.strings.tab_2,
            id: 2,
            teacher_can_view: false,
            student_can_view: true,
          },
        ],
        hours_committed: 0,
        id_committed: null,
        pages: content.pages,
        active_tab: null,
        chart_metareflexion_options: {
          maintainAspectRatio: false,
          legend: {
            display: false,
          },
          scales: {
            xAxes: [
              {
                ticks: {
                  beginAtZero: true,
                  callback: function (value) {
                    if (Number.isInteger(value)) {
                      return value;
                    }
                  },
                  suggestedMin: 0,
                  suggestedMax: 5,
                },
              },
            ],
          },
        },

        img_no_data: content.image_no_data,
      },

      mounted() {
        document.querySelector("#sr-loader").style.display = "none";
        document.querySelector("#metareflexion").style.display = "block";
        if (!this.past_week.classroom_hours) {
          this.past_week.classroom_hours = 0;
        }
        if (!this.past_week.classroom_hours) {
          this.past_week.hours_off_course = 0;
        }
        this.pages.forEach((page) => {
          if (page.selected) {
            this.selected_week = page;
            this.paginator_week = page;
          }
        });

        setTimeout(function () {
          vue.setGraphicsEventListeners();
        }, 500);
        vue.setGraphicsEventListeners();
        // if (typeof this.past_week.id != "undefined") {
        //   this.disabled_form = true;
        // }
      },

      computed: {
        progress() {
          var count_all = 0;
          var count_finished = 0;
          Object.keys(this.module_groups).map((key) => {
            let group = this.module_groups[key];
            count_all += group.count_all;
            count_finished += group.count_finished;
          });
          let average = (count_finished * 100) / count_all;
          average = Number.isNaN(average) ? 0 : average.toFixed(0);
          return average;
        },

        isDisabledBtnLastWeek() {
          return (
            this.data_report_meta_questions.questions[1].answer_selected ===
              null ||
            this.data_report_meta_questions.questions[2].answer_selected ===
              null ||
            this.data_report_meta_questions.questions[3].answer_selected ===
              null ||
            this.data_report_meta_questions.questions[4].answer_selected ===
              null
          );
        },

        hasLastWeek() {
          var last_week = false;
          this.pages.forEach((page) => {
            if (page.is_current_week && page.number > 1) {
              last_week = true;
            }
          });
          return last_week;
        },

        isDisabledQuestions() {
          //if today <= date_end (weekstart + 7) OR no shedule return true else return false
          return this.paginator_week != null
            ? !(
                this.week_schedule[this.paginator_week.weekcode] &&
                !this.paginator_week.is_current_week
              )
            : true;
        },

        // isDisabledBtnCurrentWeek() {
        //   return (
        //     this.current_week[0].weekly_schedules_hours > 0 &&
        //     (this.current_week[0].weekly_schedules_days.lun == true ||
        //       this.current_week[0].weekly_schedules_days.mar == true ||
        //       this.current_week[0].weekly_schedules_days.mie == true ||
        //       this.current_week[0].weekly_schedules_days.jue == true ||
        //       this.current_week[0].weekly_schedules_days.vie == true ||
        //       this.current_week[0].weekly_schedules_days.sab == true ||
        //       this.current_week[0].weekly_schedules_days.dom == true)
        //   );
        // },
      },

      methods: {
        get_modules(day, cmid) {
          return this.current_week[0].weekly_schedules_days.days_planned[
            day
          ].includes(cmid);
        },

        update_interactions(week) {
          this.loading = true;
          this.errors = [];
          let data = {
            action: "worksessions",
            userid: this.userid,
            courseid: this.courseid,
            weekcode: week.weekcode,
            profile: this.render_has,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
                this.hours_sessions = response.data.data.indicators.sessions;
                this.session_count = response.data.data.indicators.count;
                this.inverted_time = response.data.data.indicators.time;
              } else {
                this.error_messages.push(this.strings.error_network);
              }
            })
            .catch((e) => {
              this.errors.push(this.strings.api_error_network);
            })
            .finally(() => {
              this.loading = false;
              //Ici, la page a fini de charger
              vue.addLogsIntoDB(
                "viewed",
                "week_" + week.weekcode,
                "week_section",
                "Week section that allows you to obtain information on a specific week"
              );
              vue.setGraphicsEventListeners();
            });
          return this.data;
        },

        convert_time(time) {
          time *= 3600; // pasar las horas a segundos
          let h = this.strings.hours_short;
          let m = this.strings.minutes_short;
          let s = this.strings.seconds_short;
          let hours = Math.floor(time / 3600);
          let minutes = Math.floor((time % 3600) / 60);
          let seconds = Math.floor(time % 60);
          let text;
          if (hours >= 1) {
            if (minutes >= 1) {
              text = `${hours}${h} ${minutes}${m}`;
            } else {
              text = `${hours}${h}`;
            }
          } else if (minutes >= 1) {
            if (seconds >= 1) {
              text = `${minutes}${m} ${seconds}${s}`;
            } else {
              text = `${minutes}${m}`;
            }
          } else {
            text = `${seconds}${s}`;
          }
          return text;
        },

        get_help_content() {
          let contents = [];
          contents.push({
            title: this.strings.section_help_title,
            description: this.strings.section_help_description,
          });
          return contents;
        },

        build_inverted_time_chart() {
          //console.log("enter build_inverted_time_chart ");
          //console.log(this.students_planification);
          //console.log("this.data_report_meta_hours = ");
          //console.log(this.data_report_meta_hours);
          let chart = new Object();
          let meta = new Object();
          meta = this.chartdata_hours_week_dedication();
          //console.log("meta = ");
          //console.log(meta);
          let invest = [
            {
              name: meta.labels[2],
              y: meta.datasets[0].data[2],
            },
            {
              name: meta.labels[0],
              y: meta.datasets[0].data[0],
            },
            {
              name: meta.labels[1],
              y: meta.datasets[0].data[1],
            },
          ];
          //console.log("invest = ");
          //console.log(invest);
          chart.chart = {
            type: "bar",
            backgroundColor: null,
            style: { fontFamily: "poppins" },
          };
          chart.title = { text: null };
          chart.colors = this.inverted_time_colors;
          chart.xAxis = {
            type: "category",
            crosshair: true,
          };
          chart.yAxis = {
            title: {
              text: this.strings.inverted_time_chart_x_axis,
            },
          };
          chart.tooltip = {
            shared: true,
            useHTML: true,
            formatter: function () {
              let category_name = this.points[0].key;
              let time = vue.convert_time(this.y);
              return `<b>${category_name}: </b>${time}`;
            },
          };
          chart.legend = {
            enabled: false,
          };

          chart.series = [
            {
              colorByPoint: true,
              data: invest,
            },
          ];

          // chart.series.update(
          //   {
          //     colorByPoint: true,
          //     data: invest,
          //   },
          //   false
          // ); //true / false to redraw
          //console.log("this.inverted_time.data = ");
          //console.log(this.inverted_time.data);

          //console.log("invest = ");
          //console.log(invest);
          //console.log("end build_inverted_time_chart");
          return chart;
        },

        get_goal(goal_id) {
          return this.current_week[0].weekly_schedules_goals.includes(goal_id);
        },

        update_goal(goal_id, event) {
          //console.log(this.data_report_meta_goals);
          if (event) {
            this.current_week[0].weekly_schedules_goals.push(goal_id);
          } else {
            const i =
              this.current_week[0].weekly_schedules_goals.indexOf(goal_id);
            //console.log(i);
            this.current_week[0].weekly_schedules_goals.splice(i, 1);
          }
          //console.log(this.data_report_meta_goals);
        },

        update_module(day, module_id, event) {
          if (event) {
            this.current_week[0].weekly_schedules_days.days_planned[day].push(
              module_id
            );
          } else {
            const i =
              this.current_week[0].weekly_schedules_days.days_planned[
                day
              ].indexOf(module_id);
            //console.log(i);
            this.current_week[0].weekly_schedules_days.days_planned[day].splice(
              i,
              1
            );
          }
          //console.log(this.current_week[0].days_committed[day]);
        },

        subtitle_reports_hours_label() {
          let label = "";
          if (this.render_has == "teacher") {
            label = this.strings.subtitle_reports_hours_teacher;
          } else {
            label = this.strings.subtitle_reports_hours_student;
          }
          return label;
        },

        subtitle_reports_days_student_label() {
          let label = "";
          if (this.render_has == "teacher") {
            label = this.strings.subtitle_reports_days_teacher;
          } else {
            label = this.strings.subtitle_reports_days_student;
          }
          return label;
        },

        translate_name(name, prefix) {
          var index_name = prefix + name;
          if (!(typeof this.strings[index_name] == "undefined")) {
            name = this.strings[index_name];
          }
          return name;
        },

        get_interactions(week) {
          this.loading = true;
          var validresponse = false;
          this.errors = [];
          var data = {
            action: "metareflexionrepotgetweek",
            userid: this.userid,
            courseid: this.courseid,
            weekcode: week.weekcode,
            profile: this.render_has,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
                //console.log("if");
                this.paginator_week = week;
                validresponse = true;

                this.data_report_meta_goals =
                  response.data.data.interactions_goals;
                this.data_report_meta_days =
                  response.data.data.interactions_days;
                this.data_report_meta_hours =
                  response.data.data.interactions_hours;
                this.data_report_meta_questions =
                  response.data.data.interactions_questions;
                this.course_report_hours =
                  response.data.data.course_report_hours;

                //this.students_planification = response.data.data.students_planification;
                this.status_planning = response.data.data.status_planning;
              } else {
                //console.log("else");
                this.errors.push(this.strings.api_error_network);
              }
            })
            .catch((e) => {
              //console.log("catch");
              this.errors.push(this.strings.api_error_network);
            })
            .finally(() => {
              //console.log("finally");
              this.loading = false;
            });

          if (!validresponse) {
          }

          return validresponse;
        },

        get_interacions_last_week(week) {
          //console.log("enter get_interacions_last_week ");
          this.loading = true;
          var validresponse = false;
          this.errors = [];
          var data = {
            action: "metareflexionreportlastweek",
            userid: this.userid,
            courseid: this.courseid,
            weekcode: week.weekcode,
            profile: this.render_has,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
                this.paginator_week = week;
                validresponse = true;
                this.data_report_meta_classroom =
                  response.data.data.average_hours_clases.average_classroom;
                this.data_report_meta_of_classroom =
                  response.data.data.average_hours_clases.average_of_classroom;
                this.data_report_meta_last_week =
                  response.data.data.report_last_week;
              } else {
                this.errors.push(this.strings.api_error_network);
              }
            })
            .catch((e) => {
              this.errors.push(this.strings.api_error_network);
            })
            .finally(() => {
              this.loading = false;
            });
          return validresponse;
        },

        get_week_dates(week) {
          return `${week.weekstart} ${this.strings.tv_to} ${week.weekend}`;
        },

        chartdata_hours_week_dedication() {
          var data = new Object();
          data.datasets = [];

          let inverted = `${this.strings.myself} ${this.strings.inverted_time}`;
          let planified = `${this.strings.myself} ${this.strings.planified_time}`;

          data.labels = [inverted, planified];
          var dataset = new Object();
          dataset.label = "Horas";

          dataset.data = [
            parseFloat(this.data_report_meta_hours.hours_worked),
            parseInt(this.data_report_meta_hours.hours_planned),
          ];
          dataset.backgroundColor = ["#ffa700", "#a0c2fa"];
          dataset.borderWidth = 0;
          data.datasets.push(dataset);

          //if (this.render_has == "student" && this.compare_with_course) {
          data.labels.splice(1, 0, this.strings.inverted_time_course);
          //data.labels.splice(3, 0, this.strings.planified_time_course);
          dataset.data.splice(
            1,
            0,
            parseFloat(this.course_report_hours.hours_worked)
          );
          // dataset.data.splice(
          //   3,
          //   0,
          //   parseFloat(this.course_report_hours.hours_planned)
          // );
          dataset.backgroundColor.splice(1, 0, "#ffa700");
          //dataset.backgroundColor.splice(3, 0, "#a0c2fa");
          //}
          return data;
        },

        action_save_metareflexion(course_module) {
          if (course_module.weekly_schedules_id) {
            this.update_metareflexion(course_module);
          } else {
            this.save_metareflexion_new(course_module);
          }
          this.get_interaction_group(this.paginator_week);
          //this.saved_planification = true;
        },

        updated_metareflexion() {
          if (
            this.selected_week.weekcode == this.paginator_week.weekcode &&
            this.saved_planification
          ) {
            this.get_interactions(this.paginator_week);
            this.saved_planification = false;
          }
        },

        get_selected_days(week) {
          var filtered_days = [];
          Object.keys(week).forEach((day, index) => {
            if (week[day]) {
              filtered_days.push(day);
            }
          });
          return filtered_days.join();
        },

        update_metareflexion(course_module) {
          var data = {
            action: "updatemetareflexion",
            metareflexionid: course_module.weekly_schedules_id,
            // days: days_committed,
            hours: this.current_week[0].weekly_schedules_hours.hours_planned,
            goals: this.current_week[0].weekly_schedules_goals,
            days: JSON.stringify(
              this.current_week[0].weekly_schedules_days.days_planned
            ),
            courseid: this.courseid,
            weekcode: course_module.weekcode,
            userid: this.userid,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
                Alertify.success(this.strings.update_planification_success);
                course_module.modalopened = false;
              } else {
              }
            })
            .catch((e) => {
              //console.log("catch");
              this.saving_loader = false;
              Alertify.error("The note could not be saved...");
            });
        },

        save_metareflexion_new(course_module) {
          var data = {
            action: "savemetareflexion",
            userid: this.userid,
            courseid: this.courseid,
            weekcode: course_module.weekcode,

            hours: this.current_week[0].weekly_schedules_hours.hours_planned,
            goals: this.current_week[0].weekly_schedules_goals,
            days: JSON.stringify(
              this.current_week[0].weekly_schedules_days.days_planned
            ),
          };

          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              //console.log("then");
              if (response.status == 200 && response.data.ok) {
                course_module.weekly_schedules_id =
                  response.data.data.responsemetareflexion.id;
                //console.log(response.data.data.responsemetareflexion.days);
                //console.log("if");
                Alertify.success(this.strings.save_planification_success);
                course_module.modalopened = false;
              } else {
                //console.log("else status");
                Alertify.error(this.strings.api_error_network);
              }
            })
            .catch((e) => {
              //console.log("catch");
              this.saving_loader = false;
              Alertify.error(this.strings.api_error_network);
            })
            .finally(() => {
              //console.log("finally");
              this.saving_loader = false;
            });
        },

        actions_last_week() {
          if (this.data_report_meta_questions.id) {
            //console.log("update");
            this.update_last_week();
          } else {
            //console.log("save");
            this.save_last_week();
          }
        },

        save_last_week() {
          var data_params = {
            action: "savelastweek",
            userid: this.userid,
            courseid: this.courseid.toString(),
            weekcode: this.paginator_week.weekcode,
            classroom_hours: this.data_report_meta_questions.classroom_hours,
            hours_off: this.data_report_meta_questions.hours_off_course,
            objectives_reached:
              this.data_report_meta_questions.questions[1].answer_selected,
            previous_class:
              this.data_report_meta_questions.questions[2].answer_selected,
            benefit:
              this.data_report_meta_questions.questions[3].answer_selected,
            feeling:
              this.data_report_meta_questions.questions[4].answer_selected,
          };
          //console.log("data = ");
          //console.log(data_params);

          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data_params,
          })
            .then((response) => {
              //console.log("then");
              if (response.status == 200 && response.data.ok) {
                //console.log("if");
                this.data_report_meta_questions.id =
                  response.data.data.response_save_last_week.id;
                Alertify.success(this.strings.last_week_created);
                //this.disabled_form = true;
              } else {
                //console.log("else");
              }
            })
            .catch((e) => {
              //console.log("catch");
              Alertify.error(this.strings.api_error_network);
            });
        },

        update_last_week() {
          var data_params = {
            action: "updatelastweek",
            userid: this.userid,
            courseid: this.courseid,
            lastweekid: this.data_report_meta_questions.id,
            classroom_hours: this.data_report_meta_questions.classroom_hours,
            hours_off: this.data_report_meta_questions.hours_off_course,
            objectives_reached:
              this.data_report_meta_questions.questions[1].answer_selected,
            previous_class:
              this.data_report_meta_questions.questions[2].answer_selected,
            benefit:
              this.data_report_meta_questions.questions[3].answer_selected,
            feeling:
              this.data_report_meta_questions.questions[4].answer_selected,
          };

          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data_params,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
                Alertify.success(this.strings.last_week_update);
              }
            })
            .catch((e) => {
              Alertify.error(this.strings.api_error_network);
            });
        },

        get_icon(days_planned_trabajados, position) {
          var icon_name = "remove";

          if (days_planned_trabajados.days_planned[position]) {
            if (days_planned_trabajados.days_worked[position] > 0) {
              icon_name = "mdi-thumb-up-outline";
            } else {
              icon_name = "mdi-mdi-thumb-down-outline";
            }
          }

          return icon_name;
        },

        get_help_content() {
          var helpcontents = [];
          if (this.active_tab == 0) {
            var help = new Object();
            help.title = this.strings.currentweek_card_title;
            help.description = this.strings.currentweek_description_student;
            helpcontents.push(help);
          } else if (this.active_tab == 1) {
            var help = new Object();
            help.title = this.strings.subtitle_reports_hours;
            help.description = this.strings.description_reports_hours_student;
            helpcontents.push(help);

            help = new Object();
            help.description = this.strings.description_reports_goals_student;
            helpcontents.push(help);

            help = new Object();
            help.title = this.strings.subtitle_reports_days;
            help.description = this.strings.description_reports_days_student;
            helpcontents.push(help);

            help = new Object();
            help.description = this.strings.description_reports_meta_student;
            helpcontents.push(help);
          }
          return helpcontents;
        },

        is_teacher() {
          let is_teacher = this.render_has == "teacher";
          return is_teacher;
        },

        must_renderize(tab) {
          var render = true;
          if (this.render_has == "teacher") {
            render = tab.teacher_can_view;
          } else {
            render = tab.student_can_view;
          }
          return render;
        },

        get_title_content() {
          return this.strings.title_metareflexion;
        },

        planned_week_summary() {
          var summary = false;
          if (this.selected_week) {
            summary = `<strong class="text-uppercase">${this.strings.pagination_name} ${this.selected_week.number}</strong>${this.strings.planning_week_start}
                           ${this.selected_week.weekstart} ${this.strings.planning_week_end} ${this.selected_week.weekend}
                          `;
          }
          return summary;
        },

        get_interaction_group(week) {
          this.get_interactions(week);
          this.get_interacions_last_week(week);
        },
        setGraphicsEventListeners() {
          let graphics = document.querySelectorAll(".highcharts-container");
          if (graphics.length < 1) {
            setTimeout(vue.setGraphicsEventListeners, 500);
          } else {
            graphics[0].id = "EfficiencyChart";
            graphics.forEach((graph) => {
              graph.addEventListener("mouseenter", vue.addLogsViewGraphic);
            });
          }
        },
        addLogsViewGraphic(e) {
          event.stopPropagation();
          var action = "";
          var objectName = "";
          var objectType = "";
          var objectDescription = "";
          switch (e.target.id) {
            case "EfficiencyChart":
              action = "viewed";
              objectName = "reflection_chart";
              objectType = "chart";
              objectDescription = "A bar chart";
              break;
            default:
              action = "viewed";
              objectName = "";
              objectType = "chart";
              objectDescription = "A chart";
              break;
          }
          vue.addLogsIntoDB(action, objectName, objectType, objectDescription);
        },
        addLogsIntoDB(action, objectName, objectType, objectDescription) {
          let data = {
            courseid: content.courseid,
            userid: content.userid,
            action: "addLogs",
            sectionname: "META_REFLECTION",
            actiontype: action,
            objectType: objectType,
            objectName: objectName,
            currentUrl: document.location.href,
            objectDescription: objectDescription,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
              }
            })
            .catch((e) => {});
        },
      },
    });
  }

  return {
    init: init,
  };
});
