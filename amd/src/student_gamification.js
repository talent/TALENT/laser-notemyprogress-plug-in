define([
  "local_notemyprogress/vue",
  "local_notemyprogress/vuetify",
  "local_notemyprogress/axios",
  "local_notemyprogress/alertify",
  "local_notemyprogress/pageheader",
  "local_notemyprogress/chartdynamic",
], function (Vue, Vuetify, Axios, Alertify, PageHeader, ChartDynamic) {
  "use strict";

  function init(content) {
    // console.log(content);
    Vue.use(Vuetify);
    Vue.component("pageheader", PageHeader);
    Vue.component("chart", ChartDynamic);
    const app = new Vue({
      delimiters: ["[[", "]]"],
      el: "#student_gamification",
      vuetify: new Vuetify(),
      data: {
        strings: content.strings,
        token: content.token,
        render_has: content.profile_render,
        indicators: content.indicators,
        levelInfo: content.indicators.levelInfo,
        activity: content.indicators.activity,
        courseid: content.indicators.cid,
        userid: content.indicators.uid,
        ranking: content.ranking,
        tab: null,
        levelsData: content.levels_data.levelsdata,
        settings: content.levels_data.settings,
        error_messages: [],
        save_successful: false,
      },
      beforeMount() {
        let data = {
          courseid: this.courseid,
          userid: this.userid,
          action: "studentGamificationViewed",
        };
        Axios({
          method: "get",
          url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
          params: data,
        });
      },
      computed: {},
      methods: {
        get_help_content() {
          let help_contents = [];
          let help = new Object();
          help.title = this.strings.help_title;
          help.description = this.strings.help_description;
          help_contents.push(help);
          return help_contents;
        },
        update_dialog(value) {
          this.dialog = value;
        },
        update_help_dialog(value) {
          this.help_dialog = value;
        },
        openHelpSectionModalEvent() {
          this.saveInteraction(
            this.pluginSectionName,
            "viewed",
            "section_help_dialog",
            3
          );
        },
        get_timezone() {
          let information = `${this.strings.change_timezone} ${this.timezone}`;
          return information;
        },
        saveInteraction(component, interaction, target, interactiontype) {
          let data = {
            action: "saveinteraction",
            pluginsection: this.pluginSectionName,
            component,
            interaction,
            target,
            url: window.location.href,
            interactiontype,
            token: content.token,
          };
          Axios({
            method: "post",
            url: `${M.cfg.wwwroot}/local/notemyprogress/ajax.php`,
            data: data,
          })
            .then((r) => {})
            .catch((e) => {});
        },
        table_headers() {
          let headers = [
            { text: this.strings.ranking_text, value: "ranking" },
            { text: this.strings.level, value: "level" },
            { text: this.strings.student, value: "student" },
            { text: this.strings.total, value: "total" },
            { text: this.strings.progress, value: "progress_percentage" },
          ];
          return headers;
        },

        activateRanking() {
          location.reload();
          //console.log(" cid : " + this.courseid + " UID : " + this.userid);
          let data = {
            action: "rankable",
            pluginsection: this.pluginSectionName,
            url: window.location.href,
            token: content.token,
            courseid: this.courseid,
            userid: this.userid,
          };
          Axios({
            method: "post",
            url:
              `${M.cfg.wwwroot}/local/notemyprogress/ajax.php?courseid=` +
              this.courseid +
              "&userid=" +
              this.userid,
            params: data,
          })
            .then((r) => {
              // console.log(r);
            })
            .catch((e) => {});
        },
        chart_spread() {
          let chart = new Object();
          chart.chart = {
            type: "column",
            backgroundColor: null,
          };
          chart.title = {
            text: this.strings.chartTitle,
          };
          chart.colors = ["#118AB2"];
          (chart.xAxis = {
            type: "category",
            labels: {
              rotation: -45,
              style: {
                fontSize: "13px",
                fontFamily: "Verdana, sans-serif",
              },
            },
          }),
            (chart.yAxis = {
              min: 0,
              title: {
                text: this.strings.chartYaxis,
              },
            });
          chart.legend = {
            enabled: false,
          };
          (chart.series = [
            {
              name: null,
              data: this.strings.chart_data, //[["level : 1", 1]],
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: "#FFFFFF",
                align: "right",
                format: "{point.y:.1f}", // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                  fontSize: "13px",
                  fontFamily: "Verdana, sans-serif",
                },
              },
            },
          ]),
            console.log("series: ");
          console.log(this.chart_data);
          return chart;
        },
        setGraphicsEventListeners() {
          console.log("Listeners set");
          let graphics = document.querySelectorAll(".highcharts-container");
          if (graphics.length < 1) {
            setTimeout(app.setGraphicsEventListeners, 500);
          } else {
            graphics[0].id = "SpreadChart";
            graphics.forEach((graph) => {
              graph.addEventListener("mouseenter", app.addLogsViewGraphic);
            });
          }
        },
        addLogsViewGraphic(e) {
          event.stopPropagation();
          var action = "";
          var objectName = "";
          var objectType = "";
          var objectDescription = "";
          switch (e.target.id) {
            case "SpreadChart":
              action = "viewed";
              objectName = "spreading_chart";
              objectType = "chart";
              objectDescription =
                "Bar chart that shows the level repartition in gamification";
              break;
            default:
              action = "viewed";
              objectName = "";
              objectType = "chart";
              objectDescription = "A chart";
              break;
          }
          app.addLogsIntoDB(action, objectName, objectType, objectDescription);
        },
        addLogsIntoDB(action, objectName, objectType, objectDescription) {
          let data = {
            courseid: content.courseid,
            userid: content.userid,
            action: "addLogs",
            sectionname: "STUDENTS_GAMIFICATION",
            actiontype: action,
            objectType: objectType,
            objectName: objectName,
            currentUrl: document.location.href,
            objectDescription: objectDescription,
          };
          Axios({
            method: "get",
            url: M.cfg.wwwroot + "/local/notemyprogress/ajax.php",
            params: data,
          })
            .then((response) => {
              if (response.status == 200 && response.data.ok) {
              }
            })
            .catch((e) => {});
        },
      },
    });
  }
  return {
    init: init,
  };
});
