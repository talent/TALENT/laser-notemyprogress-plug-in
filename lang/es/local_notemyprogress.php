<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_notemyprogress
 * @category    string
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @copyright   2021 Eric Bart <bart.eric@hotmail.com>, Mar Pérez-Sanagustín <mar.perez-sanagustin@irit.fr>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'notemyprogress';

/* Global */
$string['pagination'] = 'Semana:';
$string['graph_generating'] = 'Estamos construyendo el reporte, por favor espere un momento.';
$string['weeks_not_config'] = 'El curso no ha sido configurado por el profesor, por lo que no hay visualizaciones que mostrar.';
$string['pagination_title'] = 'Selección semana';
$string['helplabel'] = 'Ayuda';
$string['exitbutton'] = '¡Entendido!';
$string['no_data'] = 'No hay datos que mostrar';
$string['only_student'] = 'Este reporte es solo para estudiantes';
$string["nmp_send_mail"] = "(Clic para enviar correo)";
$string["nmp_about"] = "Acerca de este Gráfico";
$string["nmp_about_table"] = "Acerca de esta Tabla";
$string["nmp_not_configured"] = "No Configurado";
$string["nmp_activated"] = "Activado";
$string["nmp_disabled"] = "Desactivado";

/* Menú */
$string['menu_main_title'] = "Dashboard Progreso";
$string['menu_sessions'] = 'Sesiones de Estudio';
$string['menu_setweek'] = "Configurar semanas";
$string['menu_time'] = 'Seguimiento de Tiempo';
$string['menu_assignments'] = 'Seguimiento de Tareas';
$string['menu_grades'] = 'Seguimiento de Calificaciones';
$string['menu_quiz'] = 'Seguimiento de Evaluaciones';
$string['menu_dropout'] = 'Seguimiento de estudiantes';
$string['menu_logs'] = "Registros de actividad";
$string['menu_general'] = "Indicadores Generales";
$string['menu_auto_evaluation'] = "Auto-evaluación";

/* Nav Bar Menu */
$string['togglemenu'] = 'Mostrar/Ocultar menú de nmp';

/* Pagination component */
$string['pagination_component_to'] = 'al';
$string['pagination_component_name'] = 'Semana';

/* Goups */
$string['group_allstudent'] = 'Todos los estudiantes';

/* General Errors */
$string['api_error_network'] = "Ha ocurrido un error en la comunicación con el servidor.";
$string['api_invalid_data'] = 'Datos incorrectos';
$string['api_save_successful'] = 'Se han guardado los datos correctamente en el servidor';
$string['api_cancel_action'] = 'Has cancelado la acción';
$string['api_error_conditions_setweek'] = 'No registrar los cambios. Condiciones no cumplidas.';
/* Admin Task Screen*/
$string['generate_data_task'] = 'Proceso para generar datos para Note My Progress Plugin';

/* Chart*/
$string['chart_loading'] = 'Cargando...';
$string['chart_exportButtonTitle'] = "Exportar";
$string['chart_printButtonTitle'] = "Imprimir";
$string['chart_rangeSelectorFrom'] = "De";
$string['chart_rangeSelectorTo'] = "Hasta";
$string['chart_rangeSelectorZoom'] = "Rango";
$string['chart_downloadPNG'] = 'Descargar imagen PNG';
$string['chart_downloadJPEG'] = 'Descargar imagen JPEG';
$string['chart_downloadPDF'] = 'Descargar documento PDF';
$string['chart_downloadSVG'] = 'Descargar imagen SVG';
$string['chart_downloadCSV'] = 'Descargar CSV';
$string['chart_downloadXLS'] = 'Descargar XLS';
$string['chart_exitFullscreen'] = 'Salir de Pantalla Completa';
$string['chart_hideData'] = 'Ocultar Tabla de Datos';
$string['chart_noData'] = 'No hay datos que mostrar';
$string['chart_printChart'] = 'Imprimir Gráfico';
$string['chart_viewData'] = 'Ver Tabla de Datos';
$string['chart_viewFullscreen'] = 'Ver en Pantalla Completa';
$string['chart_resetZoom'] = 'Reiniciar zoom';
$string['chart_resetZoomTitle'] = 'Reiniciar zoom nivel 1:1';

/* Set weeks */
$string['setweeks_title'] = 'Configuración de las Semanas del Curso';
$string['setweeks_description'] = 'Para comenzar, debe configurar el curso por semanas y definir una fecha de inicio para la primera semana (el resto de semanas se realizará de forma automática a partir de esta fecha. A continuación, debe asociar las actividades o módulos relacionadas a cada semana arrastrándolas de la columna de la derecha a la semana correspondiente.  No es necesario asignar todas las actividades o módulos a las semanas, simplemente aquellas que se quieran considerar para hacer el seguimiento de los estudiantes. Finalmente, debe clicar sobre el botón Guardar para conservar su configuración. Recuerde activar en su Moodle la posibilidad de hacer el seguimiento de finalización de tareas.';
$string['setweeks_sections'] = "Secciones disponibles en el curso";
$string['setweeks_weeks_of_course'] = "Planificación de semanas";
$string['setweeks_add_new_week'] = "Agregar semana";
$string['setweeks_start'] = "Comienza:";
$string['setweeks_end'] = "Termina:";
$string['setweeks_week'] = "Semana";
$string['setweeks_save'] = "Guardar configuración";
$string['setweeks_time_dedication'] = "¿Cuántas horas de trabajo espera que los estudiantes dediquen a su curso esta semana?";
$string['setweeks_enable_scroll'] = "Activar el modo desplazamiento para semanas y temas";
$string['setweeks_label_section_removed'] = "Eliminado del curso";
$string['setweeks_error_section_removed'] = "Una sección asignada a una semana se ha eliminado del curso, debe eliminarla de tu planificación para poder continuar.";
$string['setweeks_save_warning_title'] = "¿Está seguro/a que desea guardar los cambios?";
$string['setweeks_save_warning_content'] = "Si modifica la configuración de las semanas cuando el curso ya ha comenzado es posible que se pierdan datos...";
$string['setweeks_confirm_ok'] = "Guardar";
$string['setweeks_confirm_cancel'] = "Cancelar";
$string['setweeks_error_empty_week'] = "No puede guardar los cambios con una semana vacía. Por favor, elimínela y inténtelo de nuevo.";
$string['setweeks_new_group_title'] = "Nueva instancia de configuración";
$string['setweeks_new_group_text'] = "Hemos detectado que su curso ha finalizado, si desea configurar las semanas para trabajar con nuevos estudiantes, debe activar el botón de más abajo. Esto permitirá separar los datos de los estudiantes actuales de los de cursos anteriores, evitando mezclarlos.";
$string['setweeks_new_group_button_label'] = "Guardar configuración como nueva instancia";
$string['course_format_weeks'] = 'Semana';
$string['course_format_topics'] = 'Tema';
$string['course_format_social'] = 'Social';
$string['course_format_singleactivity'] = 'Actividad única';
$string['plugin_requirements_title'] = 'Estado:';
$string['plugin_requirements_descriptions'] = 'El plugin será visible y mostrará los reportes para estudiantes y profesores cuando se cumplan las siguientes condiciones...';
$string['plugin_requirements_has_users'] = 'El curso debe poseer al menos un estudiante matriculado';
$string['plugin_requirements_course_start'] = 'La fecha actual debe ser mayor a la fecha de inicio de la primera semana configurada.';
$string['plugin_requirements_has_sections'] = 'Las semanas configuradas poseen al menos una sección.';
$string['plugin_visible'] = 'Reportes visibles.';
$string['plugin_hidden'] = 'Reportes ocultos.';
$string['title_conditions'] = 'Condiciones de uso';

/* Time */
$string['nmp_mon'] = 'Lunes';
$string['nmp_tue'] = 'Martes';
$string['nmp_wed'] = 'Miércoles';
$string['nmp_thu'] = 'Jueves';
$string['nmp_fri'] = 'Viernes';
$string['nmp_sat'] = 'Sábado';
$string['nmp_sun'] = 'Domingo';
$string['nmp_mon_short'] = 'Lun';
$string['nmp_tue_short'] = 'Mar';
$string['nmp_wed_short'] = 'Mié';
$string['nmp_thu_short'] = 'Jue';
$string['nmp_fri_short'] = 'Vie';
$string['nmp_sat_short'] = 'Sáb';
$string['nmp_sun_short'] = 'Dom';

$string['nmp_jan'] = 'Enero';
$string['nmp_feb'] = 'Febrero';
$string['nmp_mar'] = 'Marzo';
$string['nmp_apr'] = 'Abril';
$string['nmp_may'] = 'Mayo';
$string['nmp_jun'] = 'Junio';
$string['nmp_jul'] = 'Julio';
$string['nmp_aug'] = 'Agosto';
$string['nmp_sep'] = 'Septiembre';
$string['nmp_oct'] = 'Octubre';
$string['nmp_nov'] = 'Noviembre';
$string['nmp_dec'] = 'Diciembre';
$string['nmp_jan_short'] = 'Ene';
$string['nmp_feb_short'] = 'Feb';
$string['nmp_mar_short'] = 'Mar';
$string['nmp_apr_short'] = 'Abr';
$string['nmp_may_short'] = 'May';
$string['nmp_jun_short'] = 'Jun';
$string['nmp_jul_short'] = 'Jul';
$string['nmp_aug_short'] = 'Ago';
$string['nmp_sep_short'] = 'Sep';
$string['nmp_oct_short'] = 'Oct';
$string['nmp_nov_short'] = 'Nov';
$string['nmp_dec_short'] = 'Dic';

$string['nmp_week1'] = 'Sem 1';
$string['nmp_week2'] = 'Sem 2';
$string['nmp_week3'] = 'Sem 3';
$string['nmp_week4'] = 'Sem 4';
$string['nmp_week5'] = 'Sem 5';
$string['nmp_week6'] = 'Sem 6';

$string['nmp_00'] = '12am';
$string['nmp_01'] = '1am';
$string['nmp_02'] = '2am';
$string['nmp_03'] = '3am';
$string['nmp_04'] = '4am';
$string['nmp_05'] = '5am';
$string['nmp_06'] = '6am';
$string['nmp_07'] = '7am';
$string['nmp_08'] = '8am';
$string['nmp_09'] = '9am';
$string['nmp_10'] = '10am';
$string['nmp_11'] = '11am';
$string['nmp_12'] = '12pm';
$string['nmp_13'] = '1pm';
$string['nmp_14'] = '2pm';
$string['nmp_15'] = '3pm';
$string['nmp_16'] = '4pm';
$string['nmp_17'] = '5pm';
$string['nmp_18'] = '6pm';
$string['nmp_19'] = '7pm';
$string['nmp_20'] = '8pm';
$string['nmp_21'] = '9pm';
$string['nmp_22'] = '10pm';
$string['nmp_23'] = '11pm';

/* Teacher General */
$string['tg_section_help_title'] = 'Indicadores Generales';
$string['tg_section_help_description'] = 'Esta sección contiene visualizaciones con indicadores generales relacionados a la configuración del curso, recursos asignados por semanas, sesiones de estudio y progreso de los estudiantes a lo largo del curso. Las visualizaciones de esta sección muestran los indicadores desde la fecha de inicio hasta la de finalización del curso (o hasta la fecha actual en caso de que el curso aún no ha terminado).';
$string['tg_week_resources_help_title'] = 'Recursos por Semanas';
$string['tg_week_resources_help_description_p1'] = 'Este gráfico visualiza la cantidad de recursos de cada una de las secciones del curso asignadas a cada semana de estudio configurada en la sección <i>Configurar Semanas</i>. Si una semana tiene asignada dos o más secciones del curso, los recursos de dichas secciones se suman para el cálculo del total de recursos de una semana.';
$string['tg_week_resources_help_description_p2'] = 'En el eje x del gráfico se encuentran el total de recursos y actividades de las secciones asignadas a cada semana configurada de Note My Progress. En el eje y se encuentran las semanas de estudio configuradas.';
$string['tg_weeks_sessions_help_title'] = 'Sesiones por Semana';
$string['tg_week_sessions_help_description_p1'] = 'Este gráfico muestra la cantidad de sesiones de estudio realizadas por los estudiantes en cada semana a partir de la fecha de inicio del curso. Se considera el acceso al curso por parte del estudiante como el inicio de una sesión de estudio. Una sesión se considera finalizada cuando el tiempo transcurrido entre dos interacciones de un estudiante supera los 30 minutos.';
$string['tg_week_sessions_help_description_p2'] = 'En el eje x del gráfico se encuentran las semanas de cada mes. En el eje y del gráfico se encuentran los diferentes meses del año partiendo del mes de creación del curso. Para mantener la simetría del gráfico se ha colocado un total de cinco semanas para cada mes, sin embargo, no todos los meses tiene tal cantidad de semanas. Dichos meses, solo sumarán sesiones hasta la semana cuatro.';
$string['tg_progress_table_help_title'] = 'Progreso de los estudiantes';
$string['tg_progress_table_help_description'] = 'Este tabla muestra una lista con todos los estudiantes matriculados en el curso junto con su progreso, cantidad de sesiones y tiempo invertido. Para el cálculo del progreso se han considerado todos los recursos del curso a excepción de los de tipo <i>Label</i>. Para determinar si un estudiante ha finalizado un recurso se verifica en primer lugar si el recurso tiene habilitada la configuración de completitud. En caso de ser así, se busca si el estudiante ya ha completado la actividad en base a esa configuración. De lo contrario, la actividad se considera completa si el estudiante la ha visto al menos una vez.';

$string['nmp_title'] = 'Sesiones de Trabajo';
$string['table_title'] = 'Progreso del Curso';
$string['thead_name'] = 'Nombre';
$string['thead_lastname'] = 'Apellidos';
$string['thead_email'] = 'Correo';
$string['thead_progress'] = 'Progreso (%)';
$string['thead_sessions'] = 'Sesiones';
$string['thead_time'] = 'Tiempo Invertido';

$string['nmp_module_label'] = 'recurso';
$string['nmp_modules_label'] = 'recursos';
$string['nmp_of_conector'] = 'de';
$string['nmp_finished_label'] = 'finalizado';
$string['nmp_finisheds_label'] = 'finalizados';

$string['nmp_smaller30'] = 'Menores que 30 minutos';
$string['nmp_greater30'] = 'Mayores que 30 minutos';
$string['nmp_greater60'] = 'Mayores que 60 minutos';

$string['nmp_session_count_title'] = 'Sesiones de la Semana';
$string['nmp_session_count_yaxis_title'] = 'Cantidad de Sesiones';
$string['nmp_session_count_tooltip_suffix'] = ' sesiones';

$string['nmp_hours_sessions_title'] = 'Sesiones por Día y Hora';
$string['nmp_weeks_sessions_title'] = 'Sesiones por Semana';

$string["nmp_session_text"] = "sesión";
$string["nmp_sessions_text"] = "sesiones";

$string['ss_change_timezone'] = 'Zona horaria:';
//$string['ss_activity_inside_plataform_student'] = 'Mi actividad en la plataforma';
//$string['ss_activity_inside_plataform_teacher'] = 'Actividad de los estudiantes en la plataforma';
//$string['ss_time_inside_plataform_student'] = 'Mi tiempo en la plataforma';
//$string['ss_time_inside_plataform_teacher'] = 'Tiempo invertido en promedio de los estudiantes en la plataforma en esta semana';
//$string['ss_time_inside_plataform_description_teacher'] = 'Tiempo que el estudiante ha invertido en la semana seleccionada, en comparación al tiempo que el/la docente planificó que se debería invertir. El tiempo invertido que se visualiza corresponde al promedio de todos los estudiantes. El tiempo planificado por el/la docente es el asignado en por el/la docente en <i>Configurar Semanas</i>.';
//$string['ss_time_inside_plataform_description_student'] = 'Tiempo que ha invertido esta semana en relación al tiempo que el profesor planificó que se debería invertir.';
//$string['ss_activity_inside_plataform_description_teacher'] = 'En el eje Y se indican las las horas del día y en el eje X los días de la semana. Dentro del gráfico podrá encontrar múltiples puntos, los cuales, al pasar el cursor sobre estos, ofrecen información detallada sobre las interacciones de los estudiantes, agrupadas por tipo de recurso (número de interacciones, número de estudiantes que interactuaron con el recurso y promedio de interacciones). <br/><br/><b>Al hacer click en las etiquetas, podrá filtrar por tipo de recurso, dejando visible sólo aquellos que no se encuentren tachados.</b>';
//$string['ss_activity_inside_plataform_description_student'] = 'Presenta las interacciones por tipo de recurso y horario. Al pasar el cursor sobre un punto visible en el gráfico, verá el número de interacciones agrupadas por tipo de recurso. Al hacer click en las etiquetas, podrá filtrar por tipo de recurso.';

/* Teacher Sessions */
$string['ts_section_help_title'] = 'Sesiones de Estudio';
$string['ts_section_help_description'] = 'Esta sección contiene visualizaciones con indicadores relacionados a la actividad de los estudiantes en el curso medida en términos de sesiones realizadas, tiempo promedio invertido en el curso por semana y sesiones de estudio en intervalos de tiempo. Los datos presentados en esta sección varían dependiendo de la semana de estudio seleccionada.';
$string['ts_inverted_time_help_title'] = 'Tiempo Invertido de los Estudiantes';
$string['ts_inverted_time_help_description_p1'] = 'Este gráfico muestra el tiempo promedio invertido por parte de los estudiantes en la semana en comparación del tiempo promedio planificado por parte del docente.';
$string['ts_inverted_time_help_description_p2'] = 'En el eje x del gráfico se encuentra el número de horas que el docente ha planificado para una semana específica. En el eje y se encuentran las etiquetas de tiempo promedio invertido y tiempo promedio que se debería invertir.';
$string['ts_hours_sessions_help_title'] = 'Sesiones por Día y Hora';
$string['ts_hours_sessions_help_description_p1'] = 'Este gráfico muestra las sesiones de estudio por día y hora de la semana seleccionada. Se considera el acceso al curso por parte del estudiante como el inicio de una sesión de estudio. Una sesión se considera finalizada cuando el tiempo transcurrido entre dos interacciones de un estudiante supera los 30 minutos.';
$string['ts_hours_sessions_help_description_p2'] = 'En el eje x del gráfico se encuentran los días de la semana. En el eje y se encuentran las horas del día empezando por las 12am y terminando a las 11pm o 23 horas.';
$string['ts_sessions_count_help_title'] = 'Sesiones de la Semana';
$string['ts_sessions_count_help_description_p1'] = 'Este gráfico muestra el número de sesiones clasificadas por su duración en rangos de tiempo: menores a 30 minutos, mayores a 30 minutos y mayores a 60 minutos. Se considera el acceso al curso por parte del estudiante como el inicio de una sesión de estudio. Una sesión se considera finalizada cuando el tiempo transcurrido entre dos interacciones de un estudiante supera los 30 minutos.';
$string['ts_sessions_count_help_description_p2'] = 'En el eje x del gráfico están los días de la semana configurada. En el eje y está la cantidad de sesiones realizadas.';

$string['nmp_time_inverted_title'] = 'Tiempo invertido de los Estudiantes';
$string['nmp_time_inverted_x_axis'] = 'Número de Horas';
$string['nmp_inverted_time'] = 'Tiempo Promedio Invertido';
$string['nmp_expected_time'] = 'Tiempo Promedio que se debería Invertir';

$string['nmp_year'] = 'año';
$string['nmp_years'] = 'años';
$string['nmp_month'] = 'mes';
$string['nmp_months'] = 'meses';
$string['nmp_day'] = 'día';
$string['nmp_days'] = 'días';
$string['nmp_hour'] = 'hora';
$string['nmp_hours'] = 'horas';
$string['nmp_hours_short'] = 'h';
$string['nmp_minute'] = 'minuto';
$string['nmp_minutes'] = 'minutos';
$string['nmp_minutes_short'] = 'm';
$string['nmp_second'] = 'segundo';
$string['nmp_seconds'] = 'segundos';
$string['nmp_seconds_short'] = 's';
$string['nmp_ago'] = 'atrás';
$string['nmp_now'] = 'justo ahora';

/*Teacher Assignments*/
$string['ta_section_help_title'] = 'Seguimiento de Tareas';
$string['ta_section_help_description'] = 'Esta sección contiene indicadores relacionados a la entrega de tareas y acceso a recursos. Los datos presentados en esta sección varían dependiendo de la semana de estudio seleccionada.';
$string['ta_assigns_submissions_help_title'] = 'Envíos de Tareas';
$string['ta_assigns_submissions_help_description_p1'] = 'Este gráfico presenta la distribución de la cantidad de estudiantes, respecto al estado de entrega de una tarea.';
$string['ta_assigns_submissions_help_description_p2'] = 'En el eje x del gráfico se encuentran las tareas de las secciones asignadas a la semana junto con la fecha y hora de entrega. En el eje y se encuentra la distribución del número de estudiantes según el estado de entrega. El gráfico cuenta con la opción de enviar un correo electrónico a los estudiantes en alguna distribución (envío a tiempo, envíos tardíos, sin envío) al dar clic sobre el gráfico.';
$string['ta_access_content_help_title'] = 'Acceso a los contenidos del curso';
$string['ta_access_content_help_description_p1'] = 'Este gráfico presenta la cantidad de estudiantes que han accedido y no han accedido a los recursos del curso. En la parte superior se tienen los distintos tipos de recursos de Moodle, con la posibilidad de filtrar la información del gráfico según el tipo de recurso seleccionado.';
$string['ta_access_content_help_description_p2'] = 'En el eje x del gráfico se encuentran la cantidad de estudiantes matriculados en el curso. En el eje y del gráfico se encuentran los recursos de las secciones asignadas a la semana. Además, este gráfico permite enviar un correo electrónico a los estudiantes que han accedido al recurso o bien a aquellos que no han accedido al dar clic sobre el gráfico.';

/* Assign Submissions */
$string['nmp_intime_sub'] = 'Envíos a tiempo';
$string['nmp_late_sub'] = 'Envíos tardíos';
$string['nmp_no_sub'] = 'Sin envío';
$string['nmp_assign_nodue'] = 'Sin fecha límite';
$string['nmp_assignsubs_title'] = 'Envíos de Tareas';
$string['nmp_assignsubs_yaxis'] = 'Número de Estudiantes';


/* Content Access */
$string['nmp_assign'] = 'Tarea';
$string['nmp_assignment'] = 'Tarea';
$string['nmp_attendance'] = 'Asistencia';
$string['nmp_book'] = 'Libro';
$string['nmp_chat'] = 'Chat';
$string['nmp_choice'] = 'Elección';
$string['nmp_data'] = 'Base de Datos';
$string['nmp_feedback'] = 'Retroalimentación';
$string['nmp_folder'] = 'Carpeta';
$string['nmp_forum'] = 'Foro';
$string['nmp_glossary'] = 'Glosario';
$string['nmp_h5pactivity'] = 'H5P';
$string['nmp_imscp'] = 'Contenido IMS';
$string['nmp_label'] = 'Etiqueta';
$string['nmp_lesson'] = 'Lección';
$string['nmp_lti'] = 'Contenido IMS';
$string['nmp_page'] = 'Página';
$string['nmp_quiz'] = 'Examen';
$string['nmp_resource'] = 'Recurso';
$string['nmp_scorm'] = 'Paquete SCORM';
$string['nmp_survey'] = 'Encuesta';
$string['nmp_url'] = 'Url';
$string['nmp_wiki'] = 'Wiki';
$string['nmp_workshop'] = 'Taller';

$string['nmp_access'] = 'Accedido';
$string['nmp_no_access'] = 'Sin Acceso';
$string['nmp_access_chart_title'] = 'Acceso a los Contenidos Curso';
$string['nmp_access_chart_yaxis_label'] = 'Cantidad de Estudiantes';
$string['nmp_access_chart_suffix'] = ' estudiantes';


/* Email */
$string['nmp_validation_subject_text'] = 'Asunto es requerido';
$string['nmp_validation_message_text'] = 'Mensaje es requerido';
$string['nmp_subject_label'] = 'Agrega un asunto';
$string['nmp_message_label'] = 'Agrega un mensaje';

$string['nmp_submit_button'] = 'Enviar';
$string['nmp_cancel_button'] = 'Cancelar';
$string['nmp_close_button'] = 'Cerrar';
$string['nmp_emailform_title'] = 'Enviar Correo';
$string['nmp_sending_text'] = 'Enviando Correos';

$string['nmp_recipients_label'] = 'Para';
$string['nmp_mailsended_text'] = 'Correos Enviados';

$string['nmp_email_footer_text'] = 'Este es un correo electrónico enviado con Note My Progress.';
$string['nmp_email_footer_prefix'] = 'Ve a';
$string['nmp_email_footer_suffix'] = 'para más información.';
$string['nmp_mailsended_text'] = 'Correos Enviados';

$string['nmp_assign_url'] = '/mod/assign/view.php?id=';
$string['nmp_assignment_url'] = '/mod/assignment/view.php?id=';
$string['nmp_book_url'] = '/mod/book/view.php?id=';
$string['nmp_chat_url'] = '/mod/chat/view.php?id=';
$string['nmp_choice_url'] = '/mod/choice/view.php?id=';
$string['nmp_data_url'] = '/mod/data/view.php?id=';
$string['nmp_feedback_url'] = '/mod/feedback/view.php?id=';
$string['nmp_folder_url'] = '/mod/folder/view.php?id=';
$string['nmp_forum_url'] = '/mod/forum/view.php?id=';
$string['nmp_glossary_url'] = '/mod/glossary/view.php?id=';
$string['nmp_h5pactivity_url'] = '/mod/h5pactivity/view.php?id=';
$string['nmp_imscp_url'] = '/mod/imscp/view.php?id=';
$string['nmp_label_url'] = '/mod/label/view.php?id=';
$string['nmp_lesson_url'] = '/mod/lesson/view.php?id=';
$string['nmp_lti_url'] = '/mod/lti/view.php?id=';
$string['nmp_page_url'] = '/mod/page/view.php?id=';
$string['nmp_quiz_url'] = '/mod/quiz/view.php?id=';
$string['nmp_resource_url'] = '/mod/resource/view.php?id=';
$string['nmp_scorm_url'] = '/mod/scorm/view.php?id=';
$string['nmp_survey_url'] = '/mod/survey/view.php?id=';
$string['nmp_url_url'] = '/mod/url/view.php?id=';
$string['nmp_wiki_url'] = '/mod/wiki/view.php?id=';
$string['nmp_workshop_url'] = '/mod/workshop/view.php?id=';
$string['nmp_course_url'] = '/course/view.php?id=';


/* Teacher Rating*/
$string['tr_section_help_title'] = 'Seguimiento de Calificaciones';
$string['tr_section_help_description'] = 'Esta sección contiene indicadores relacionados a los promedios de calificaciones en las actividades evaluables. Las diferentes unidades didácticas (Categorías de Calificación) creadas por el docente se muestran en el selector <i>Categoría de Calificación</i>. Este selector permitirá cambiar entre las diferentes unidades definidas y mostrar las actividades evaluables en cada una.';
$string['tr_grade_items_average_help_title'] = 'Promedio de Actividades Evaluables';
$string['tr_grade_items_average_help_description_p1'] = 'Este gráfico presenta el promedio (en porcentaje) de calificaciones de los estudiantes en cada una de las actividades evaluables del curso. El promedio en porcentaje se calcula en base a la calificación máxima de la actividad evaluable (ejemplo: una actividad evaluable con calificación máxima de 80 y calificación promedio de 26 presentará una barra con una altura igual al 33%, ya que 26 es el 33% de la calificación total). Se ha expresado el promedio de calificaciones en base a porcentajes para conservar la simetría del gráfico, puesto que Moodle permite crear actividades y asignar calificaciones personalizadas.';
$string['tr_grade_items_average_help_description_p2'] = 'En el eje x del gráfico se encuentran las distintas actividades evaluables del curso. En el eje y se encuentra el promedio de calificaciones expresado en porcentaje.';
$string['tr_grade_items_average_help_description_p3'] = 'Al hacer clic sobre la barra correspondiente a una actividad evaluable, los datos de los dos gráficos inferiores se actualizarán para mostrar información adicional de la actividad evaluable seleccionada.';
$string['tr_item_grades_details_help_title'] = 'Mejor, Peor y Calificación Promedio';
$string['tr_item_grades_details_help_description_p1'] = 'Este gráfico muestra la mejor calificación, la calificación promedio y la peor calificación en una actividad evaluable (la actividad seleccionada del gráfico Promedio de Actividades Evaluables).';
$string['tr_item_grades_details_help_description_p2'] = 'En el eje x del gráfico se encuentra el puntaje para la calificación de la actividad, siendo la nota máxima de la actividad el máximo valor en este eje. En el eje y se encuentran las etiquetas de Mejor Calificación, Calificación Promedio y Peor Calificación.';
$string['tr_item_grades_distribution_help_title'] = 'Distribución de Calificaciones';
$string['tr_item_grades_distribution_help_description_p1'] = 'Este gráfico muestra la distribución de los estudiantes en diferentes rangos de calificación. Los rangos de calificación se calculan en base a porcentajes. Se toman en cuenta los siguientes rangos: menor al 50%, mayor al 50%, mayor al 60%, mayor al 70%, mayor al 80% y mayor al 90%. Estos rangos se calculan en base a la ponderación máxima que el docente asignó a una actividad evaluable.';
$string['tr_item_grades_distribution_help_description_p2'] = 'En el eje x están los rangos de calificación de la actividad. En el eje y está la cantidad de estudiantes que pertenecen a un determinado rango.';
$string['tr_item_grades_distribution_help_description_p3'] = 'Al hacer clic sobre la barra correspondiente a un rango se puede enviar un correo electrónico a los estudiantes dentro del rango de calificación.';

/* Grades */
$string['nmp_grades_select_label'] = 'Categoría de Calificación';
$string['nmp_grades_chart_title'] = 'Promedios de Actividades Evaluables';
$string['nmp_grades_yaxis_title'] = 'Promedio de Calificaciones (%)';
$string['nmp_grades_tooltip_average'] = 'Calificación Promedio';
$string['nmp_grades_tooltip_grade'] = 'Calificación Máxima';
$string['nmp_grades_tooltip_student'] = 'estudiante calificado de';
$string['nmp_grades_tooltip_students'] = 'estudiantes calificados de';

$string['nmp_grades_best_grade'] = 'Mejor Calificación';
$string['nmp_grades_average_grade'] = 'Calificación Promedio';
$string['nmp_grades_worst_grade'] = 'Peor Calificación';
$string['nmp_grades_details_subtitle'] = 'Mejor, Peor y Calificación Promedio';

$string['nmp_grades_distribution_subtitle'] = 'Distribución de Calificaciones';
$string['nmp_grades_distribution_greater_than'] = 'mayor al';
$string['nmp_grades_distribution_smaller_than'] = 'menor al';
$string['nmp_grades_distribution_yaxis_title'] = 'Número de Estudiantes';
$string['nmp_grades_distribution_tooltip_prefix'] = 'Rango';
$string['nmp_grades_distribution_tooltip_suffix'] = 'en este rango';
$string["nmp_view_details"] = "(Clic para ver detalles)";


/* Teacher Quiz  */
$string['tq_section_help_title'] = 'Seguimiento de Evaluaciones';
$string['tq_section_help_description'] = 'Esta sección contiene indicadores relacionados al resumen de intentos en las diferentes evaluaciones del curso y análisis de las preguntas de una evaluación. Los datos presentados en esta sección varían dependiendo de la semana de estudio seleccionada y de un selector que contiene todas las actividades de tipo Evaluación de las secciones del curso asignadas a la semana seleccionada.';
$string['tq_questions_attempts_help_title'] = 'Intentos de Preguntas';
$string['tq_questions_attempts_help_description_p1'] = 'Este gráfico muestra la distribución de intentos de resolución de cada una de las preguntas de una evaluación junto con el estado de revisión en el que se encuentran.';
$string['tq_questions_attempts_help_description_p2'] = 'En el eje x del gráfico se encuentran las preguntas de la evaluación. En el eje y se encuentra la cantidad de intentos de resolución para cada una de dichas preguntas. La simetría del gráfico se verá afectada por la configuración de la evaluación (ejemplo: en una evaluación que tenga siempre las mismas preguntas, el gráfico presentará la misma cantidad de intentos para cada barra correspondiente a una pregunta. En una evaluación que tenga preguntas aleatorias (de un banco de preguntas), el gráfico presentará en la barra de cada pregunta la suma de los intentos de evaluaciones en los que apareció, pudiendo no ser la misma para cada pregunta de la evaluación).';
$string['tq_questions_attempts_help_description_p3'] = 'Al hacer clic en alguna de las barras correspondiente a una pregunta es posible ver la pregunta de la evaluación en una ventana emergente.';
$string['tq_hardest_questions_help_title'] = 'Preguntas más difíciles';
$string['tq_hardest_questions_help_description_p1'] = 'Este gráfico muestra las preguntas de la evaluación ordenadas por su nivel de dificultad. Se considera incorrecto a un intento de resolución de una pregunta con el estado de Parcialmente Correcto, Incorrecto o En Blanco, de manera que la cantidad total de intentos incorrectos de una pregunta es la suma de los intentos con los estados antes mencionados. El nivel de dificultad se representa en porcentaje calculado en base a la cantidad total de intentos.';
$string['tq_hardest_questions_help_description_p2'] = 'En el eje x del gráfico se encuentran las preguntas de la evaluación identificadas por el nombre. En el eje y se encuentran el porcentaje de intentos incorrectos del total de intentos de la pregunta. Este eje permite identificar cuáles han sido las preguntas que han representado mayor dificultad para los estudiantes que rindieron la evaluación.';
$string['tq_hardest_questions_help_description_p3'] = 'Al hacer clic en alguna de las barras correspondiente a una pregunta es posible ver la pregunta de la evaluación en una ventana emergente.';

$string["nmp_quiz_info_text"] = "Esta Evaluación tiene";
$string["nmp_question_text"] = "pregunta";
$string["nmp_questions_text"] = "preguntas";
$string["nmp_doing_text_singular"] = "intento realizado por";
$string["nmp_doing_text_plural"] = "intentos realizados por";
$string["nmp_attempt_text"] = "intento";
$string["nmp_attempts_text"] = "intentos";
$string["nmp_student_text"] = "estudiante";
$string["nmp_students_text"] = "estudiantes";
$string["nmp_quiz"] = "Evaluaciones";
$string["nmp_questions_attempts_chart_title"] = "Intentos de Preguntas";
$string["nmp_questions_attempts_yaxis_title"] = "Número de Intentos";
$string["nmp_hardest_questions_chart_title"] = "Preguntas mas Difíciles";
$string["nmp_hardest_questions_yaxis_title"] = "Intentos Incorrectos";
$string["nmp_correct_attempt"] = "Correctos";
$string["nmp_partcorrect_attempt"] = "Parcialmente Correctos";
$string["nmp_incorrect_attempt"] = "Incorrectos";
$string["nmp_blank_attempt"] = "En Blanco";
$string["nmp_needgraded_attempt"] = "Sin Calificar";
$string["nmp_review_question"] = "(Clic para revisar la pregunta)";


/* Deserción */
$string['td_section_help_title'] = 'Deserción';
$string['td_section_help_description'] = 'Esta sección contiene indicadores relacionados a la predicción de abandono de estudiantes de un curso. La información se muestra en base a grupos de estudiantes calculados por un algoritmo que analiza el comportamiento de cada estudiante en base al tiempo invertido, la cantidad de sesiones del estudiante, la cantidad de días activo y las interacciones que ha realizado con cada recurso y con los demás estudiantes del curso. El algoritmo coloca en el mismo grupo a estudiantes con similar comportamiento, de manera que se puede identificar a los estudiantes más y menos comprometidos con el curso. Los datos presentados en esta sección varían dependiendo del grupo seleccionado en el selector que contiene los grupos identificados en el curso.';
$string['td_group_students_help_title'] = 'Estudiantes del Grupo';
$string['td_group_students_help_description_p1'] = 'En esta tabla están los estudiantes pertenecientes al grupo seleccionado del selector Grupo de Estudiantes. De cada estudiante se lista su foto, nombres y el porcentaje de progreso del curso. Para el cálculo del progreso se han considerado todos los recursos del curso a excepción de los de tipo Label. Para determinar si un estudiante ha finalizado un recurso se verifica en primer lugar si el recurso tiene habilitada la configuración de completitud. En caso de ser así, se busca si el estudiante ya ha completado la actividad en base a esa configuración. De lo contrario, la actividad se considera completa si el estudiante la ha visto al menos una vez.';
$string['td_group_students_help_description_p2'] = 'Al hacer clic sobre un estudiante en esta tabla, se actualizarán los gráficos inferiores con la información del estudiante seleccionado.';
$string['td_modules_access_help_title'] = 'Recursos del Curso';
$string['td_modules_access_help_description_p1'] = 'Este gráfico muestra la cantidad de recursos a los que el estudiante ha accedido y completado. Los datos presentados en este gráfico varían dependiendo del estudiante seleccionado en la tabla Estudiantes del Grupo. Para determinar la cantidad de recursos y actividades completas se hace uso de la configuración de Moodle denominada Finalización de Actividad. En caso de que el docente no realice la configuración de completitud para las actividades del curso, la cantidad de actividades accedidas y completas siempre será la misma, ya que sin tal configuración, un recurso se considera finalizado cuando el estudiante accede a él.';
$string['td_modules_access_help_description_p2'] = 'En el eje x se encuentran la cantidad de recursos del curso. En el eje y se encuentran las etiquetas de Accedidos, Completos y Total de recursos del curso.';
$string['td_modules_access_help_description_p3'] = 'Al hacer clic sobre alguna barra es posible ver los recursos y actividades disponibles en el curso (en una ventana emergente) junto con la cantidad de interacciones del estudiante con cada recurso y una etiqueta de no accedido, accedido o completado.';
$string['td_week_modules_help_title'] = 'Recursos por Semanas';
$string['td_week_modules_help_description_p1'] = 'Este gráfico muestra la cantidad de recursos que el estudiante ha accedido y completado de cada una de las semanas configuradas en el plugin. Los datos presentados en este gráfico varían dependiendo del estudiante seleccionado en la tabla <i>Estudiantes del Grupo</i>.';
$string['td_week_modules_help_description_p2'] = 'En el eje x del gráfico se encuentran las diferentes semanas de estudio configuradas. En el eje y se encuentra la cantidad de recursos y actividades accedidas y completadas del estudiante.';
$string['td_week_modules_help_description_p3'] = 'Al hacer clic sobre alguna barra es posible ver los recursos y actividades disponibles en el curso (en una ventana emergente) junto con la cantidad de interacciones del estudiante con cada recurso y una etiqueta de no accedido, accedido o completado.';
$string['td_sessions_evolution_help_title'] = 'Sesiones y tiempo invertido';
$string['td_sessions_evolution_help_description_p1'] = 'Este gráfico permite conocer cómo han evolucionado las sesiones de estudio desde que se registró su primera sesión en el curso. Los datos presentados en este gráfico varían dependiendo del estudiante seleccionado en la tabla <i>Estudiantes del Grupo</i>.';
$string['td_sessions_evolution_help_description_p2'] = 'En el eje x del gráfico se muestra una línea temporal con los días que han transcurrido desde que el estudiante realizó la primera sesión de estudio hasta el día de la última sesión registrada. En el eje y muestran 2 valores, en el lado izquierdo el número de sesiones del estudiante y en el lado derecho la cantidad de tiempo invertido en horas. Entre dichos ejes se dibujan la cantidad de sesiones y el tiempo invertido del estudiante como una serie de tiempo.';
$string['td_sessions_evolution_help_description_p3'] = 'Esta visualización permite hacer un acercamiento sobre una región seleccionada. Este acercamiento ayuda a evidenciar de manera clara dicha evolución en diferentes rangos de fechas.';
$string['td_user_grades_help_title'] = 'Calificaciones';
$string['td_user_grades_help_description_p1'] = 'Este gráfico muestra una comparación de las calificaciones del estudiante con los promedios de calificaciones (media en porcentaje) de sus compañeros en las distintas actividades evaluables del curso. Los datos presentados en este gráfico varían dependiendo del estudiante seleccionado en la tabla <i>Estudiantes del Grupo</i>.';
$string['td_user_grades_help_description_p2'] = 'En el eje x del gráfico se muestran las diferentes actividades evaluables. En el eje y se encuentra la calificación del estudiante y la media de calificaciones de sus compañeros. Tanto la calificación del estudiante como la media del curso se muestran en porcentaje para mantener la simetría del gráfico.';
$string['td_user_grades_help_description_p3'] = 'Con un clic en la barra correspondiente a alguna actividad es posible dirigirse a dicha analizada.';

$string["nmp_cluster_label"] = "Grupo";
$string["nmp_cluster_select"] = "Grupo de Estudiantes";
$string["nmp_dropout_table_title"] = "Estudiantes del Grupo";
$string["nmp_dropout_see_profile"] = "Ver Perfil";
$string["nmp_dropout_user_never_access"] = "Nunca Accedido";
$string["nmp_dropout_student_progress_title"] = "Progreso del Estudiante";
$string["nmp_dropout_student_grade_title"] = "Calificación";
$string['nmp_dropout_no_data'] = "Aún no hay datos de desercion para este curso";
$string['nmp_dropout_no_users_cluster'] = "No hay estudiantes de este grupo";
$string['nmp_dropout_generate_data_manually'] = "Generar Manualmente";
$string['nmp_dropout_generating_data'] = "Generando datos...";
$string["nmp_modules_access_chart_title"] = "Recursos del Curso";
$string["nmp_modules_access_chart_series_total"] = "Total";
$string["nmp_modules_access_chart_series_complete"] = "Completos";
$string["nmp_modules_access_chart_series_viewed"] = "Accedidos";
$string["nmp_week_modules_chart_title"] = "Recursos por Semanas";
$string["nmp_modules_amount"] = "Cantidad de Recursos";
$string["nmp_modules_details"] = "(Clic para ver recursos)";
$string["nmp_modules_interaction"] = "interacción";
$string["nmp_modules_interactions"] = "interacciones";
$string["nmp_modules_viewed"] = "Accedido";
$string["nmp_modules_no_viewed"] = "No accedido";
$string["nmp_modules_complete"] = "Completado";
$string["nmp_sessions_evolution_chart_title"] = "Sesiones y Tiempo Invertido";
$string["nmp_sessions_evolution_chart_xaxis1"] = "Número de Sesiones";
$string["nmp_sessions_evolution_chart_xaxis2"] = "Cantidad de Horas";
$string["nmp_sessions_evolution_chart_legend1"] = "Cantidad de Sesiones";
$string["nmp_sessions_evolution_chart_legend2"] = "Tiempo Invertido";
$string["nmp_user_grades_chart_title"] = "Calificaciones";
$string["nmp_user_grades_chart_yaxis"] = "Calificación en Porcentaje";
$string["nmp_user_grades_chart_xaxis"] = "Actividades Evaluables";
$string["nmp_user_grades_chart_legend"] = "Curso (Media)";
$string["nmp_user_grades_chart_tooltip_no_graded"] = "Sin Calificaciones";
$string["nmp_user_grades_chart_view_activity"] = "Clic para ver la actividad";
$string['nmp_send_mail_to_user'] = 'Correo a';
$string['nmp_send_mail_to_group'] = 'Correo al Grupo';


/*Student General*/
$string['sg_section_help_title'] = 'Indicadores Generales';
$string['sg_section_help_description'] = 'Esta sección contiene indicadores relacionados a tu información, progreso, indicadores generales, recursos del curso, sesiones a lo largo del curso y calificaciones obtenidas. Las visualizaciones de esta sección muestran los indicadores durante todo el curso (hasta la fecha actual).';
$string['sg_modules_access_help_title'] = 'Recursos del Curso';
$string['sg_modules_access_help_description_p1'] = 'Este gráfico muestra la cantidad de recursos que has accedido y completado. Para determinar la cantidad de recursos que has completado se hace uso de la configuración de Moodle denominada Finalización de Actividad. En caso de que el docente no haya configurado la completitud para las actividades del curso, la cantidad de actividades accedidas y completas siempre será la misma, ya que sin tal configuración, un recurso se considera finalizado cuando accedes a él.';
$string['sg_modules_access_help_description_p2'] = 'En el eje x se encuentran la cantidad de recursos del curso. En el eje y se encuentran las etiquetas de Accedidos, Completos y Total de recursos en referencia a tus interacciones con los recursos del curso.';
$string['sg_modules_access_help_description_p3'] = 'Al hacer clic sobre alguna barra es posible ver los recursos y actividades disponibles en el curso (en una ventana emergente) junto con la cantidad de interacciones que has realizado con cada recurso y una etiqueta de no accedido, accedido o completado.';
$string['sg_weeks_session_help_title'] = 'Sesiones por Semana';
$string['sg_weeks_session_help_description_p1'] = 'Este gráfico muestra la cantidad de sesiones de estudio que has realizado cada semana a partir de la fecha de inicio del curso. Se considera el acceso al curso como el inicio de una sesión de estudio. Una sesión se considera finalizada cuando el tiempo transcurrido entre dos interacciones supera los 30 minutos.';
$string['sg_weeks_session_help_description_p2'] = 'En el eje x del gráfico se encuentran las semanas de cada mes. En el eje y del gráfico se encuentran los diferentes meses del año partiendo del mes de creación del curso. Para mantener la simetría del gráfico se ha colocado un total de cinco semanas para cada mes, sin embargo, no todos los meses tiene tal cantidad de semanas. Dichos meses, solo sumarán sesiones hasta la semana cuatro.';
$string['sg_sessions_evolution_help_title'] = 'Sesiones y Tiempo Invertido';
$string['sg_sessions_evolution_help_description_p1'] = 'Este gráfico permite conocer cómo han evolucionado tus sesiones de estudio desde que se registró tu primera sesión en el curso.';
$string['sg_sessions_evolution_help_description_p2'] = 'En el eje x del gráfico se muestra una línea temporal con los días que han transcurrido desde que realizaste tu primera sesión de estudio hasta el día de tu última sesión registrada. En el eje y muestran 2 valores, en el lado izquierdo tu cantidad de sesiones y en el lado derecho tu cantidad de tiempo invertido en horas. Entre dichos ejes se dibujan tu cantidad de sesiones y tu tiempo invertido del estudiante como una serie de tiempo.';
$string['sg_sessions_evolution_help_description_p3'] = 'Esta visualización permite hacer un acercamiento sobre una región seleccionada.';
$string['sg_user_grades_help_title'] = 'Calificaciones';
$string['sg_user_grades_help_description_p1'] = 'Este gráfico muestra una comparación de tus calificaciones con los promedios de calificaciones (media en porcentaje) de tus compañeros en las distintas actividades evaluables del curso.';
$string['sg_user_grades_help_description_p2'] = 'En el eje x del gráfico se muestran las diferentes actividades evaluables. En el eje y se encuentra tus calificaciones y la media de calificaciones de tus compañeros. Tanto tu calificación como la media del curso se muestran en porcentaje para mantener la simetría del gráfico.';
$string['sg_user_grades_help_description_p3'] = 'Con un clic en la barra correspondiente a alguna actividad es posible dirigirse a dicha analizada.';

/* User Sessions*/
$string['ss_section_help_title'] = 'Sesiones de Estudio';
$string['ss_section_help_description'] = 'Esta sección contiene visualizaciones con indicadores relacionados a tu actividad en el curso medida en términos de sesiones de estudio, tiempo invertido y progreso en cada una de las semanas configuradas por el docente. Las visualizaciones de esta sección varían dependiendo de la semana de estudio seleccionada.';
$string['ss_inverted_time_help_title'] = 'Tu tiempo invertido';
$string['ss_inverted_time_help_description_p1'] = 'Este gráfico muestra tu tiempo invertido en la semana en comparación del tiempo planificado por parte del docente.';
$string['ss_inverted_time_help_description_p2'] = 'En el eje x del gráfico se encuentra el número de horas que el docente ha planificado para una semana específica. En el eje y se encuentran las etiquetas de tiempo invertido y tiempo que se debería invertir.';
$string['ss_hours_session_help_title'] = 'Sesiones por Día y Hora';
$string['ss_hours_session_help_description_p1'] = 'Este gráfico muestra tus sesiones de estudio por día y hora de la semana seleccionada. Se considera el acceso al curso como el inicio de una sesión de estudio. Una sesión se considera finalizada cuando el tiempo transcurrido entre dos interacciones supera los 30 minutos.';
$string['ss_hours_session_help_description_p2'] = 'En el eje x del gráfico se encuentran los días de la semana. En el eje y se encuentran las horas del día empezando por las 12am y terminando a las 11pm o 23 horas.';
$string['ss_resources_access_help_title'] = 'Interacción por Tipos de Recursos';
$string['ss_resources_access_help_description_p1'] = 'Este gráfico muestra cuántos recursos tienes pendientes y cuáles ya has completado en la semana seleccionada. Los recursos se agrupan por su tipo en este gráfico. Además, en la parte superior se muestra una barra que representa el porcentaje de recursos accedidos del total de recursos asignados a la semana seleccionada.';
$string['ss_resources_access_help_description_p2'] = 'En el eje x del gráfico se encuentran los diferentes tipos de recursos. En el eje y se encuentran la cantidad de recursos accedidos de la semana.';
$string['ss_resources_access_help_description_p3'] = 'Al hacer clic sobre alguna barra es posible ver los recursos y actividades disponibles en el curso (en una ventana emergente) junto con la cantidad de interacciones que has realizado con cada recurso y una etiqueta de no accedido, accedido o completado.';


$string['nmp_student_time_inverted_title'] = 'Tu Tiempo Invertido';
$string['nmp_student_time_inverted_x_axis'] = 'Número de Horas';
$string['nmp_student_inverted_time'] = 'Tiempo Invertido';
$string['nmp_student_expected_time'] = 'Tiempo que se debería Invertir';

$string['nmp_resource_access_title'] = 'Interacción por Tipos de Recursos';
$string['nmp_resource_access_y_axis'] = 'Cantidad de Recursos';
$string['nmp_resource_access_x_axis'] = 'Tipos de Recursos';
$string['nmp_resource_access_legend1'] = 'Completos';
$string['nmp_resource_access_legend2'] = 'Pendientes';

$string['nmp_week_progress_title'] = 'Progreso de la Semana';



/*Teacher Indicators*/
$string['nmp_teacher_indicators_title'] = 'Indicadores Generales';
$string['nmp_teacher_indicators_students'] = 'Estudiantes';
$string['nmp_teacher_indicators_weeks'] = 'Semanas';
$string['nmp_teacher_indicators_grademax'] = 'Calificación';
$string['nmp_teacher_indicators_course_start'] = 'Inicio';
$string['nmp_teacher_indicators_course_end'] = 'Fin';
$string['nmp_teacher_indicators_course_format'] = 'Formato';
$string['nmp_teacher_indicators_course_completion'] = 'Completitud de Módulos';
$string["nmp_teacher_indicators_student_progress"] = "Progreso del los Estudiantes";
$string["nmp_teacher_indicators_week_resources_chart_title"] = "Recursos por Semanas";
$string["nmp_teacher_indicators_week_resources_yaxis_title"] = "Cantidad de Recursos";

/* Logs */
$string['nmp_logs_title'] = 'Descargar los registros de actividad';
$string['nmp_logs_help_description'] = 'Esta sección le permite descargar los registros de actividad que se han realizado. Es decir, tienes acceso a las acciones que han realizado los usuarios registrados en la plataforma en un formato de hoja de cálculo.';
$string['nmp_logs_title_MoodleSetpoint_title'] = 'Seleccione un rango de fechas para las acciones realizadas en Moodle';
$string['nmp_logs_title_MMPSetpoint_title'] = 'Seleccione un rango de fechas para las acciones realizadas en Note My Progress';
$string['nmp_logs_help'] = 'Esta sección le permite descargar un archivo de registro de las actividades realizadas.';
$string['nmp_logs_select_date'] = 'Seleccione un intervalo de tiempo para el registro';
$string['nmp_logs_first_date'] = 'Fecha de inicio';
$string['nmp_logs_last_date'] = 'Fecha de finalización';
$string['nmp_logs_valid_Moodlebtn'] = 'Descargar el registro de actividades de Moodle';
$string['nmp_logs_valid_NMPbtn'] = 'Descargar el registro de actividades de NMP';
$string['nmp_logs_invalid_date'] = 'Introduzca una fecha';
$string['nmp_logs_download_btn'] = 'Descarga en curso';
$string['nmp_logs_download_nmp_help_title'] = 'Sobre las acciones realizadas en Note My Progress';
$string['nmp_logs_download_moodle_help_title'] = 'Sobre las acciones realizadas en Moodle';
$string['nmp_logs_download_nmp_help_description'] = 'El archivo de registro que se descarga enumera todas las acciones que ha realizado el usuario únicamente dentro del plugin Note My Progress (ver el progreso, ver los indicadores generales...)';
$string['nmp_logs_download_moodle_help_description'] = 'El archivo de registro que se sube enumera todas las acciones que ha realizado el usuario sólo dentro de Moodle (ver el curso, ver los recursos, enviar una tarea...)';



/* Logs CSV Header */
$string['nmp_logs_csv_headers_username'] = 'Nombre de usuario';
$string['nmp_logs_csv_headers_firstname'] = 'Nombre';
$string['nmp_logs_csv_headers_lastname'] = 'Apellido';
$string['nmp_logs_csv_headers_date'] = 'Fecha';
$string['nmp_logs_csv_headers_hour'] = 'Hora';
$string['nmp_logs_csv_headers_action'] = 'Acción';
$string['nmp_logs_csv_headers_coursename'] = 'Nombre del curso';
$string['nmp_logs_csv_headers_detail'] = 'Detalle';
$string['nmp_logs_csv_headers_detailtype'] = 'Tipo de objeto utilizado';

$string['nmp_logs_error_begin_date_superior'] = 'La fecha de inicio no puede ser mayor que la fecha actual';
$string['nmp_logs_error_begin_date_inferior'] = 'La fecha de inicio debe ser anterior a la fecha de finalización';
$string['nmp_logs_error_empty_dates'] = 'Las fechas no pueden estar vacías';
$string['nmp_logs_error_problem_encountered'] = 'Se ha encontrado un problema, por favor, inténtelo de nuevo';

$string['nmp_logs_success_file_downloaded'] = '¡Archivo cargado!';


$string['nmp_logs_moodle_csv_headers_role'] = 'Role';
$string['nmp_logs_moodle_csv_headers_email'] = 'Email';
$string['nmp_logs_moodle_csv_headers_username'] = 'Username';
$string['nmp_logs_moodle_csv_headers_fullname'] = 'Fullname';
$string['nmp_logs_moodle_csv_headers_date'] = 'Date';
$string['nmp_logs_moodle_csv_headers_hour'] = 'Hour';
$string['nmp_logs_moodle_csv_headers_action'] = 'Action';
$string['nmp_logs_moodle_csv_headers_courseid'] = 'CourseID';
$string['nmp_logs_moodle_csv_headers_coursename'] = 'Course_name';
$string['nmp_logs_moodle_csv_headers_detailid'] = 'Detail ID';
$string['nmp_logs_moodle_csv_headers_details'] = 'Details';
$string['nmp_logs_moodle_csv_headers_detailstype'] = 'Details_type';

$string['nmp_logs_moodle_csv_headers_role_description'] = 'Da el rol que tiene el usuario en el curso en el que ha realizado una acción (alumno, profesor...)';
$string['nmp_logs_moodle_csv_headers_email_description'] = 'Proporciona el correo electrónico del usuario';
$string['nmp_logs_moodle_csv_headers_username_description'] = 'Da el nombre de usuario de moodle de la persona que realizó la acción';
$string['nmp_logs_moodle_csv_headers_fullname_description'] = 'Da el nombre completo del usuario (Nombre + Apellido)';
$string['nmp_logs_moodle_csv_headers_date_description'] = 'Indica la fecha en que se realizó la acción en el formato dd-MM-AAAA';
$string['nmp_logs_moodle_csv_headers_hour_description'] = 'Indica la hora en que se realizó la acción';
$string['nmp_logs_moodle_csv_headers_action_description'] = 'Dar un verbo que describa la acción realizada (por ejemplo, hacer clic, ver...)';
$string['nmp_logs_moodle_csv_headers_courseid_description'] = 'Indica el identificador del precio sobre el que se ha realizado la acción';
$string['nmp_logs_moodle_csv_headers_coursename_description'] = 'Indique el nombre de la acción en la que se realizó la acción';
$string['nmp_logs_moodle_csv_headers_detailid_description'] = 'Da el ID del objeto con el que el usuario ha interactuado';
$string['nmp_logs_moodle_csv_headers_details_description'] = 'Indicar el nombre del objeto al que se apunta';
$string['nmp_logs_moodle_csv_headers_detailstype_description'] = 'Indica el tipo de objeto al que se apunta (ejemplos de objetos: Repositorio, Concurso, Recursos...)';

$string['nmp_logs_moodle_table_title'] = 'Descripción de las rúbricas';
$string['nmp_logs_moodle_table_subtitle'] = 'Acerca de los registros de Moodle';

$string['nmp_logs_nmp_table_title'] = 'Descripción de las rúbricas';
$string['nmp_logs_nmp_table_subtitle'] = 'Acerca de los registros de Note My Progress';

$string['nmp_logs_nmp_csv_headers_role'] = 'Role';
$string['nmp_logs_nmp_csv_headers_email'] = 'Email';
$string['nmp_logs_nmp_csv_headers_username'] = 'Username';
$string['nmp_logs_nmp_csv_headers_fullname'] = 'Fullname';
$string['nmp_logs_nmp_csv_headers_date'] = 'Date';
$string['nmp_logs_nmp_csv_headers_hour'] = 'Hour';
$string['nmp_logs_nmp_csv_headers_courseid'] = 'CourseID';
$string['nmp_logs_nmp_csv_headers_section_name'] = 'NMP_SECTION_NAME';
$string['nmp_logs_nmp_csv_headers_action_type'] = 'NMP_ACTION_TYPE';

$string['nmp_logs_nmp_csv_headers_role_description'] = 'Da el rol que tiene el usuario en el curso en el que ha realizado una acción (alumno, profesor...)';
$string['nmp_logs_nmp_csv_headers_email_description'] = 'Proporciona el correo electrónico del usuari';
$string['nmp_logs_nmp_csv_headers_username_description'] = 'Da el nombre de usuario de moodle de la persona que realizó la acción';
$string['nmp_logs_nmp_csv_headers_fullname_description'] = 'Da el nombre completo del usuario (Nombre + Apellido)';
$string['nmp_logs_nmp_csv_headers_date_description'] = 'Indica la fecha en que se realizó la acción en el formato dd-MM-AAAA';
$string['nmp_logs_nmp_csv_headers_hour_description'] = 'Indica la hora en que se realizó la acción';
$string['nmp_logs_nmp_csv_headers_courseid_description'] = 'Indica el identificador del precio sobre el que se ha realizado la acción';
$string['nmp_logs_nmp_csv_headers_section_name_description'] = 'Da el nombre de la sección de note my progress en la que se encontraba el usuario cuando realizó la acción';
$string['nmp_logs_nmp_csv_headers_action_type_description'] = 'Proporciona una descripción completa de la acción realizada por el usuario en forma de verbo + sujeto + objeto (por ejemplo, download_moodle_logfile)';

$string['nmp_logs_table_title'] = 'Título';
$string['nmp_logs_table_title_bis'] = 'Descripción';

$string['nmp_logs_help_button_nmp'] = 'Sobre las acciones realizadas en Note My Progress';
$string['nmp_logs_help_button_moodle'] = 'Sobre las acciones realizadas en Moodle';


$string['nmp_logs_download_details_link'] = 'Leer más';
$string['nmp_logs_download_details_title'] = '¿Está seguro de que quiere un informe explicativo detallado?';
$string['nmp_logs_download_details_description'] = 'Si acepta, se descargará un archivo en formato PDF.';
$string['nmp_logs_download_details_ok'] = 'Descargar';
$string['nmp_logs_download_details_cancel'] = 'Cancelar';
$string['nmp_logs_download_details_validation'] = 'El informe se ha descargado';


/* NoteMyProgress admin settings */

$string['nmp_settings_bddusername_label'] = 'Nombre de usuario de la base de datos';
$string['nmp_settings_bddusername_description'] = 'Este parámetro designa el nombre de usuario desde el que se puede acceder a la base de datos MongoDB. Si se introduce este parámetro, se debe introducir la contraseña así como el nombre de la base de datos de destino.';
$string['nmp_settings_bddusername_default'] = 'Vacío';

$string['nmp_settings_bddpassword_label'] = 'Contraseña de la cuenta';
$string['nmp_settings_bddpassword_description'] = 'Este parámetro es la contraseña de la cuenta desde la que se puede acceder a la base de datos MongoDB. Si se introduce este parámetro, será necesario introducir el nombre de usuario así como el nombre de la base de datos de destino.';
$string['nmp_settings_bddpassword_default'] = 'Vacío';


$string['nmp_settings_bddaddress_label'] = 'Dirección del servidor MongoDB *';
$string['nmp_settings_bddaddress_description'] = 'Este parámetro es la dirección desde la que se puede acceder a la base de datos MongoDB. Este parámetro es obligatorio y tiene la forma: 151.125.45.58      or       yourserver.com';
$string['nmp_settings_bddaddress_default'] = 'localhost';

$string['nmp_settings_bddport_label'] = 'Puerto de comunicación *';
$string['nmp_settings_bddport_description'] = 'Este parámetro designa el puerto a utilizar para comunicarse con la base de datos. Este parámetro es obligatorio y debe ser un número.';
$string['nmp_settings_bddport_default'] = '27017';


$string['nmp_settings_bddname_label'] = 'Nombre de la base de datos';
$string['nmp_settings_bddname_description'] = 'Este parámetro designa el nombre de la base de datos MongoDB en la que se guardará la información.';
$string['nmp_settings_bddname_default'] = 'Vacío';

//Planning
/* Global */
$string['pagination'] = 'Semana:';
$string['graph_generating'] = 'Estamos construyendo el reporte, por favor espere un momento.';
$string['txt_hour'] = 'Hora';
$string['txt_hours'] = 'Horas';
$string['txt_minut'] = 'Minuto';
$string['txt_minuts'] = 'Minutos';
$string['pagination_week'] = 'Semana';
$string['only_student'] = 'Este reporte es solo para estudiantes';
$string['sr_hour'] = 'hora';
$string['sr_hours'] = 'horas';
$string['sr_minute'] = 'minuto';
$string['sr_minutes'] = 'minutos';
$string['sr_second'] = 'segundo';
$string['sr_seconds'] = 'segundos';
$string['weeks_not_config'] = 'El curso no ha sido configurado por el profesor, por lo que no puede utilizar el módulo de reportes.';
$string['pagination_title'] = 'Selección semana';
$string['helplabel'] = 'Ayuda';
$string['exitbutton'] = '¡Entendido!';
$string['hours_unit_time_label'] = 'Número de Horas';

/* Menú */
$string['sr_menu_main_title'] = "Reportes";
$string['sr_menu_setweek'] = "Configurar semanas";
$string['sr_menu_logs'] = "Descargar registros";
$string['sr_menu_time_worked_session_report'] = 'Sesiones de estudio por semana';
$string['sr_menu_time_visualization'] = 'Tiempo invertido (Horas por semana)';
$string['sr_menu_activities_performed'] = 'Actividades realizadas';
$string['sr_menu_notes'] = 'Anotaciones';

/* General Errors */
$string['pluginname'] = 'Note My Progress';
$string['previous_days_to_create_report'] = 'Días considerados para la construcción del reporte';
$string['previous_days_to_create_report_description'] = 'Días anteriores a la fecha actual que se tendrán en cuenta para generar el reporte.';
$string['student_reports:usepluggin'] = 'Utilizar el pluggin';
$string['student_reports:downloadreport'] = 'Descargar actividad de los estudiantes';
$string['student_reports:setweeks'] = 'Configurar semanas del curso';
$string['student_reports:activities_performed'] = 'Ver el reporte "Actividades realizadas"';
$string['student_reports:metareflexion'] = 'Ver el reporte "Metareflexión"';
$string['student_reports:notes'] = 'Utilizar las notas';

/* Set weeks */
$string['setweeks_title'] = "Mettre en place des semaines";
$string['setweeks_description'] = "Pour commencer, vous devez organiser le cours par semaines et définir une date de début pour la première semaine (le reste des semaines se fera automatiquement à partir de cette date). Ensuite, vous devez associer les activités ou modules liés à chaque semaine en les faisant glisser de la colonne de droite à la semaine correspondante. Il n'est pas nécessaire d'attribuer toutes les activités ou modules aux semaines, mais seulement ceux que vous voulez prendre en compte pour le suivi des étudiants. Enfin, vous devez cliquer sur le bouton 'Enregistrer' pour conserver vos paramètres.";
$string['setweeks_sections'] = "Sections disponibles dans le cours";
$string['setweeks_weeks_of_course'] = "Planification des semaines";
$string['setweeks_add_new_week'] = "Ajouter une semaine";
$string['setweeks_start'] = "Date de début :";
$string['setweeks_end'] = "Date de finalisation :";
$string['setweeks_week'] = "Semaine";
$string['setweeks_save'] = "Sauvegarder";
$string['setweeks_time_dedication'] = "Combien d'heures de travail pensez-vous que les étudiants consacreront à leur cours cette semaine ?";
$string['setweeks_enable_scroll'] = "Activer le mode de défilement pour les semaines et les thèmes";
$string['setweeks_label_section_removed'] = "Effacer le cours";
$string['setweeks_error_section_removed'] = " Une section assignée à une semaine a été supprimée du cours, vous devez la supprimer de votre emploi du temps pour pouvoir continuer";
$string['setweeks_save_warning_title'] = "Êtes-vous sûr de vouloir sauvegarder les changements";
$string['setweeks_save_warning_content'] = "Si vous modifiez la configuration des semaines alors que le cours a déjà commencé, il est possible que des données soient perdues...";
$string['setweeks_confirm_ok'] = "Sauvegarder";
$string['setweeks_confirm_cancel'] = "Annuler";
$string['setweeks_error_empty_week'] = "Impossible de sauvegarder les changements avec une semaine vide. Veuillez l'effacer et réessayer.";
$string['setweeks_new_group_title'] = "Nouvelle instance de configuration";
$string['setweeks_new_group_text'] = "Nous avons détecté que votre cours est terminé, si vous souhaitez configurer les semaines pour travailler avec de nouveaux étudiants, vous devez activer le bouton ci-dessous. Cela vous permettra de séparer les données des étudiants actuels de celles des cours précédents, en évitant de les mélanger";
$string['setweeks_new_group_button_label'] = "Enregistrer configuration comme nouvelle instance";
$string['course_format_weeks'] = "Semaine";
$string['course_format_topics'] = "Sujet";
$string['course_format_social'] = "Social";
$string['course_format_singleactivity'] = "Activité unique";
$string['plugin_requirements_title'] = "Situation:";
$string['plugin_requirements_descriptions'] = "Le plugin sera visible et affichera les rapports pour les étudiants et les enseignants lorsque les conditions suivantes sont remplies...";
$string['plugin_requirements_has_users'] = "Le cours doit avoir au moins un étudiant inscrit";
$string['plugin_requirements_course_start'] = "La date actuelle doit être supérieure à la date de début de la première semaine configurée";
$string['plugin_requirements_a_sections'] = "Les semaines configurées ont au moins une section.";
$string['plugin_visible'] = "Rapports visibles";
$string['plugin_hidden'] = "Rapports cachés";
$string['title_conditions'] = "Conditions d'utilisation";

/* Logs */
$string['logs_title'] = 'Descargar fichero de registro de estudiantes';
$string['logs_description'] = 'Para descargar el fichero de registros (las acciones realizadas por los estudiantes sobre Moodle), debe hacer click en el botón Generar Fichero de Registros. Posteriormente, se habilitará un enlace que permitirá la descarga del fichero.';
$string['logs_btn_download'] = 'Generar fichero de registros';
$string['logs_label_creating'] = 'El fichero está siendo generando, esto puede tomar varios minutos. Por favor espere...';
$string['logs_generation_finished'] = 'El fichero de registros se ha generado con éxito.';
$string['logs_download_link'] = 'Descargar fichero de registros';
$string['logs_th_title_column'] = "Columna";
$string['logs_th_title_description'] = "Descripción";
$string['logs_tb_title_logid'] = "Id registro";
$string['logs_tb_title_userid'] = "Id usuario";
$string['logs_tb_title_username'] = "Nombre de usuario";
$string['logs_tb_title_names'] = "Nombres";
$string['logs_tb_title_lastnames'] = "Apellidos";
$string['logs_tb_title_email'] = "Correo electrónico";
$string['logs_tb_title_roles'] = "Roles";
$string['logs_tb_title_courseid'] = "Id curso";
$string['logs_tb_title_component'] = "Componente";
$string['logs_tb_title_action'] = "Acción";
$string['logs_tb_title_timecreated'] = "Fecha (Timestamp)";
$string['logs_tb_description_logid'] = "Numero de log registrado";
$string['logs_tb_description_userid'] = "Id de la persona que genero el log";
$string['logs_tb_description_username'] = "Usuario registrado en plataforma de la persona que genero el log";
$string['logs_tb_description_names'] = "Nombres de participantes en el curso";
$string['logs_tb_description_lastnames'] = "Apellidos de los participantes en el curso";
$string['logs_tb_description_email'] = "Correo electrónico de los participantes en el curso";
$string['logs_tb_description_roles'] = "Rol en plataforma de persona que genero el log";
$string['logs_tb_description_courseid'] = "Curso desde el cual se genero el log";
$string['logs_tb_description_component'] = "Componente del curso interactuado";
$string['logs_tb_description_action'] = "Acción que gatilló la generación del log";
$string['logs_tb_description_timecreated'] = "Fecha en formato timestamp en la cual se genero el log";

/*new string*/
$string['studentreports'] = "Rapports des étudiants";

/*Menu*/
$string['binnacle'] = 'Bitácora';
$string['report1'] = 'Informe 1';
$string['report2'] = 'Informe 2';
$string['report3'] = 'Informe 3';
$string['report4'] = 'Informe 4';
$string['report5'] = 'Informe 5';
$string['set_weeks_title'] = 'Configuración de las semanas del curso';
$string['set_weeks'] = 'Configuración de las semanas del curso';

/*Notes*/
$string['notes_btn_delete'] = 'Eliminar';
$string['notes_btn_new'] = 'Descartar cambios';
$string['notes_btn_notes'] = 'Guardar';
$string['notes_btn_update'] = 'Editar';
$string['notes_notes'] = 'Anotaciones';
$string['notes_string_new'] = 'Nuevo';
$string['notes_string_update'] = 'Actualizado';
$string['notes_title_notes'] = 'Anotaciones';
$string['notes_placeholder_title'] = 'Título';
$string['notes_placeholder_search'] = 'Buscar...';
$string['notes_placeholder_note'] = 'Escribir mi nota...';
$string['notes_save_success'] = 'Se ha guardado la nota';
$string['notes_created'] = 'Creado el...';
$string['notes_not_found'] = 'No se encontraron notas';
$string['notes_description'] = 'Escriba y guarde las notas que consideres relevantes para este curso. Podrá ver todas las notas que ha escrito para este curso, como recordatorios, destacados u otros. Utilize el buscador para encontrar una nota particular entre las generadas a partir de una palabra clave o una frase.';
$string['notes_message_created'] = 'Se ha creado la nota';
$string['notes_message_not_created'] = 'No se ha podido crear la nota';
$string['notes_message_deleted'] = 'Se ha eliminado la nota';
$string['notes_message_not_deleted'] = 'No se ha podido eliminar la nota.';
$string['notes_message_updated'] = 'Se ha actualizado la nota';
$string['notes_message_not_updated'] = 'No se ha podido actualizar la nota.';




/*Metareflexion*/
$string['metareflexion_goal1'] = "Aprender cosas nuevas";
$string['metareflexion_goal2'] = "Repasando lo que he visto";
$string['metareflexion_goal3'] = "Planificar mi aprendizaje";

$string['metareflexion_no_modules_in_section'] = "Esta semana no hay módulos";
$string['compare_with_course'] = "Comparar mis resultados con los de mis compañeros";
$string['sr_menu_metareflexion'] = "Metareflexión";
$string['metareflexion_last_week_created'] = 'Reflexiones de la semana pasada guardadas';
$string['metareflexion_last_week_update'] = 'Reflexiones de la semana pasada actualizadas';
$string['metareflexion_update_planification_success'] = 'Compromiso actualizado';
$string['metareflexion_save_planification_success'] = "Compromiso registrado";
$string['metareflexion_tab_reflexion'] = "Estadísticas de eficiencia";
$string['metareflexion_tab_reflexion_title'] = "Lista de recursos de esta semana";
$string['metareflexion_tab_planification'] = "Planificación de la semana";
$string['metareflexion_tab_lastweek'] = "Reflexiones de la semana pasada";
$string['metareflexion_tab_graphic_lastweek'] = "Reflexiones";
$string['metareflexion_effectiveness_title'] = "Metareflexión - Estadísticas de eficiencia";
$string['effectiveness_graphic_legend_pending'] = "Recursos planificados";
$string['metareflexion_graphic_legend_unplanned'] = "No hay recursos planificados";
$string['effectiveness_graphic_legend_completed'] = "Todos los recursos completados";
$string['effectiveness_graphic_legend_failed'] = "Algunos recursos no se han completado";
$string['metareflexion_cw_card_title'] = "Metareflexión - Planificación de la semana";
$string['metareflexion_cw_description_student'] = "En esta sección puedes planificar el número de horas que quieres invertir, tus objetivos personales y los días que quieres trabajar en el curso para esta semana. Esta información se utilizará para evaluar su eficacia, que podrá consultar en la sección 'Estadísticas de eficacia'";
$string['metareflexion_cw_card_daysdedicate'] = "Días que pasaré en el curso:";
$string['metareflexion_cw_card_hoursdedicate'] = "Horas que pasaré en el curso:";
$string['metareflexion_cw_card_msg'] = "Todavía no has planificado el tiempo que quieres dedicar a este recurso";
$string['metareflexion_cw_card_btn_plan'] = "Planificación de la semana";
$string['metareflexion_cw_table_title'] = "Planificación de recursos por día";
$string['metareflexion_cw_table_thead_activity'] = "Actividad";
$string['metareflexion_cw_table_thead_type'] = "Tipo de recurso";
$string['metareflexion_cw_table_thead_days_committed'] = "Días dedicados";
$string['metareflexion_cw_table_thead_hours_committed'] = "Horas dedicadas";
$string['metareflexion_cw_table_thead_plan'] = "Plan";
$string['metareflexion_cw_dialog_daysdedicate'] = "Elige qué días piensas completar los diferentes recursos";
$string['metareflexion_cw_dialog_hoursdedicate'] = "¿Cuántas horas de trabajo tiene previsto hacer esta semana?";
$string['metareflexion_cw_dialog_goalsdedicate'] = "¿Qué objetivos tienes para esta semana?";
$string['metareflexion_cw_dialog_btn_cancel'] = "Cancelar";
$string['metareflexion_cw_dialog_btn_accept'] = "Guardar el plan";
$string['metareflexion_pw_title'] = "Reflexiones de la semana pasada";
$string['metareflexion_pw_description_student'] = "En esta sección podrá indicar cuántas horas presenciales ha realizado en el curso y cuántas fuera del horario de clase. Además, podrá indicar si se han cumplido los objetivos de aprendizaje de la semana pasada y dar su opinión sobre los beneficios de haber asistido al curso. Esta información se facilitará al profesor de forma anónima con el fin de mejorar el curso";
$string['metareflexion_pw_classroom_hours'] = "¿Cuántas horas presenciales ha dedicado a este curso esta semana?";
$string['metareflexion_pw_hours_off_course'] = "¿Cuántas horas de trabajo ha dedicado al curso fuera de la plataforma Moodle?";
$string['metareflexion_pw_btn_save'] = "Guardar";
$string['metareflexion_report_title'] = "Gráfico vinculado al formulario de compromiso de planificación/reflexión";
$string['metareflexion_report_description_days_teacher'] = "En este gráfico, encontrará los días previstos que los estudiantes se han comprometido a trabajar (primera barra) en comparación con los días realmente trabajados (segunda barra). Si la primera barra no es visible, significa que sus alumnos no han planificado ningún día de trabajo para esa semana; han planificado algunos días de trabajo. Si la segunda barra no es visible, significa que los alumnos no han realizado el trabajo que se comprometieron a hacer el día previsto.";
$string['metareflexion_report_description_days_student'] = " Debajo de los objetivos encontrarás una tabla que muestra si has cumplido con la programación del módulo según los días que has establecido. Si se cumple el plazo, se mostrará un pulgar verde. Si el trabajo previsto no se completa, se mostrará un pulgar rojo. <br> Hay que tener en cuenta que un módulo se considera validado cuando se consulta. Así, para validar un día, debe consultar todos los módulos que haya planificado en la sección 'Planificación de la semana'.<br>";
$string['metareflexion_report_subtitle_days_student'] = "Mis compromisos previstos para esta semana";
$string['metareflexion_report_subtitle_days_teacher'] = "Porcentaje de estudiantes que se ciñen a su plan ese día";
$string['metareflexion_report_description_goals_student'] = "Debajo de este gráfico, encontrarás un recordatorio de los objetivos que fijaste la semana pasada.";
$string['metareflexion_report_subtitle_hours_student'] = "Mi eficiencia durante la semana";
$string['metareflexion_report_subtitle_hours_teacher'] = "Eficiencia media de la semana";
$string['metareflexion_report_descrition_hours_teacher'] = "Este gráfico muestra las horas que los estudiantes tenían previsto trabajar durante esa semana en comparación con las horas que realmente trabajaron. <br> Tenga en cuenta que se considera un día de trabajo si el estudiante ha interactuado con los recursos en esa semana. El tiempo previsto se obtiene de la metarreflexión del estudiante en su planificación de la semana.<br>";
$string['metareflexion_report_descrition_hours_student'] = "Este gráfico muestra las horas que el estudiante tiene previsto trabajar durante esa semana en comparación con las horas realmente trabajadas y la media de horas trabajadas por los estudiantes del curso. El tiempo planificado se obtiene de la metarreflexión semanal que hiciste en la sección 'Planificación de la semana'.";
$string['metareflexion_report_description_meta_student'] = "Finalmente, en la última posición encontrarás un cuestionario sobre la semana pasada. Tenga en cuenta que este formulario no está disponible para una semana sin planificación o para la semana actual";
$string['metareflexion_cw_day_lun'] = "Lunes";
$string['metareflexion_cw_day_mar'] = "Martes";
$string['metareflexion_cw_day_mie'] = "Miércoles";
$string['metareflexion_cw_day_jue'] = "Jueves";
$string['metareflexion_cw_day_vie'] = "viernes";
$string['metareflexion_cw_day_sab'] = "Sábado";
$string['metareflexion_cw_day_dom'] = "Domingo";
$cadena['metareflexión_mismo'] = "Mi";
$string['metareflexion_inverted_time'] = "Tiempo invertido";
$string['metareflexion_inverted_time_course'] = "Tiempo medio invertido (curso)";
$string['metareflexion_planified_time'] = "Tiempo planificado";
$string['metareflexion_planified_time_course'] = "Tiempo medio planificado (curso)";
$string['metareflexion_belonging'] = "Pertenecer a";
$string['metareflexion_student'] = "Estudiantes";
$string['metareflexion_interaction_user'] = "Usuarios que han interactuado con este recurso";
$string['metareflexion_hover_title_teacher'] = "Eficacia de los alumnos (media)";
$string['metareflexion_hover_title_student'] = "Eficiencia por semana";
$string['metareflexion_planning_week'] = "Él/ella está planificando";
$string['metareflexion_planning_week_start'] = ", comienza el ";
$string['metareflexion_planning_week_end'] = "y termina el";
$string['metareflexion_report_pw_avegare_hour_clases'] = "Número medio de horas dedicadas a las clases presenciales";
$string['metareflexion_report_pw_avegare_hour_outside_clases'] = "Promedio de horas pasadas fuera de clase";
$string['metareflexion_report_pw_summary_card'] = "Tiempo medio de clase";
$string['metareflexion_report_pw_learning_objetives'] = "Relacionado con los objetivos de aprendizaje";
$string['metareflexion_report_pw_attending_classes'] = "Sobre el beneficio de tomar cursos";
$string['metareflexion_report_pw_text_no_data'] = "No hay datos para esta semana";
$string['metareflexion_report_pw_description_teacher'] = "En estos gráficos se puede ver la información obtenida de los alumnos sobre sus impresiones de los cursos de la semana anterior";
$string['metareflexion_weekly_planning_subtitle'] = "Vea los resultados de su planificación semanal en la pestaña 'Eficiencia por semana'";
$string['metareflexion_saved_form'] = "Ya ha completado el formulario en Metareflexion esta semana";
$string['metareflexion_efficiency_title'] = "Su eficiencia";
$string['metareflexion_goals_title'] = "Elegir objetivos personales";
$string['metareflexion_pres_question_time_invested_outside'] = "¿Cuánto tiempo he invertido en trabajar fuera de la plataforma Moodle?";
$string['metareflexion_pres_question_objectives_reached'] = "He alcanzado mis objetivos?";
$string['metareflexion_pres_question_pres_question_feel'] = "¿Qué pienso de esta semana?";
$string['metareflexion_pres_question_learn_and_improvement'] = "¿Qué he aprendido y cómo puedo mejorar?";
$string['metareflexion_goals_reflexion_title'] = "Los objetivos elegidos para esta semana";
$string['metareflexion_title_hours_plan'] = "Plan de horas para la semana actual";
$string['metareflexion_title_retrospective'] = "Retrospectiva de la semana";
$string['metareflexion_title_metareflexion'] = "Planning";

/* Reports */
$string['student_reports:view_report_as_teacher'] = 'Ver el reporte como profesor';
$string['student_reports:view_report_as_student'] = 'Ver el reporte como estudiante';
$string['student_reports:time_visualizations'] = 'Reporte "Trabajo en plataforma"';
$string['student_reports:time_worked_session_report'] = 'Reporte "Tiempo de trabajo en plataforma"';
$string['student_reports:write'] = 'Guardar cambios en los reportes';

/* time_worked_session_report */
$string['twsr_title'] = 'Sesiones de estudio por semana';
$string['twsr_description_student'] = 'Una sesión corresponde al tiempo dedicado a interactuar dentro de la plataforma y el tiempo promedio de las respectivas sesiones. A continuación, se presenta el listado de semanas con el total de sesiones. Al pasar el cursor sobre el gráfico, podrá encontrar la información en detalle del conjunto de estudiantes del curso.';
$string['twsr_description_teacher'] = 'Una sesión corresponde al tiempo que los estudiantes han estado interactuando con los materiales del curso Moodle. En este reporte encontrará el listado de semanas con el total de sesiones realizadas por los estudiantes del curso. Al poner el cursor sobre una de las barras podrá ver el total de sesiones, la duración promedio de éstas y a cuántos estudiantes diferentes participaron en cada sesión.';
$string['twsr_axis_x'] = 'Número de sesiones';
$string['twsr_axis_y'] = 'Semanas del curso';
$string['twsr_student'] = 'Estudiante';
$string['twsr_students'] = 'estudiantes';
$string['twsr_sessions_by'] = 'Sesiones pertenecientes a';
$string['twsr_sessions_middle_time'] = 'Duración promedio de las sesiones';
$string['twsr_total_sessions'] = 'Total de sesiones';

/* time_visualizations */
$string['tv_title'] = 'Tiempo invertido (horario)';
$string['tv_lunes'] = 'Lunes';
$string['tv_martes'] = 'Martes';
$string['tv_miercoles'] = 'Miércoles';
$string['tv_jueves'] = 'Jueves';
$string['tv_viernes'] = 'Viernes';
$string['tv_sabado'] = 'Sábado';
$string['tv_domingo'] = 'Domingo';
$string['tv_axis_x'] = 'Días de la semana';
$string['tv_axis_y'] = 'Horas del día';
$string['tv_url'] = 'URL';
$string['tv_resource_document'] = 'Documento';
$string['tv_resource_image'] = 'Imagen';
$string['tv_resource_audio'] = 'Audio';
$string['tv_resource_video'] = 'Video';
$string['tv_resource_file'] = 'Archivo';
$string['tv_resource_script'] = 'Script/Código';
$string['tv_resource_text'] = 'Texto';
$string['tv_resource_download'] = 'Descargas';
$string['tv_assign'] = 'Tarea';
$string['tv_assignment'] = 'Tarea';
$string['tv_book'] = 'Libro';
$string['tv_choice'] = 'Elección';
$string['tv_feedback'] = 'Retroalimentación';
$string['tv_folder'] = 'Carpeta';
$string['tv_forum'] = 'Foro';
$string['tv_glossary'] = 'Glosario';
$string['tv_label'] = 'Etiqueta';
$string['tv_lesson'] = 'Lección';
$string['tv_page'] = 'Página';
$string['tv_quiz'] = 'Examen';
$string['tv_survey'] = 'Encuesta';
$string['tv_lti'] = 'Herramienta externa';
$string['tv_other'] = 'Otro';
$string['tv_interaction'] = 'Interacción por';
$string['tv_interactions'] = 'Interacciones por';
$string['tv_course_module'] = 'Módulo';
$string['tv_course_modules'] = 'Módulos';
$string['tv_student'] = 'Estudiante.';
$string['tv_students'] = 'Estudiantes.';
$string['tv_average'] = 'Promedio interacciones';
$string['tv_change_timezone'] = 'Zona horaria:';
$string['tv_activity_inside_plataform_student'] = 'Mi actividad en la plataforma';
$string['tv_activity_inside_plataform_teacher'] = 'Actividad de los estudiantes en la plataforma';
$string['tv_time_inside_plataform_student'] = 'Mi tiempo en la plataforma';
$string['tv_time_inside_plataform_teacher'] = 'Tiempo invertido en promedio de los estudiantes en la plataforma en esta semana';
$string['tv_time_inside_plataform_description_teacher'] = 'Tiempo que el estudiante ha invertido en la semana seleccionada, en comparación al tiempo que el/la docente planificó que se debería invertir. El tiempo invertido que se visualiza corresponde al promedio de todos los estudiantes. El tiempo planificado por el/la docente es el asignado en por el/la docente en <i>Configurar Semanas</i>.';
$string['tv_time_inside_plataform_description_student'] = 'Tiempo que ha invertido esta semana en relación al tiempo que el profesor planificó que se debería invertir.';
$string['tv_activity_inside_plataform_description_teacher'] = 'En el eje Y se indican las las horas del día y en el eje X los días de la semana. Dentro del gráfico podrá encontrar múltiples puntos, los cuales, al pasar el cursor sobre estos, ofrecen información detallada sobre las interacciones de los estudiantes, agrupadas por tipo de recurso (número de interacciones, número de estudiantes que interactuaron con el recurso y promedio de interacciones). <br/><br/><b>Al hacer click en las etiquetas, podrá filtrar por tipo de recurso, dejando visible sólo aquellos que no se encuentren tachados.</b>';
$string['tv_activity_inside_plataform_description_student'] = 'Presenta las interacciones por tipo de recurso y horario. Al pasar el cursor sobre un punto visible en el gráfico, verá el número de interacciones agrupadas por tipo de recurso. Al hacer click en las etiquetas, podrá filtrar por tipo de recurso.';
$string['tv_to'] = 'al';
$string['tv_time_spend'] = 'Tiempo invertido';
$string['tv_time_spend_teacher'] = 'Tiempo promedio invertido';
$string['tv_time_should_spend'] = 'Tiempo que deberías invertir';
$string['tv_time_should_spend_teacher'] = 'Tiempo promedio que se debería invertir';

/* activities_performed */
$string['ap_title'] = 'Actividades realizadas';
$string['ap_description_student'] = 'Este reporte presenta las actividades realizadas y pendientes esta semana. La barra de progreso de la semana corresponde al porcentaje de actividades a realizar esta semana y planificadas por el profesor. La opción Semana permite escoger y ver el reporte correspondiente a la semana escogida. Las actividades se encuentran agrupadas por tipo de recurso. Las marcadas en color verde son las realizadas y en rojo las pendientes. Para que una actividad se marque como realizada, debes haber interactuado al menos una vez con el recurso. <br/> <br/> Al hacer click sobre las etiquetas finalizados o pendientes, podrás filtrar la información.';
$string['ap_description_teacher'] = 'Pendiente de texto';
$string['ap_axis_x'] = 'Tipo de recursos';
$string['ap_axis_y'] = 'Cantidad de recursos';
$string['ap_progress'] = 'Progreso de la semana';
$string['ap_to'] = 'al';
$string['ap_week'] = 'Semana';
$string['ap_activities'] = 'Interacción agrupada por tipos de recursos';

/* activities_performed teacher */
$string['apt_title'] = 'Actividades realizadas del curso';
$string['apt_description'] = 'Este reporte presenta las actividades disponibles del curso. La opción Semana permite escoger y ver el reporte correspondiente a dicha semana. Al pasar el cursor por alguna de las barras, encontrará el promedio de las visualizaciones e interacciones. Si hace click en la barra, encontrarás mayor detalle (nombre de estudiante, correo, interacciones, tiempo promedio de visualización).';
$string['apt_axis_x'] = 'Interacciones';
$string['apt_axis_y'] = 'Recursos';
$string['apt_visualization_average'] = 'Promedio de visualizaciones';
$string['apt_interacctions_average'] = 'Promedio de interacciones';
$string['apt_thead_name'] = 'Nombre';
$string['apt_thead_lastname'] = 'Apellidos';
$string['apt_thead_email'] = 'Correo';
$string['apt_thead_interactions'] = 'Interacciones';
$string['apt_thead_visualization'] = 'Tiempo promedio de visualización';

/* Pagination component */
$string['pagination_component_to'] = 'al';
$string['pagination_component_name'] = 'Semana';

/* Questions et réponses */
/* Preguntas y respuestas */
$string['question_number_one'] = "¿Cumpliste los objetivos de aprendizaje la semana pasada?";
$string['answers_number_one_option_one'] = "Sí";
$string['answers_number_one_option_two'] = "No";


$string['question_number_two'] = "¿Cuál es su resultado de aprendizaje?";
$string['answers_number_two_option_one'] = "Tengo una idea clara del tema(s) y sé cómo aplicarlo";
$string['answers_number_two_option_two'] = "Tengo una idea clara del tema(s) y no de cómo aplicarlo";
$string['answers_number_two_option_three'] = "No tengo una idea clara sobre el tema(s)";

$string['question_number_three'] = "¿Qué utilidad tuvo para usted la participación en las clases presenciales?";
$string['answers_number_three_option_one'] = "Nada útil";
$string['answers_number_three_option_two'] = "Bastante útil";
$string['answers_number_three_option_three'] = "Útil";
$string['answers_number_three_option_four'] = "Muy útil";
$string['answers_number_three_option_five'] = "Realmente útil";
$cadena['answers_number_three_option_six'] = "No fui a clase";

$string['question_number_four'] = "¿Cómo te sientes con respecto a la semana pasada?";
$string['answers_number_four_option_one'] = "No estoy satisfecho con la forma en que me he organizado para lograr mis objetivos";
$string['answers_number_four_option_two'] = "Estoy bastante satisfecho con la forma en que me he organizado para lograr mis objetivos";
$string['answers_number_four_option_three'] = "Estoy satisfecho con la forma en que me he organizado para lograr mis objetivos";
$string['answers_number_four_option_four'] = "Estoy muy satisfecho con la forma en que me he organizado para lograr mis objetivos";

/* Auto-evaluation */
$string['qst1'] = "Establezco objetivos de logro para mi trabajo personal";
$string['qst2'] = "Establezco objetivos a corto plazo (diarios o semanales) y a largo plazo (mensuales o semestrales)";
$string['qst3'] = "Soy exigente con mi propio aprendizaje y me pongo normas";
$string['qst4'] = "Establezco objetivos para ayudarme a gestionar mi trabajo personal";
$string['qst5'] = "No reduzco la calidad de mi trabajo cuando está en línea";
$string['qst6'] = "Elijo dónde estudiar para evitar demasiadas distracciones";
$string['qst7'] = "Elijo un lugar cómodo para estudiar";
$string['qst8'] = "Ya sé dónde puedo estudiar mejor";
$string['qst9'] = "Elijo un momento con pocas distracciones para trabajar";
$string['qst10'] = "Intento hacer cursos de toma de apuntes en profundidad en línea, porque los apuntes son aún más importantes para aprender en línea que en un aula normal";
$string['qst11'] = "Leo mis documentos en voz alta para combatir las distracciones";
$string['qst12'] = "Preparo mis preguntas antes de entrar en la sala de debate y en la discusión";
$string['qst13'] = "Trabajo en problemas adicionales más allá de los asignados para dominar el contenido del curso";
$string['qst14'] = "Hago tiempo para trabajar fuera de clase";
$string['qst15'] = "Intento programar la misma hora cada día o semana para estudiar, y me atengo a ese horario";
$string['qst16'] = "Aunque no tenemos clases todos los días, intento repartir mi tiempo de trabajo uniformemente a lo largo de la semana";
$string['qst17'] = "Busco un estudiante que domine el curso para poder consultarlo cuando necesite ayuda";
$string['qst18'] = "Comparto mis problemas con mis compañeros para que podamos resolverlos juntos";
$string['qst19'] = "Si es necesario, intento reunirme con mis compañeros cara a cara";
$string['qst20'] = "No dudo en pedir ayuda al profesor por correo electrónico";
$string['qst21'] = "Hago resúmenes de mis lecciones para ser consciente de lo que he entendido";
$string['qst22'] = "Me hago muchas preguntas sobre el contenido del curso que estoy estudiando";
$string['qst23'] = "Me comunico con mis compañeros para saber cómo me relaciono con ellos";
$string['qst24'] = "Me comunico con mis compañeros para saber que lo que estoy aprendiendo es diferente de lo que ellos están aprendiendo";

$string['btn_save_auto_eval'] = "Guardar";
$string['message_save_successful'] = "Autoevaluación guardada con éxito";
$string['message_update_successful'] = "Autoevaluación actualizada con éxito";
$string['page_title_auto_eval'] = "Autoevaluación";
$string['form_questions_auto_eval'] = "Preguntas de autoevaluación";
$string['description_auto_eval'] = "Esta página le permite autoevaluar sus métodos de estudio. Este cuestionario es específico para cada estudiante pero común a todos los cursos en Moodle. Una vez que lo hayas completado, puedes guardar tus respuestas con el botón de la parte inferior de la página y cambiar tus opciones en cualquier momento.";
/* Goups */
$string['group_allstudent'] = 'Todos los estudiantes';

/* Admin Settings */
$string['nmp_use_navbar_menu'] = 'Habilitar nuevo menú';
$string['nmp_use_navbar_menu_desc'] = 'Despliega el menú del plugin en la barra de navegación superior, del contrario incluirá el menú en el bloque de navegación.';
/* Gamification */
$string['tg_tab1'] = "Cuadro";
$string['tg_tab2'] = "Clasificación";
$string['tg_tab3'] = "Mi perfil";
$string['EnableGame'] = "Desactivar/Activar :";
$string['studentRanking1'] = "¿Quieres aparecer en el ranking ? ";
$string['studentRanking2'] = "/!\ Te verán todos los inscritos en este curso que hayan aceptado. Aceptar es definitivo: ";
$string['studentRanking3'] = "Acceptar";
$string['gamification_denied'] = 'La gamificación ha sido desactivada por su maestro';
$string['tg_colon'] = '{$a->a}: {$a->b}';
$string['tg_section_title'] = 'Configuración de gamificación del curso';
$string['tg_section_help_title'] = 'Configuración de gamificación del curso';
$string['tg_section_help_description'] = 'Puede dar la opción de gamificación a los estudiantes del curso.';
$string['tg_save_warning_title'] = "¿Estás seguro de que quieres guardar los cambios?";
$string['tg_save_warning_content'] = "Si cambia la configuración de la gamificación cuando el curso ya ha comenzado, los datos pueden perderse ...";
$string['tg_confirm_ok'] = "Guardar";
$string['tg_confirm_cancel'] = "Cancelar";
$string['tg_save'] = "Guardar configuración";
$string['tg_section_preview'] = 'Avance';
$string['tg_section_experience'] = 'Puntos de experiencia';
$string['tg_section_information'] = 'Información';
$string['tg_section_ranking'] = 'Clasificación/informe';
$string['tg_section_settings'] = 'Ajustes';

$string['tg_section_points'] = 'puntos';
$string['tg_section_description'] = 'Descripción';
$string['tg_section_no_description'] = 'Sin descripción';
$string['tg_section_no_name'] = 'Sin nombre';

$string['tg_section_preview_next_level'] = 'al siguiente nivel';
$string['tg_section_ranking_ranking_text'] = 'Clasificación';
$string['tg_section_ranking_level'] = 'Nivel';
$string['tg_section_ranking_student'] = 'Alumna';
$string['tg_section_ranking_total'] = 'Total';
$string['tg_section_ranking_progress'] = 'Progreso %';

$string['tg_section_settings_appearance'] = 'Apariencia';
$string['tg_section_settings_appearance_title'] = 'Título';
$string['tg_section_settings_levels'] = 'Configuración de nivel';
$string['tg_section_settings_levels_quantity'] = 'Nivel';
$string['tg_section_settings_levels_base_points'] = 'Puntos base';
$string['tg_section_settings_levels_calculated'] = 'automático';
$string['tg_section_settings_levels_manually'] = 'A mano';
$string['tg_section_settings_levels_name'] = 'Nombre';
$string['tg_section_settings_levels_required'] = 'Se requieren puntos';
$string['tg_section_settings_rules'] = 'Establecimiento de reglas';
$string['tg_section_settings_add_rule'] = 'Agregar nueva regla';
$string['tg_section_settings_earn'] = 'Para este evento, gane:';
$string['tg_section_settings_select_event'] = 'Seleccione un evento';
$string['gm_Chart_Title'] = 'Tabla de esparcimiento';
$string['gm_Chart_Y'] = 'Alumno';

$string['tg_timenow'] = 'Justo ahora';
$string['tg_timeseconds'] = '{$a}s atrás';
$string['tg_timeminutes'] = '{$a}m atrás';
$string['tg_timehours'] = '{$a}h atrás';
$string['tg_timedays'] = '{$a}d atrás';
$string['tg_timeweeks'] = '{$a}w atrás';
$string['tg_timewithinayearformat'] = '%b %e';
$string['tg_timeolderyearformat'] = '%b %Y';

/* General Errors */
$string['nmp_api_error_network'] = "Se ha producido un error en la comunicación con el servidor.";
$string['nmp_api_invalid_data'] = 'Datos Incorrectos';
$string['nmp_api_json_decode_error'] = 'Error en decodificación de datos enviados';
$string['nmp_api_invalid_token_error'] = 'El token enviado en la transacción no es válido, actualice la página';
$string['nmp_api_invalid_transaction'] = 'La solicitud es incorrecta';
$string['nmp_api_invalid_profile'] = 'No puedes hacer esta acción, tu perfil es incorrecto';
$string['nmp_api_save_successful'] = 'Los datos se han guardado correctamente en el servidor';
$string['nmp_api_cancel_action'] = 'Has cancelado la acción';
$string['overview'] = 'Visión de conjunto : ';
$string['game_point_error'] = 'Los puntos son requeridos';
$string['game_event_error'] = 'El evento es requerido';
$string['game_name_error'] = 'Se requiere el nombre';
