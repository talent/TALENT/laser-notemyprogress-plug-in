<?php
// Ce fichier fait partie de Moodle - http://moodle.org/
//
// Moodle est un logiciel libre: vous pouvez le redistribuer et / ou le modifier
// selon les termes de la licence publique générale GNU comme publié par
// la Free Software Foundation, soit la version 3 de la licence, soit
// (à votre choix) toute version ultérieure.
//
// Moodle est distribué dans l\'espoir qu'il sera utile,
// mais SANS AUCUNE GARANTIE; sans même la garantie implicite de
// QUALITÉ MARCHANDE ou d\'aDÉQUATION À UN USAGE PARTICULIER. Voir la
// Licence publique générale GNU pour plus de détails.
//
// Vous devriez avoir reçu une copie de la licence publique générale GNU
// avec Moodle. Sinon, consultez <http://www.gnu.org/licenses/>.

/**
 * Plugin chains are defined here.
 *
 * @package local_notemyprogress
 * @category string
 * @author 2021 Éric Bart <bart.eric@hotmail.com>
 * @copyright 2020 Edisson Sigma <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license http://www.gnu.org/copyleft /gpl.html GNU GPL v3 ou version ultérieure
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'notemyprogress';

/* Global */
$string['pagination'] = 'Week:';
$string['graph_generating'] = 'We are building the report, please wait a moment.';
$string['weeks_not_config'] = 'The course has not been configured by the teacher, so there are no visualizations to display.';
$string['pagination_title'] = 'Selection of the week';
$string['helplabel'] = 'Help';
$string['exitbutton'] = 'OK!';
$string['no_data'] = 'There is no data to display';
$string['only_student'] = 'This report is for students only';
$string["nmp_send_mail"] = "(Click to send an e-mail)";
$string["nmp_about"] = "About this chart";
$string["nmp_about_table"] = "About this table";
$string["nmp_not_configured"] = "Not configured";
$string["nmp_activated"] = "Activated";
$string["nmp_disabled"] = "Disabled";

/* Menu */
$string['menu_main_title'] = "Progression of the scoreboard";
$string['menu_sessions'] = 'Study session';
$string['menu_setweek'] = "Define the weeks";
$string['menu_time'] = 'Time tracking';
$string['menu_assignments'] = 'Monitoring of deposits';
$string['menu_grades'] = 'Tracking grades';
$string['menu_quiz'] = 'Follow-up of evaluations';
$string['menu_dropout'] = 'Dropout';
$string['menu_logs'] = "Activity reports";
$string['menu_planning'] = "Planning";
$string['menu_general'] = "Global indicators";
$string['menu_auto_evaluation'] = "Auto-evaluation";

/* Nav Bar Menu */
$string['togglemenu'] = 'Show / Hide the NoteMyProgress menu';

/* Composant de pagination */
$string['pagination_component_to'] = 'al';
$string['pagination_component_name'] = 'Week';

/* Groups */
$string['group_allstudent'] = 'All students';

/* Erreurs générales */
$string['api_error_network'] = "An error occurred during communication with the server.";
$string['api_invalid_data'] = 'Invalid data';
$string['api_save_successful'] = 'The data has been correctly recorded on the server';
$string['api_cancel_action'] = 'You have cancelled the action';
$string['api_error_conditions_setweek'] = 'Failed to register changes. Conditions not met.';

/* Admin Task Screen */
$string['generate_data_task'] = 'Data generation process for the NoteMyProgress plugin';

/* Graphique */
$string['chart_loading'] = 'Loading...';
$string['chart_exportButtonTitle'] = "Export";
$string['chart_printButtonTitle'] = "Print";
$string['chart_rangeSelectorFrom'] = "From";
$string['chart_rangeSelectorTo'] = "To";
$string['chart_rangeSelectorZoom'] = "Range";
$string['chart_downloadPNG'] = 'Download a PNG image';
$string['chart_downloadJPEG'] = 'Download a JPEG image';
$string['chart_downloadPDF'] = 'Download a PDF document';
$string['chart_downloadSVG'] = 'Download a SVG image';
$string['chart_downloadCSV'] = 'Download CSV';
$string['chart_downloadXLS'] = 'Download XLS';
$string['chart_exitFullscreen'] = 'Exit full screen';
$string['chart_hideData'] = 'Hide the data table';
$string['chart_noData'] = 'There is no data to display ';
$string['chart_printChart'] = 'Print the chart';
$string['chart_viewData'] = 'Display the data table';
$string['chart_viewFullscreen'] = 'View full screen';
$string['chart_resetZoom'] = 'Restart the zoom';
$string['chart_resetZoomTitle'] = 'Reset zoom level 1: 1';

/* Définir les semaines */
$string['setweeks_title'] = 'Course weeks configuration';
$string['setweeks_description'] = 'To start, you must set up the course by weeks and define a start date for the first week (the rest of the weeks will be done automatically from this date onwards). Then, you must associate the related activities or modules to each week by dragging them from the right column to the corresponding week.  It is not necessary to assign all the activities or modules to the weeks, just those that you want to consider for tracking the students. Finally, click on the Save button to keep your settings. ';
$string['setweeks_sections'] = "Sections available in the course";
$string['setweeks_weeks_of_course'] = "Planning the weeks";
$string['setweeks_add_new_week'] = "Add a week";
$string['setweeks_start'] = "Start the:";
$string['setweeks_end'] = "End the:";
$string['setweeks_week'] = "Week";
$string['setweeks_save'] = "Save configuration";
$string['setweeks_time_dedication'] = 'How many hours of work do you expect students to put into your course this week ?';
$string['setweeks_enable_scroll'] = "Activate the scroll mode for weeks and themes";
$string['setweeks_label_section_removed'] = "Removed from the course";
$string['setweeks_error_section_removed'] = "A section assigned to a week has been removed from the course, you must remove it from your plan to continue.";
$string['setweeks_save_warning_title'] = "Are you sure you want to save the changes ?";
$string['setweeks_save_warning_content'] = "If you change the configuration of weeks where the course has already started, data may be lost...";
$string['setweeks_confirm_ok'] = "Save";
$string['setweeks_confirm_cancel'] = "Cancel";
$string['setweeks_error_empty_week'] = "You cannot save changes with an week without content. Please delete it and try again.";
$string['setweeks_new_group_title'] = "New instance of configuration";
$string['setweeks_new_group_text'] = "We have detected that your course is finished, if you wish to set up the weeks to work with new students you need to activate the button below. This will separate the data of current students from previous courses, avoiding mixing them up. ";
$string['setweeks_new_group_button_label'] = "Save the configuration as a new instance";
$string['course_format_weeks'] = 'Week';
$string['course_format_topics'] = 'Topic';
$string['course_format_social'] = 'Social';
$string['course_format_singleactivity'] = 'Single activity';
$string['plugin_requirements_title'] = 'Status:';
$string['plugin_requirements_descriptions'] = 'The plugin will be visible and display reports for students and teachers when the following conditions are met...';
$string['plugin_requirements_has_users'] = 'The course must have at least one student enrolled';
$string['plugin_requirements_course_start'] = 'The current date must be later than the start date of the first configured week.';
$string['plugin_requirements_has_sections'] = 'The configured weeks must contain at least one section.';
$string['plugin_visible'] = 'Visible reports.';
$string['plugin_hidden'] = 'Hidden reports.';
$string['title_conditions'] = 'Terms of use ';

/* Heure */
$string['nmp_mon'] = 'Monday';
$string['nmp_tue'] = 'Tuesday';
$string['nmp_wed'] = 'Wednesday';
$string['nmp_thu'] = 'Thursday';
$string['nmp_fri'] = 'Friday';
$string['nmp_sat'] = 'Saturday';
$string['nmp_sun'] = 'Sunday';
$string['nmp_mon_short'] = 'Mon';
$string['nmp_tue_short'] = 'Tue';
$string['nmp_wed_short'] = 'Wed';
$string['nmp_thu_short'] = 'Thu';
$string['nmp_fri_short'] = 'Fri';
$string['nmp_sat_short'] = 'Sat';
$string['nmp_sun_short'] = 'Sun';

$string['nmp_jan'] = 'January';
$string['nmp_feb'] = 'February';
$string['nmp_mar'] = 'March';
$string['nmp_apr'] = 'April';
$string['nmp_may'] = 'May';
$string['nmp_jun'] = 'June';
$string['nmp_jul'] = 'July';
$string['nmp_aug'] = 'August';
$string['nmp_sep'] = 'September';
$string['nmp_oct'] = 'October';
$string['nmp_nov'] = 'November';
$string['nmp_dec'] = 'December';
$string['nmp_jan_short'] = 'Jan';
$string['nmp_feb_short'] = 'Feb';
$string['nmp_mar_short'] = 'Mar';
$string['nmp_apr_short'] = 'Apr';
$string['nmp_may_short'] = 'May';
$string['nmp_jun_short'] = 'Jun';
$string['nmp_jul_short'] = 'Jul';
$string['nmp_aug_short'] = 'Aug';
$string['nmp_sep_short'] = 'Sep';
$string['nmp_oct_short'] = 'Oct';
$string['nmp_nov_short'] = 'Nov';
$string['nmp_dec_short'] = 'Dec';

$string['nmp_week1'] = 'Week 1';
$string['nmp_week2'] = 'Week 2';
$string['nmp_week3'] = 'Week 3';
$string['nmp_week4'] = 'Week 4';
$string['nmp_week5'] = 'Week 5';
$string['nmp_week6'] = 'Week 6';

$string['nmp_00'] = '12:00am';
$string['nmp_01'] = '01:00am';
$string['nmp_02'] = '02:00am';
$string['nmp_03'] = '03:00am';
$string['nmp_04'] = '04:00am';
$string['nmp_05'] = '05:00am';
$string['nmp_06'] = '06:00am';
$string['nmp_07'] = '07:00am';
$string['nmp_08'] = '08:00am';
$string['nmp_09'] = '09:00am';
$string['nmp_10'] = '10:00am';
$string['nmp_11'] = '11:00am';
$string['nmp_12'] = '12:00pm';
$string['nmp_13'] = '01:00pm';
$string['nmp_14'] = '02:00pm';
$string['nmp_15'] = '03:00pm';
$string['nmp_16'] = '04:00pm';
$string['nmp_17'] = '05:00pm';
$string['nmp_18'] = '06:00pm';
$string['nmp_19'] = '07:00pm';
$string['nmp_20'] = '08:00pm';
$string['nmp_21'] = '09:00pm';
$string['nmp_22'] = '10:00pm';
$string['nmp_23'] = '11:00pm';

/* Enseignant général */
$string['tg_section_help_title'] = 'Global indicators';
$string['tg_section_help_description'] = 'This section contains views with general indicators related to course setup, resources assigned per week, study sessions and student progress through the course. The views in this section show indicators from the start date to the end date of the course (or the current date if the course is not yet finished).';
$string['tg_week_resources_help_title'] = 'Resources per week';
$string['tg_week_resources_help_description_p1'] = 'This graph displays the amount of resources for each of the course sections assigned to each study week configured in the <i>Configure Weeks</i> section. If a week has two or more course sections assigned to it, the resources of those sections are added together for the calculation of the total resources for a week. ';
$string['tg_week_resources_help_description_p2'] = 'On the x-axis of the graph are the total resources and activities of the sections assigned to each configured week of NoteMyProgress. On the y-axis are the configured study weeks. ';
$string['tg_weeks_sessions_help_title'] = 'Sessions per week';
$string['tg_week_sessions_help_description_p1'] = 'This graph shows the number of study sessions completed by students in each week from the start date of the course. The student\'s access to the course is considered as the start of a study session. A session is considered completed when the time elapsed between two student interactions exceeds 30 minutes. ';
$string['tg_week_sessions_help_description_p2'] = 'On the x-axis of the graph are the weeks of each month. The y-axis of the graph shows the different months of the year from the month of course creation. To maintain the symmetry of the graph, a total of five weeks has been placed for each month, however, each month does not have that many weeks. These months will only add sessions up to the fourth week.';
$string['tg_progress_table_help_title'] = 'Student progress';
$string['tg_progress_table_help_description'] = 'This table shows a list of all students enrolled in the course together with their progress, number of sessions and time spent. For the calculation of the progress all the resources of the course have been considered except for those of type <i>Label</i>. To determine whether a student has completed a resource, the first step is to check whether the resource has the completeness setting enabled. If so, we look to see if the student has already completed the activity based on that setting. Otherwise, the activity is considered complete if the student has viewed it at least once. ';

$string['nmp_title'] = 'Work sessions';
$string['table_title'] = 'Course progress';
$string['thead_name'] = 'First name';
$string['thead_lastname'] = 'Last name';
$string['thead_email'] = 'Mail';
$string['thead_progress'] = 'Progression (%)';
$string['thead_sessions'] = 'Sessions';
$string['thead_time'] = 'Time invested';

$string['nmp_module_label'] = 'ressource';
$string['nmp_modules_label'] = 'ressources';
$string['nmp_of_conector'] = 'of';
$string['nmp_finished_label'] = 'finished';
$string['nmp_finisheds_label'] = 'finished';

$string['nmp_smaller30'] = 'Under 30 minutes';
$string['nmp_greater30'] = 'Over 30 minutes';
$string['nmp_greater60'] = 'Over 60 minutes';

$string['nmp_session_count_title'] = 'Sessions of the week';
$string['nmp_session_count_yaxis_title'] = 'Number of Sessions';
$string['nmp_session_count_tooltip_suffix'] = ' sessions';

$string['nmp_hours_sessions_title'] = 'Sessions by day and time';
$string['nmp_weeks_sessions_title'] = 'Sessions per week';

$string["nmp_session_text"] = "session";
$string["nmp_sessions_text"] = "sessions";

$string['ss_change_timezone'] = 'Timezone:';
// $string['ss_activity_inside_plataform_student'] = 'Mon activité sur la plateforme';
// $string['ss_activity_inside_plataform_teacher'] = 'Activité des étudiants sur la plateforme';
// $string['ss_time_inside_plataform_student'] = 'Mon temps sur la plateforme';
// $string['ss_time_inside_plataform_teacher'] = 'Temps moyen passé par les étudiants sur la plateforme cette semaine';
// $string['ss_time_inside_plataform_description_teacher'] = 'Temps que l’élève a investi dans la semaine sélectionnée, par rapport au temps que l’enseignant a prévu de l’investir. Le temps passé affiché correspond à la moyenne de tous les élèves. Le temps prévu par l’enseignant est le temps attribué par l’enseignant dans <i> Configurer les semaines </i>. ';
// $string['ss_time_inside_plataform_description_student'] = 'Temps passé cette semaine par rapport au temps que l’enseignant a prévu de passer.';
// $string['ss_activity_inside_plataform_description_teacher'] = 'Les heures de la journée sont indiquées sur l\'axe Y et les jours de la semaine sur l\'axe X. Dans le graphique, vous pouvez trouver plusieurs points qui, en les survolant, offrent des informations détaillées sur les interactions des étudiants, regroupées par type de ressource (nombre d\'interactions, nombre d\'étudiants qui ont interagi avec la ressource et moyenne des interactions). <br/> <br/> <b> En cliquant sur les balises, vous pourrez filtrer par type de ressource, ne laissant visibles que celles qui ne sont pas barrées. </b> ';
// $string['ss_activity_inside_plataform_description_student'] = 'Afficher les interactions par type de ressource et planification. Lorsque vous survolez un point visible du graphique, vous verrez le nombre d\'interactions regroupées par type de ressource. En cliquant sur les balises, vous pourrez filtrer par type de ressource. ';

/* Sessions de l\'enseignant */
$string['ts_section_help_title'] = 'Study sessions';
$string['ts_section_help_description'] = 'This section contains visualisations related to student activity in the course measured in terms of sessions completed, average time spent in the course per week and study sessions at time intervals. The data presented in this section varies depending on the study week selected. ';
$string['ts_inverted_time_help_title'] = 'Time invested by students';
$string['ts_inverted_time_help_description_p1'] = 'This graph shows the average time spent by students during the week compared to the average time expected by the teacher.';
$string['ts_inverted_time_help_description_p2'] = 'On the x-axis of the graph is the number of hours the teacher has planned for a specific week. On the y-axis are the labels for average time spent and average time that should be spent. ';
$string['ts_hours_sessions_help_title'] = 'Sessions by day and time';
$string['ts_hours_sessions_help_description_p1'] = 'This graph shows the study sessions by day and time of the selected week. The student\'s access to the course is considered as the start of a study session. A session is considered finished when the time elapsed between two student interactions exceeds 30 minutes. ';
$string['ts_hours_sessions_help_description_p2'] = 'On the x-axis of the graph are the days of the week. On the y-axis are the hours of the day starting at 12am and ending at 11pm or 11pm.';
$string['ts_sessions_count_help_title'] = 'Sessions of the Week';
$string['ts_sessions_count_help_description_p1'] = 'This graph shows the number of sessions classified by their duration in time ranges: less than 30 minutes, more than 30 minutes and more than 60 minutes. The student\'s access to the course is considered as the start of a study session. A session is considered finished when the time elapsed between two student interactions exceeds 30 minutes.';
$string['ts_sessions_count_help_description_p2'] = 'On the x-axis of the graph are the days of the configured week. On the y-axis is the number of sessions held.';

$string['nmp_time_inverted_title'] = 'Time invested by students';
$string['nmp_time_inverted_x_axis'] = 'Number of hours';
$string['nmp_inverted_time'] = 'Average time invested';
$string['nmp_expected_time'] = 'Average time to invest';

$string['nmp_year'] = 'year';
$string['nmp_years'] = 'years';
$string['nmp_month'] = 'month';
$string['nmp_months'] = 'months';
$string['nmp_day'] = 'day';
$string['nmp_days'] = 'days';
$string['nmp_hour'] = 'hour';
$string['nmp_hours'] = 'hours';
$string['nmp_hours_short'] = 'h';
$string['nmp_minute'] = 'minute';
$string['nmp_minutes'] = 'minutes';
$string['nmp_minutes_short'] = 'm';
$string['nmp_second'] = 'second';
$string['nmp_seconds'] = 'seconds';
$string['nmp_seconds_short'] = 's';
$string['nmp_ago'] = 'ago';
$string['nmp_now'] = 'now';

/*Devoirs des enseignants */

$string['ta_section_help_title'] = 'Task monitoring';
$string['ta_section_help_description'] = 'This section contains indicators related to the delivery of assignments to drop-off areas and access to resources. The data presented in this section will vary depending on the study week selected. ';
$string['ta_assigns_submissions_help_title'] = 'Follow-up of homework submitted in the deposit areas';
$string['ta_assigns_submissions_help_description_p1'] = 'This graph shows the distribution of the number of students, in relation to the delivery status of an assignment in the drop-off areas.';
$string['ta_assigns_submissions_help_description_p2'] = 'On the x-axis of the graph are the names of the repositories for the selected week\'s sections with the date and time of expected delivery. On the y-axis is the distribution of the number of students according to the delivery status submitted (green), not submitted (red), submitted late (yellow). By clicking on the different areas of the graph, you can send an email to the groups of students you want (those who sent the assignment late or not, those who sent their assignment on time...).';
$string['ta_access_content_help_title'] = 'Access to the course content ';
$string['ta_access_content_help_description_p1'] = 'This graph shows the number of students who have and have not accessed the course resources. At the top are the different types of Moodle resources, with the ability to filter the graph information according to the type of resource selected. ';
$string['ta_access_content_help_description_p2'] = 'The x-axis of the graph shows the number of students enrolled in the course. The y-axis of the graph shows the section resources assigned for the week. In addition, this graph allows you to send an email to students who have accessed the resource or to those who have not by clicking on the graph. ';

/* Assign Submissions */
$string['nmp_intime_sub'] = 'Assignment submitted on time';
$string['nmp_late_sub'] = 'Late submission of assignments.';
$string['nmp_no_sub'] = 'Assignment not submitted';
$string['nmp_assign_nodue'] = 'No deadline';
$string['nmp_assignsubs_title'] = 'Follow-up of submitted assignments in the deposit areas';
$string['nmp_assignsubs_yaxis'] = 'Number of students';


/* Accès au contenu */
$string['nmp_assign'] = 'Assign';
$string['nmp_assignment'] = 'Assignment';
$string['nmp_attendance'] = 'Attendance';
$string['nmp_book'] = 'Book';
$string['nmp_chat'] = 'Chat';
$string['nmp_choice'] = 'Choice';
$string['nmp_data'] = 'Database';
$string['nmp_feedback'] = 'Feedback';
$string['nmp_folder'] = 'Folder';
$string['nmp_forum'] = 'Forum';
$string['nmp_glossary'] = 'Glossary';
$string['nmp_h5pactivity'] = 'H5P';
$string['nmp_imscp'] = 'IMS Content';
$string['nmp_label'] = 'Label';
$string['nmp_lesson'] = 'Lesson';
$string['nmp_lti'] = 'LTI Content';
$string['nmp_page'] = 'Page';
$string['nmp_quiz'] = 'Quiz';
$string['nmp_resource'] = 'Resource';
$string['nmp_scorm'] = 'Package SCORM';
$string['nmp_survey'] = 'Survey';
$string['nmp_url'] = 'Url';
$string['nmp_wiki'] = 'Wiki';
$string['nmp_workshop'] = 'Workshop';

$string['nmp_access'] = 'Accessed';
$string['nmp_no_access'] = 'No access';
$string['nmp_access_chart_title'] = 'Access to the course content';
$string['nmp_access_chart_yaxis_label'] = 'Number of students';
$string['nmp_access_chart_suffix'] = 'students';


/* Email */
$string['nmp_validation_subject_text'] = 'The subject is mandatory';
$string['nmp_validation_message_text'] = 'Please write a message';
$string['nmp_subject_label'] = 'Add a topic';
$string['nmp_message_label'] = 'Add a message';

$string['nmp_submit_button'] = 'Send';
$string['nmp_cancel_button'] = 'Cancel';
$string['nmp_close_button'] = 'Close';
$string['nmp_emailform_title'] = 'Send an e-mail';
$string['nmp_sending_text'] = 'Sending emails';

$string['nmp_recipients_label'] = 'To';
$string['nmp_mailsended_text'] = 'mails sent';

$string['nmp_email_footer_text'] = 'This is an email sent with NoteMyProgress.';
$string['nmp_email_footer_prefix'] = 'Go to';
$string['nmp_email_footer_suffix'] = 'for more information.';
$string['nmp_mailsended_text'] = 'Emails sent';

$string['nmp_assign_url'] = '/mod/assign/view.php?id=';
$string['nmp_assignment_url'] = '/mod/assignment/view.php?id=';
$string['nmp_book_url'] = '/mod/book/view.php?id=';
$string['nmp_chat_url'] = '/mod/chat/view.php?id=';
$string['nmp_choice_url'] = '/mod/choice/view.php?id=';
$string['nmp_data_url'] = '/mod/data/view.php?id=';
$string['nmp_feedback_url'] = '/mod/feedback/view.php?id=';
$string['nmp_folder_url'] = '/mod/folder/view.php?id=';
$string['nmp_forum_url'] = '/mod/forum/view.php?id=';
$string['nmp_glossary_url'] = '/mod/glossary/view.php?id=';
$string['nmp_h5pactivity_url'] = '/mod/h5pactivity/view.php?id=';
$string['nmp_imscp_url'] = '/mod/imscp/view.php?id=';
$string['nmp_label_url'] = '/mod/label/view.php?id=';
$string['nmp_lesson_url'] = '/mod/lesson/view.php?id=';
$string['nmp_lti_url'] = '/mod/lti/view.php?id=';
$string['nmp_page_url'] = '/mod/page/view.php?id=';
$string['nmp_quiz_url'] = '/mod/quiz/view.php?id=';
$string['nmp_resource_url'] = '/mod/resource/view.php?id=';
$string['nmp_scorm_url'] = '/mod/scorm/view.php?id=';
$string['nmp_survey_url'] = '/mod/survey/view.php?id=';
$string['nmp_url_url'] = '/mod/url/view.php?id=';
$string['nmp_wiki_url'] = '/mod/wiki/view.php?id=';
$string['nmp_workshop_url'] = '/mod/workshop/view.php?id=';
$string['nmp_course_url'] = '/course/view.php?id=';


/* Évaluation de l\'enseignant */
$string['tr_section_help_title'] = 'Suivi des notes';
$string['tr_section_help_description'] = 'This section contains indicators related to grade averages in assessable activities. The different teaching units (Qualification Categories) created by the teacher are displayed in the <i>Qualification Category </i> selector. This selector will allow you to switch between the different units defined and show the activities that can be assessed in each. ';
$string['tr_grade_items_average_help_title'] = 'Average of assessable activities';
$string['tr_grade_items_average_help_description_p1'] = 'This graph shows the average (percentage) score of students in each of the assessable activities in the course. The percentage average is calculated based on the maximum score for the assessable activity (e.g. an assessable activity with a maximum score of 80 and an average score of 26 will have a bar height of 33%, as 26 is 33% of the total score). The grade point average has been expressed in terms of percentages in order to preserve the symmetry of the graph, as Moodle allows you to create activities and assign custom grades. ';
$string['tr_grade_items_average_help_description_p2'] = 'On the x-axis of the graph are the different assessable activities of the course. On the y-axis is the weighted average expressed as a percentage. ';
$string['tr_grade_items_average_help_description_p3'] = 'Clicking on the bar for an assessable activity will update the data in the two lower graphs to show additional information about the selected assessable activity.';
$string['tr_item_grades_details_help_title'] = 'Best grade, worst grade and average grade';
$string['tr_item_grades_details_help_description_p1'] = 'This graph shows the best, average and worst score for an assessable activity (the activity selected in the table of average assessable activities).';
$string['tr_item_grades_details_help_description_p2'] = 'On the x-axis of the graph is the activity score, with the maximum activity score being the maximum value on this axis. On the y-axis are the labels for the best score, average score and worst score. ';
$string['tr_item_grades_distribution_help_title'] = 'Distribution of grades';
$string['tr_item_grades_distribution_help_description_p1'] = 'This graph shows the distribution of students in different grade ranges. The score ranges are calculated according to percentages. The following ranges are considered: less than 50%, more than 50%, more than 60%, more than 70%, more than 80% and more than 90%. These ranges are calculated according to the maximum weight that the teacher assigns to an assessable activity. ';
$string['tr_item_grades_distribution_help_description_p2'] = 'On the x-axis are the ranges of activity scores. On the y-axis is the number of students belonging to a certain rank. ';
$string['tr_item_grades_distribution_help_description_p3'] = 'By clicking on the bar corresponding to a rank, you can send an email to the students in the ranking.';

/* Notes */
$string['nmp_grades_select_label'] = 'Grade category';
$string['nmp_grades_chart_title'] = 'Averages of assessable activities';
$string['nmp_grades_yaxis_title'] = 'Average grade (%)';
$string['nmp_grades_tooltip_average'] = 'Average grade';
$string['nmp_grades_tooltip_grade'] = 'Highest grade';
$string['nmp_grades_tooltip_student'] = 'student graded from';
$string['nmp_grades_tooltip_students'] = 'students graded from';

$string['nmp_grades_best_grade'] = 'Highest grades';
$string['nmp_grades_average_grade'] = 'Average grade';
$string['nmp_grades_worst_grade'] = 'Worst grade';
$string['nmp_grades_details_subtitle'] = 'Best grade, worst grade and average grade';

$string['nmp_grades_distribution_subtitle'] = 'Distribution of grades';
$string['nmp_grades_distribution_greater_than'] = 'greater than';
$string['nmp_grades_distribution_smaller_than'] = 'lower than';
$string['nmp_grades_distribution_yaxis_title'] = 'Number of students';
$string['nmp_grades_distribution_tooltip_prefix'] = 'Range';
$string['nmp_grades_distribution_tooltip_suffix'] = 'in this range';
$string["nmp_view_details"] = "(Click to see details)";


/* Quiz enseignant */
$string['tq_section_help_title'] = 'Follow-up of evaluations';
$string['tq_section_help_description'] = 'This section contains indicators related to the summary of attempts in the various course evaluations and the analysis of evaluation questions. The data presented in this section varies according to the week of study selected and a selector containing all the Evaluation type activities in the course sections assigned to the selected week.';
$string['tq_questions_attempts_help_title'] = 'Attempted questions';
$string['tq_questions_attempts_help_description_p1'] = 'This graph shows the distribution of resolution attempts for each question in an assessment and their revision status.';
$string['tq_questions_attempts_help_description_p2'] = 'On the x-axis of the graph are the assessment questions. The y-axis shows the number of attempts to solve each of these questions. The symmetry of the graph will be affected by the assessment parameters (e.g. in an assessment that always has the same questions, the graph will show the same number of attempts for each bar corresponding to a question. In an assessment with random questions (from a question bank), the graph will show in the bar for each question the sum of the assessment attempts in which it appeared, and may not be the same for each assessment question). ';
$string['tq_questions_attempts_help_description_p3'] = 'By clicking on one of the bars corresponding to a question, it is possible to see the assessment question in a pop-up window.';
$string['tq_hardest_questions_help_title'] = 'More difficult questions';
$string['tq_hardest_questions_help_description_p1'] = 'This graph shows the assessment questions sorted by difficulty level. An attempt to solve a question with the status Partially correct, incorrect or blank is considered incorrect, so the total number of incorrect attempts for a question is the sum of attempts with the above statuses. The level of difficulty is represented as a percentage calculated on the basis of the total number of attempts. ';
$string['tq_hardest_questions_help_description_p2'] = 'On the x-axis of the graph are the assessment questions identified by name. The y-axis shows the percentage of incorrect attempts relative to the total number of attempts for the question. This axis helps to identify the questions that represented the greatest difficulty for the students who took the assessment. ';
$string['tq_hardest_questions_help_description_p3'] = 'By clicking on one of the bars corresponding to a question, it is possible to see the assessment question in a pop-up window.';

$string["nmp_quiz_info_text"] = "This evaluation has";
$string["nmp_question_text"] = "question";
$string["nmp_questions_text"] = "questions";
$string["nmp_doing_text_singular"] = "attempt made by";
$string["nmp_doing_text_plural"] = "attempts made by";
$string["nmp_attempt_text"] = "attempt";
$string["nmp_attempts_text"] = "attempts";
$string["nmp_student_text"] = "student";
$string["nmp_students_text"] = "students";
$string["nmp_quiz"] = "Assessments";
$string["nmp_questions_attempts_chart_title"] = "Attempted questions";
$string["nmp_questions_attempts_yaxis_title"] = "No. of attempts";
$string["nmp_hardest_questions_chart_title"] = "More difficult questions";
$string["nmp_hardest_questions_yaxis_title"] = "Incorrect attempts";
$string["nmp_correct_attempt"] = "Correct";
$string["nmp_partcorrect_attempt"] = "Partially correct";
$string["nmp_incorrect_attempt"] = "Wrong";
$string["nmp_blank_attempt"] = "Empty";
$string["nmp_needgraded_attempt"] = "Not rated";
$string["nmp_review_question"] = "(Click to review the question)";


/* Abandon */
$string['td_section_help_title'] = 'Dropout';
$string['td_section_help_description'] = 'This section contains indicators related to the prediction of student dropout in a course. The information is displayed in terms of student groups calculated by an algorithm that analyses the behaviour of each student in terms of the time invested, the number of student sessions, the number of days of activity and the interactions they have had with each resource and with other students in the course. The algorithm places students with similar behaviour in the same group, so that students who are increasingly unengaged in the course can be identified. The data presented in this section varies depending on the group selected in the selector that contains the groups identified in the course. ';
$string['td_group_students_help_title'] = 'Grouping students together';
$string['td_group_students_help_description_p1'] = 'This table shows the students who belong to the group selected in the student group selector. Each student\'s photo, names and percentage of progress in the course are listed. For the purposes of calculating progress, all the resources in the course have been taken into account, with the exception of those of the Label type. To determine if a student has completed a resource, it is first checked to see if the resource completeness setting is enabled. If so, it is checked to see if the student has already completed the activity based on that setting. Otherwise, the activity is considered complete if the student has seen it at least once. ';
$string['td_group_students_help_description_p2'] = 'Clicking on a student in this table will update the charts below with the selected student\'s information.';
$string['td_modules_access_help_title'] = 'Course resources';
$string['td_modules_access_help_description_p1'] = 'This graph shows the amount of resources the student has accessed and completed. The data presented in this graph varies depending on the student selected in the Student Group table. To determine the amount of resources and complete activities, the Moodle setting called Complete Activities is used. If the teacher does not set up the completeness of course activities, the number of activities accessed and completed will always be the same, as without such a set up, a resource is considered completed when the student accesses it. ';
$string['td_modules_access_help_description_p2'] = 'The x-axis shows the number of resources in the course. On the y-axis are the labels of the accessed, complete and total resources of the course. ';
$string['td_modules_access_help_description_p3'] = 'By clicking on any bar it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of student interactions with each resource and a label of not accessed, accessed or completed. ';
$string['td_week_modules_help_title'] = 'Resources per week';
$string['td_week_modules_help_description_p1'] = 'This graph shows the amount of resources accessed and completed by the student for each week configured in the plugin. The data presented in this graph varies depending on the student selected in the <i>Student Group</i> table. ';
$string['td_week_modules_help_description_p2'] = 'On the x-axis of the graph are the different study weeks configured. The y-axis shows the amount of resources and activities accessed and completed by the student. ';
$string['td_week_modules_help_description_p3'] = 'By clicking on any bar it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of student interactions with each resource and a label of not accessed, accessed or completed. ';
$string['td_sessions_evolution_help_title'] = 'Sessions and time spent';
$string['td_sessions_evolution_help_description_p1'] = 'This graph shows how study sessions have evolved since your first session was recorded in the course. The data presented in this graph varies depending on the student selected in the <i>Student Group</i> table. ';
$string['td_sessions_evolution_help_description_p2'] = 'The x-axis of the graph shows a timeline with the days that have elapsed since the student did the first study session until the day of the last recorded session. On the y-axis, they display 2 values, on the left side the number of student sessions and on the right side the time spent in hours. Between these axes, the number of sessions and the time spent by the student are drawn as a time series. ';
$string['td_sessions_evolution_help_description_p3'] = 'This visualisation allows you to zoom in on a selected region. This approach makes it possible to clearly show this development in different date ranges. ';
$string['td_user_grades_help_title'] = 'Grades';
$string['td_user_grades_help_description_p1'] = 'This graph shows a comparison of the student\'s grades with the grade averages (mean percentage) of their peers in the various assessable activities of the course. The data presented in this graph varies depending on the student selected in the <i>Student Group</i> table. ';
$string['td_user_grades_help_description_p2'] = 'The various assessable activities are displayed on the x-axis of the graph. On the y-axis are the student\'s grade and the average grade of their peers. The student\'s grade and the course average are displayed as a percentage to maintain the symmetry of the graph. ';
$string['td_user_grades_help_description_p3'] = 'With a click on the bar corresponding to an activity, it is possible to go to the analysed activity. ';

$string["nmp_cluster_label"] = "Group";
$string["nmp_cluster_select"] = 'Student group';
$string["nmp_dropout_table_title"] = "Students in the group";
$string["nmp_dropout_see_profile"] = "View profile";
$string["nmp_dropout_user_never_access"] = "Never accessed";
$string["nmp_dropout_student_progress_title"] = "Student's progression";
$string["nmp_dropout_student_grade_title"] = "Grade";
$string['nmp_dropout_no_data'] = "There is no dropout data for this course yet";
$string['nmp_dropout_no_users_cluster'] = "There are no students in this group";
$string['nmp_dropout_generate_data_manually'] = "Generate manually";
$string['nmp_dropout_generating_data'] = "Generating data...";
$string["nmp_modules_access_chart_title"] = "Course resources";
$string["nmp_modules_access_chart_series_total"] = "Total";
$string["nmp_modules_access_chart_series_complete"] = "Completed";
$string["nmp_modules_access_chart_series_viewed"] = "Accessed";
$string["nmp_week_modules_chart_title"] = "Resources per week";
$string["nmp_modules_amount"] = "Quantity of resources";
$string["nmp_modules_details"] = "(Click to view resources)";
$string["nmp_modules_interaction"] = "interaction";
$string["nmp_modules_interactions"] = "interactions";
$string["nmp_modules_viewed"] = "Accessed";
$string["nmp_modules_no_viewed"] = "Not accessed";
$string["nmp_modules_complete"] = "Completed";
$string["nmp_sessions_evolution_chart_title"] = "Sessions and time invested";
$string["nmp_sessions_evolution_chart_xaxis1"] = "No. of sessions";
$string["nmp_sessions_evolution_chart_xaxis2"] = "No. of hours";
$string["nmp_sessions_evolution_chart_legend1"] = "No. of sessions";
$string["nmp_sessions_evolution_chart_legend2"] = "Time invested";
$string["nmp_user_grades_chart_title"] = "Grades";
$string["nmp_user_grades_chart_yaxis"] = "Percentage grade";
$string["nmp_user_grades_chart_xaxis"] = "Assessable activities";
$string["nmp_user_grades_chart_legend"] = "Course (average)";
$string["nmp_user_grades_chart_tooltip_no_graded"] = "No rating";
$string["nmp_user_grades_chart_view_activity"] = "Click to view the activity";
$string['nmp_send_mail_to_user'] = 'e-mail ';
$string['nmp_send_mail_to_group'] = 'e-mail the group';


/* Général étudiant */
$string['sg_section_help_title'] = 'General indicators';
$string['sg_section_help_description'] = 'This section contains indicators related to your information, progress, general indicators, course resources, sessions throughout the course and grades. The displays in this section show indicators throughout the course (up to the current date). ';
$string['sg_modules_access_help_title'] = 'Course resources';
$string['sg_modules_access_help_description_p1'] = 'This graph shows the amount of resources you have accessed and completed. To determine how many resources you have completed, use the Moodle setting called Activity Completion. If the teacher has not set up the completeness of the course activities, the number of activities accessed and completed will always be the same, as without such a setting a resource is considered completed when you access it. ';
$string['sg_modules_access_help_description_p2'] = 'On the x-axis is the amount of resources in the course. On the y-axis are the labels of the accessible, complete and total resources in reference to your interactions with the course resources. ';
$string['sg_modules_access_help_description_p3'] = 'By clicking on any bar, it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of interactions you have had with each resource and a label of not accessed, accessed or completed. ';
$string['sg_weeks_session_help_title'] = 'Sessions per week';
$string['sg_weeks_session_help_description_p1'] = 'This graph shows the number of study sessions you have completed each week from the start date of the course. A session is considered completed when the time between two interactions exceeds 30 minutes. ';
$string['sg_weeks_session_help_description_p2'] = 'On the x-axis of the graph are the weeks of each month. The y-axis of the graph shows the different months of the year from the month of course creation. To maintain the symmetry of the graph, a total of five weeks has been placed for each month, however, each month does not have that many weeks. These months will only add sessions up to the fourth week. ';
$string['sg_sessions_evolution_help_title'] = 'Sessions and time invested';
$string['sg_sessions_evolution_help_description_p1'] = 'This graph shows how your study sessions have evolved since your first session was enrolled in the course. ';
$string['sg_sessions_evolution_help_description_p2'] = 'The x-axis of the graph shows a timeline with the days that have passed since your first study session to the day of your last recorded session. On the y-axis, they display 2 values, on the left side your number of sessions and on the right side your time spent in hours. Between these axes, your number of sessions and your time spent as a student are represented as a time series. ';
$string['sg_sessions_evolution_help_description_p3'] = 'This view allows you to zoom in on a selected region.';
$string['sg_user_grades_help_title'] = 'Grades';
$string['sg_user_grades_help_description_p1'] = 'This graph shows a comparison of your grades with the average grades (percentage average) of your classmates in the different assessable activities of the course.';
$string['sg_user_grades_help_description_p2'] = 'The x-axis of the graph shows the different activities that can be assessed. On the y-axis you will find your grades and the average grade of your classmates. Your grade and the course average are displayed as percentages to maintain the symmetry of the graph. ';
$string['sg_user_grades_help_description_p3'] = 'By clicking on the bar corresponding to an activity, it is possible to access the one analysed. ';

/* Sessions utilisateur */
$string['ss_section_help_title'] = 'Study sessions';
$string['ss_section_help_description'] = 'This section contains visualisations with indicators related to your activity in the course measured in terms of study sessions, time spent and progress in each of the weeks configured by the teacher. The displays in this section vary depending on the study week selected. ';
$string['ss_inverted_time_help_title'] = 'Your time invested';
$string['ss_inverted_time_help_description_p1'] = 'This graph shows your time spent in the week compared to the time planned by the teacher.';
$string['ss_inverted_time_help_description_p2'] = 'On the x-axis of the graph is the number of hours the teacher has scheduled for a specific week. On the y-axis are the labels for time spent and time to be spent. ';
$string['ss_hours_session_help_title'] = 'Sessions by day and time';
$string['ss_hours_session_help_description_p1'] = 'This graph shows your study sessions by day and time of the selected week. A session is considered completed when the time between two interactions exceeds 30 minutes. ';
$string['ss_hours_session_help_description_p2'] = 'On the x-axis of the graph are the days of the week. On the y-axis are the hours of the day starting at 12:00am and ending at 11:00am. ';
$string['ss_resources_access_help_title'] = 'Interaction by type of resource';
$string['ss_resources_access_help_description_p1'] = 'This graph shows how many resources you have pending and which ones you have already completed in the selected week. Resources are grouped by type in this graph. In addition, a bar is displayed at the top which represents the percentage of resources accessed in relation to the total resources assigned to the selected week. ';
$string['ss_resources_access_help_description_p2'] = 'The x-axis of the graph shows the different types of resources. The y-axis shows the amount of resources accessed for the week.';
$string['ss_resources_access_help_description_p3'] = 'By clicking on any bar, it is possible to see the resources and activities available in the course (in a pop-up window) as well as the number of interactions you have had with each resource and a label of not accessed, accessed or completed. ';


$string['nmp_student_time_inverted_title'] = 'Your time invested';
$string['nmp_student_time_inverted_x_axis'] = 'No. of hours';
$string['nmp_student_inverted_time'] = 'Time invested';
$string['nmp_student_expected_time'] = 'Time to invest';

$string['nmp_resource_access_title'] = 'Interaction by type of resource';
$string['nmp_resource_access_y_axis'] = 'Quantity of resources';
$string['nmp_resource_access_x_axis'] = 'Types of resources';
$string['nmp_resource_access_legend1'] = 'Completed';
$string['nmp_resource_access_legend2'] = 'Waiting';

$string['nmp_week_progress_title'] = 'Progress of the week';





/* time_visualizations */
$string['tv_title'] = 'Tiempo invertido (horario)';
$string['tv_lunes'] = 'Lunes';
$string['tv_martes'] = 'Martes';
$string['tv_miercoles'] = 'Miércoles';
$string['tv_jueves'] = 'Jueves';
$string['tv_viernes'] = 'Viernes';
$string['tv_sabado'] = 'Sábado';
$string['tv_domingo'] = 'Domingo';
$string['tv_axis_x'] = 'Días de la semana';
$string['tv_axis_y'] = 'Horas del día';
$string['tv_url'] = 'URL';
$string['tv_resource_document'] = 'Documento';
$string['tv_resource_image'] = 'Imagen';
$string['tv_resource_audio'] = 'Audio';
$string['tv_resource_video'] = 'Video';
$string['tv_resource_file'] = 'Archivo';
$string['tv_resource_script'] = 'Script/Código';
$string['tv_resource_text'] = 'Texto';
$string['tv_resource_download'] = 'Descargas';
$string['tv_assign'] = 'Tarea';
$string['tv_assignment'] = 'Tarea';
$string['tv_book'] = 'Libro';
$string['tv_choice'] = 'Elección';
$string['tv_feedback'] = 'Retroalimentación';
$string['tv_folder'] = 'Carpeta';
$string['tv_forum'] = 'Foro';
$string['tv_glossary'] = 'Glosario';
$string['tv_label'] = 'Etiqueta';
$string['tv_lesson'] = 'Lección';
$string['tv_page'] = 'Página';
$string['tv_quiz'] = 'Examen';
$string['tv_survey'] = 'Encuesta';
$string['tv_lti'] = 'Herramienta externa';
$string['tv_other'] = 'Otro';
$string['tv_interaction'] = 'Interacción por';
$string['tv_interactions'] = 'Interacciones por';
$string['tv_course_module'] = 'Módulo';
$string['tv_course_modules'] = 'Módulos';
$string['tv_student'] = 'Estudiante.';
$string['tv_students'] = 'Estudiantes.';
$string['tv_average'] = 'Promedio interacciones';
$string['tv_change_timezone'] = 'Zona horaria:';
$string['tv_activity_inside_plataform_student'] = 'Mi actividad en la plataforma';
$string['tv_activity_inside_plataform_teacher'] = 'Actividad de los estudiantes en la plataforma';
$string['tv_time_inside_plataform_student'] = 'Mi tiempo en la plataforma';
$string['tv_time_inside_plataform_teacher'] = 'Tiempo invertido en promedio de los estudiantes en la plataforma en esta semana';
$string['tv_time_inside_plataform_description_teacher'] = 'Tiempo que el estudiante ha invertido en la semana seleccionada, en comparación al tiempo que el/la docente planificó que se debería invertir. El tiempo invertido que se visualiza corresponde al promedio de todos los estudiantes. El tiempo planificado por el/la docente es el asignado en por el/la docente en <i>Configurar Semanas</i>.';
$string['tv_time_inside_plataform_description_student'] = 'Tiempo que ha invertido esta semana en relación al tiempo que el profesor planificó que se debería invertir.';
$string['tv_activity_inside_plataform_description_teacher'] = 'En el eje Y se indican las las horas del día y en el eje X los días de la semana. Dentro del gráfico podrá encontrar múltiples puntos, los cuales, al pasar el cursor sobre estos, ofrecen información detallada sobre las interacciones de los estudiantes, agrupadas por tipo de recurso (número de interacciones, número de estudiantes que interactuaron con el recurso y promedio de interacciones). <br/><br/><b>Al hacer click en las etiquetas, podrá filtrar por tipo de recurso, dejando visible sólo aquellos que no se encuentren tachados.</b>';
$string['tv_activity_inside_plataform_description_student'] = 'Presenta las interacciones por tipo de recurso y horario. Al pasar el cursor sobre un punto visible en el gráfico, verá el número de interacciones agrupadas por tipo de recurso. Al hacer click en las etiquetas, podrá filtrar por tipo de recurso.';
$string['tv_to'] = 'al';
$string['tv_time_spend'] = 'Tiempo invertido';
$string['tv_time_spend_teacher'] = 'Tiempo promedio invertido';
$string['tv_time_should_spend'] = 'Tiempo que deberías invertir';
$string['tv_time_should_spend_teacher'] = 'Tiempo promedio que se debería invertir';


/* Indicateurs de l\'enseignant */
$string['nmp_teacher_indicators_title'] = 'General indicators';
$string['nmp_teacher_indicators_students'] = 'Students';
$string['nmp_teacher_indicators_weeks'] = 'Weeks';
$string['nmp_teacher_indicators_grademax'] = 'Grades';
$string['nmp_teacher_indicators_course_start'] = 'Start the';
$string['nmp_teacher_indicators_course_end'] = 'End the';
$string['nmp_teacher_indicators_course_format'] = 'Format';
$string['nmp_teacher_indicators_course_completion'] = 'Completeness of the modules';
$string["nmp_teacher_indicators_student_progress"] = "Student progress";
$string["nmp_teacher_indicators_week_resources_chart_title"] = "Resources per week";
$string["nmp_teacher_indicators_week_resources_yaxis_title"] = "Quantity of resources";

/* Logs section */
$string['nmp_logs_title'] = 'Download the activity logs';
$string['nmp_logs_help_description'] = 'This section allows you to download the activity logs that have been performed. That is, you have access to the actions that have been carried out by registered users on the platform in a spreadsheet format.';
$string['nmp_logs_title_MoodleSetpoint_title'] = 'Select a date range for actions performed on Moodle';
$string['nmp_logs_title_MMPSetpoint_title'] = 'Select a date range for actions performed on NoteMyProgress';
$string['nmp_logs_help'] = 'This section allows you to download a log file of activities performed.';
$string['nmp_logs_select_date'] = 'Select a time interval for the log';
$string['nmp_logs_first_date'] = 'Start date';
$string['nmp_logs_last_date'] = 'End date';
$string['nmp_logs_valid_Moodlebtn'] = 'Download the Moodle activity log';
$string['nmp_logs_valid_NMPbtn'] = 'Download the NMP activity log';
$string['nmp_logs_invalid_date'] = 'Please enter a date';
$string['nmp_logs_download_btn'] = 'Download in progress';
$string['nmp_logs_download_nmp_help_title'] = 'About the actions carried out on NoteMyProgress';
$string['nmp_logs_download_moodle_help_title'] = 'About the actions carried out on Moodle';
$string['nmp_logs_download_nmp_help_description'] = 'The log file that is downloaded lists all the actions that have been performed by the user within the NoteMyProgress plugin only (viewing progress, viewing general indicators, etc.).';
$string['nmp_logs_download_moodle_help_description'] = 'The log file that is uploaded lists all the actions that have been performed by the user within Moodle only (viewing the course, viewing resources, submitting an assignment, etc).';




/* Logfiles + Logs section */
$string['nmp_logs_csv_headers_username'] = 'Username';
$string['nmp_logs_csv_headers_firstname'] = 'First name';
$string['nmp_logs_csv_headers_lastname'] = 'Last name';
$string['nmp_logs_csv_headers_date'] = 'Date';
$string['nmp_logs_csv_headers_hour'] = 'Hour';
$string['nmp_logs_csv_headers_action'] = 'Action';
$string['nmp_logs_csv_headers_coursename'] = 'Course name';
$string['nmp_logs_csv_headers_detail'] = 'Object_Name';
$string['nmp_logs_csv_headers_detailtype'] = 'Object_Type';

$string['nmp_logs_error_begin_date_superior'] = 'The start date cannot be later than the current date';
$string['nmp_logs_error_begin_date_inferior'] = 'The start date must be earlier than the end date';
$string['nmp_logs_error_empty_dates'] = 'Dates cannot be empty';
$string['nmp_logs_error_problem_encountered'] = 'A problem has been encountered, please try again';

$string['nmp_logs_success_file_downloaded'] = 'File uploaded!';

$string['nmp_logs_moodle_csv_headers_role'] = 'Role';
$string['nmp_logs_moodle_csv_headers_email'] = 'Email';
$string['nmp_logs_moodle_csv_headers_username'] = 'Username';
$string['nmp_logs_moodle_csv_headers_fullname'] = 'Fullname';
$string['nmp_logs_moodle_csv_headers_date'] = 'Date';
$string['nmp_logs_moodle_csv_headers_hour'] = 'Hour';
$string['nmp_logs_moodle_csv_headers_action'] = 'Action';
$string['nmp_logs_moodle_csv_headers_courseid'] = 'CourseID';
$string['nmp_logs_moodle_csv_headers_coursename'] = 'Course_name';
$string['nmp_logs_moodle_csv_headers_detailid'] = 'Detail ID';
$string['nmp_logs_moodle_csv_headers_details'] = 'Details';
$string['nmp_logs_moodle_csv_headers_detailstype'] = 'Details_type';

$string['nmp_logs_moodle_csv_headers_role_description'] = 'Gives the role the user has on the course on which they have taken an action (student, teacher...)';
$string['nmp_logs_moodle_csv_headers_email_description'] = 'Gives the user\'s e-mail address';
$string['nmp_logs_moodle_csv_headers_username_description'] = 'Gives the moodle username of the person who performed the action';
$string['nmp_logs_moodle_csv_headers_fullname_description'] = 'Gives the user\'s full name (First + Last)';
$string['nmp_logs_moodle_csv_headers_date_description'] = 'Gives the date on which the action was performed in the format dd-MM-YYYY';
$string['nmp_logs_moodle_csv_headers_hour_description'] = 'Gives the time at which the action was performed';
$string['nmp_logs_moodle_csv_headers_action_description'] = 'Give a verb describing the action that has been performed (e.g. clicked, viewed...)';
$string['nmp_logs_moodle_csv_headers_courseid_description'] = 'Gives the ID on which the action was performed';
$string['nmp_logs_moodle_csv_headers_coursename_description'] = 'Give the name of the price on which the action was carried out';
$string['nmp_logs_moodle_csv_headers_detailid_description'] = 'Gives the ID of the object with which the user has interacted';
$string['nmp_logs_moodle_csv_headers_details_description'] = 'Give the name of the object that was targeted';
$string['nmp_logs_moodle_csv_headers_detailstype_description'] = 'Gives the type of object that has been targeted (examples of objects: Repository, Quiz, Resources...)';

$string['nmp_logs_nmp_csv_headers_role_description'] = 'Gives the role the user has on the course on which they have taken an action (student, teacher...)';
$string['nmp_logs_nmp_csv_headers_email_description'] = 'Gives the user\'s e-mail address';
$string['nmp_logs_nmp_csv_headers_username_description'] = 'Gives the moodle username of the person who performed the action';
$string['nmp_logs_nmp_csv_headers_fullname_description'] = 'Gives the user\'s full name (First + Last)';
$string['nmp_logs_nmp_csv_headers_date_description'] = 'Gives the date on which the action was performed in the format dd-MM-YYYY';
$string['nmp_logs_nmp_csv_headers_hour_description'] = 'Gives the time at which the action was performed';
$string['nmp_logs_nmp_csv_headers_courseid_description'] = 'Gives the price identifier on which the action was performed';
$string['nmp_logs_nmp_csv_headers_section_name_description'] = 'Gives the name of the section of NoteMyProgress that the user was in when they performed the action';
$string['nmp_logs_nmp_csv_headers_action_type_description'] = 'Gives a full description of the action that was performed by the user in the form of verb + subject + object (e.g. downloaded_moodle_logfile)';

$string['nmp_logs_moodle_table_title'] = 'Description of headings';
$string['nmp_logs_moodle_table_subtitle'] = 'Regarding Moodle logs';

$string['nmp_logs_nmp_table_title'] = 'Description of headings';
$string['nmp_logs_nmp_table_subtitle'] = 'Regarding NoteMyProgress logs';

$string['nmp_logs_nmp_csv_headers_role'] = 'Role';
$string['nmp_logs_nmp_csv_headers_email'] = 'Email';
$string['nmp_logs_nmp_csv_headers_username'] = 'Username';
$string['nmp_logs_nmp_csv_headers_fullname'] = 'Fullname';
$string['nmp_logs_nmp_csv_headers_date'] = 'Date';
$string['nmp_logs_nmp_csv_headers_hour'] = 'Hour';
$string['nmp_logs_nmp_csv_headers_courseid'] = 'CourseID';
$string['nmp_logs_nmp_csv_headers_section_name'] = 'NMP_SECTION_NAME';
$string['nmp_logs_nmp_csv_headers_action_type'] = 'NMP_ACTION_TYPE';

$string['nmp_logs_table_title'] = 'Heading';
$string['nmp_logs_table_title_bis'] = 'Description';

$string['nmp_logs_help_button_nmp'] = 'About the actions carried out on NoteMyProgress';
$string['nmp_logs_help_button_moodle'] = 'About the actions carried out on Moodle';

$string['nmp_logs_download_details_link'] = 'Read more';
$string['nmp_logs_download_details_title'] = 'Are you sure you want a detailed explanation report?';
$string['nmp_logs_download_details_description'] = 'If you accept, a file in PDF format will be downloaded.';
$string['nmp_logs_download_details_ok'] = 'Download';
$string['nmp_logs_download_details_cancel'] = 'Cancel';
$string['nmp_logs_download_details_validation'] = 'The report has been downloaded';

/* NoteMyProgress admin settings */

$string['nmp_settings_bddusername_label'] = 'Database username';
$string['nmp_settings_bddusername_description'] = 'This parameter designates the username from which the MongoDB database can be accessed. If this parameter is entered, you will need to enter the password and the name of the database you wish to connect to.';
$string['nmp_settings_bddusername_default'] = 'Empty';

$string['nmp_settings_bddpassword_label'] = 'Account password';
$string['nmp_settings_bddpassword_description'] = 'This parameter is the password for the account from which the MongoDB database can be accessed. If this parameter is entered, you will need to enter the username and the name of the database you wish to connect to.';
$string['nmp_settings_bddpassword_default'] = 'Empty';


$string['nmp_settings_bddaddress_label'] = 'MongoDB server address *';
$string['nmp_settings_bddaddress_description'] = 'This parameter is the address from which the MongoDB database is accessible. This parameter is mandatory and is in the form: 151.125.45.58 or yourserver.com';
$string['nmp_settings_bddaddress_default'] = 'localhost';

$string['nmp_settings_bddport_label'] = 'Communication port *';
$string['nmp_settings_bddport_description'] = 'This parameter designates the port to be used to communicate with the database. This parameter is mandatory and must be a number.';
$string['nmp_settings_bddport_default'] = '27017';


$string['nmp_settings_bddname_label'] = 'Name of the database';
$string['nmp_settings_bddname_description'] = 'This parameter designates the name of the MongoDB database in which the information will be stored.';
$string['nmp_settings_bddname_default'] = 'Empty';

//Planning
/* Global */
$string['pagination'] = "Week:";
$string['graph_generating'] = "We are building the report, please wait a moment";
$string['txt_hour'] = "Time";
$string['txt_hours'] = "Hours";
$string['txt_minut'] = "Minutes";
$string['txt_minuts'] = "Minutes";
$string['pagination_week'] = "Week";
$string['only_student'] = "This report is for students only";
$string['sr_hour'] = "Time";
$string['sr_hours'] = "Hours";
$string['sr_minute'] = "Minute";
$string['sr_minutes'] = "Minutes";
$string['sr_second'] = "Seconds";
$string['sr_seconds'] = "Seconds";
$string['weeks_not_config'] = "The course has not been configured by the teacher, so you cannot use the reporting tool";
$string['helplabel'] = "Help";
$string['exitbutton'] = "Got it!";
$string['hours_unit_time_label'] = "Number of hours";

/* Menú */
$string['sr_menu_main_title'] = "Reports";
$string['sr_menu_setweek'] = "Set weeks";
$string['sr_menu_logs'] = "Download records";
$string['sr_menu_time_worked_session_report'] = "Study sessions per week";
$string['sr_menu_time_visualization'] = "Time spent (hours per week)";
$string['sr_menu_activities_performed'] = "Activities performed";
$string['sr_menu_notes'] = "Annotations";

/* General Errors */
$string['pluginname'] = "Note My Progress";
$string['previous_days_to_create_report'] = "Days taken into account for report construction";
$string['previous_days_to_create_report_description'] = "Days before the current date that will be taken into account to generate the report";
$string['student_reports:usepluggin'] = "Using the plug-in";
$string['student_reports:downloadreport'] = "Download the student's activity";
$string['student_reports:setweeks'] = "Set weeks";
$string['student_reports:activities_performed'] = "View the 'Activities Performed' report";
$string['student_reports:metareflexion'] = "View 'Metareflection' report";
$string['student_reports:notes'] = "Use notepads";

/* Logs */
$string['logs_title'] = "Download the student's log file";
$string['logs_description'] = "To download the log file (student's actions on Moodle), you need to click on the 'Generate logs' button. Later, a link will be activated that will allow the file to be downloaded.";
$string['logs_btn_download'] = "Generate log file";
$string['logs_label_creating'] = "As the log file is generated, it may take several minutes. Please wait...";
$string['logs_generation_finished'] = "The log file has been generated with success";
$string['logs_download_link'] = "Download the log file";
$string['logs_th_title_column'] = "Column";
$string['logs_th_title_description'] = "Description";
$string['logs_tb_title_logid'] = "Log ID";
$string['logs_tb_title_userid'] = "User ID";
$string['logs_tb_title_username'] = "User name";
$string['logs_tb_title_names'] = "Name";
$string['logs_tb_title_lastnames'] = "Last name";
$string['logs_tb_title_email'] = "E-mail";
$string['logs_tb_title_roles'] = "Roles";
$string['logs_tb_title_courseid'] = "Course ID";
$string['logs_tb_title_component'] = "Component";
$string['logs_tb_title_action'] = "Action";
$string['logs_tb_title_timecreated'] = "Date (Timestamp)";
$string['logs_tb_description_logid'] = "Number of the saved log file";
$string['logs_tb_description_userid'] = "Id of the person who generated the log file";
$string['logs_tb_description_username'] = "User registered in the platform of the person who generated the log file";
$string['logs_tb_description_names'] = "Names of the course participants";
$string['logs_tb_description_lastnames'] = "Last names of course participants";
$string['logs_tb_description_email'] = "Email of course participants";
$string['logs_tb_description_roles'] = "Role in the platform of the person who generated the log file";
$string['logs_tb_description_courseid'] = "Course from which the log file was generated";
$string['logs_tb_description_component'] = "Interactive course component";
$string['logs_tb_description_action'] = "Action that triggered the creation of the log file";
$string['logs_tb_description_timecreated'] = "Date in timestamp format in which the log file was generated";

/*new string*/
$string['studentreports'] = "Student reports";

/*Menu*/
$string['binnacle'] = "Blog";
$string['report1'] = "Report 1";
$string['report2'] = "Report 2";
$string['report3'] = "Report 3";
$string['report4'] = "Report 4";
$string['report5'] = "Report 5";
$string['set_weeks_title'] = "Setting up the course weeks";
$string['set_weeks'] = "Configuration of the course weeks";

/*Metareflexion*/
$string['metareflexion_goal1'] = "Learning something new";
$string['metareflexion_goal2'] = "Reviewing what I’ve learned";
$string['metareflexion_goal3'] = "Plan my learning";

$string['metareflexion_no_modules_in_section'] = "This week has no modules";
$string['compare_with_course'] = "Compare my results with those of my colleagues";
$string['sr_menu_metareflexion'] = "Metareflexion";
$string['metareflexion_last_week_created'] = "Last week's reflections have been saved";
$string['metareflexion_last_week_update'] = "Last week's reflections updated";
$string['metareflexion_update_planification_success'] = "Updated commitment";
$string['metareflexion_save_planification_success'] = "Commitment saved";
$string['metareflexion_tab_planification'] = "Planning for the week";
$string['metareflexion_tab_reflexion'] = "Reflection";
$string['metareflexion_tab_reflexion_title'] = "List of resources for this week";
$string['metareflexion_tab_lastweek'] = "Last week's reflections";
$string['metareflexion_tab_graphic_lastweek'] = "Reflections";
$string['metareflexion_effectiveness_title'] = "Metareflexion - Effectiveness Statistics";
$string['effectiveness_graphic_legend_pending'] = "Planned Resources";
$string['metareflexion_graphic_legend_unplanned'] = "No resources planned";
$string['effectiveness_graphic_legend_completed'] = "All resources completed";
$string['effectiveness_graphic_legend_failed'] = "Some resources have not been completed";
$string['metareflexion_cw_card_title'] = "Metareflexion - Planning for the week";
$string['metareflexion_cw_description_student'] = "In this section, you can plan your schedules, personal goals, and course work days for this week. This information will be used to calculate your efficiency in working on the course, which you can view in the Efficiency section.";
$string['metareflexion_cw_card_daysdedicate'] = "Days I will be working on the course:";
$string['metareflexion_cw_card_hoursdedicate'] = "Hours I will spend on the course:";
$string['metareflexion_cw_card_msg'] = "You have not yet planned how much time you want to spend on this resource.";
$string['metareflexion_cw_card_btn_plan'] = "Planning for the week";
$string['metareflexion_cw_table_title'] = "Resource planning by day";
$string['metareflexion_cw_table_thead_activity'] = "Activity";
$string['metareflexion_cw_table_thead_type'] = "Resource type";
$string['metareflexion_cw_table_thead_days_committed'] = "Dedicated days";
$string['metareflexion_cw_table_thead_hours_committed'] = "Dedicated hours";
$string['metareflexion_cw_table_thead_plan'] = "Plan";
$string['metareflexion_cw_dialog_daysdedicate'] = "Choose which days you plan to complete the various resources";
$string['metareflexion_cw_dialog_hoursdedicate'] = "How many hours of work do you plan to do this week?";
$string['metareflexion_cw_dialog_goalsdedicate'] = "What goals do you have for this week?";
$string['metareflexion_cw_dialog_btn_cancel'] = "Cancel.";
$string['metareflexion_cw_dialog_btn_accept'] = "Save the plan";
$string['metareflexion_pw_title'] = "Reflections from last week";
$string['metareflexion_pw_description_student'] = "In this section, you will be able to indicate how many face-to-face hours you spent on the course and how many you took outside of class time. In addition, you will be able to indicate if the learning objectives for the past week were met and give your opinion on the benefits of taking the course. This information will be provided to the teacher anonymously in order to improve the course.";
$string['metareflexion_pw_classroom_hours'] = "How many face-to-face class hours did you spend on this course this week?";
$string['metareflexion_pw_hours_off_course'] = "How many hours of work have you spent on the course outside of the Moodle platform?";
$string['metareflexion_pw_btn_save'] = "Save";

$string['metareflexion_report_title'] = "Graphic linked to the Planning/Meta-Reflection Commitment Form";
$string['metareflexion_report_description_days_teacher'] = "In this graph, you will find the planned days that students have committed to work (first bar) versus the days actually worked (second bar). If the first bar is not visible, it means that your students did not schedule any days to work for that week; they scheduled days to work. If the second bar is not visible, it means that the students did not do the work they committed to do on the scheduled day.";
$string['metareflexion_report_description_days_student'] = "Below the objectives, you will find a table indicating whether you have respected the schedule of the modules according to the days you have set. If the deadline is met, a green thumb will be displayed. In the event that the scheduled work is not completed, a red thumb will be displayed.<br> Note that a module is considered validated when it is consulted. Thus, to validate a day, you must consult all the modules that you have planned in the section 'Planning for the week'.<br>";
$string['metareflexion_report_subtitle_days_student'] = "My planned commitments for this week";
$string['metareflexion_report_subtitle_days_teacher'] = "Percentage of students who stuck to their plan that day";
$string['metareflexion_report_description_goals_student'] = "Below this graph is a reminder of the goals you set last week.";
$string['metareflexion_report_subtitle_hours_student'] = "My efficiency in the week.";
$string['metareflexion_report_subtitle_hours_teacher'] = "Average efficiency for the week";
$string['metareflexion_report_descrition_hours_teacher'] = "This graph shows the hours students planned to work during that week versus the hours actually worked.<br> Note that it is considered a working day if the student has done any interaction with the resources for that week. The scheduled time is obtained from the student's Meta-reflection in their Planning for the week.<br>";
$string['metareflexion_report_descrition_hours_student'] = "This graph shows the hours the student has scheduled to work during that week versus the hours actually worked and the average hours worked by students in the course. The expected time is obtained from the weekly metareflection you made in the 'Planning the Week' section.";
$string['metareflexion_report_description_meta_student'] = "Finally, in the last position you will find a questionnaire about the past week. Note that this form is not available for a week without planning or for the current week.";
$string['metareflexion_cw_day_lun'] = "Monday";
$string['metareflexion_cw_day_mar'] = "Tuesday";
$string['metareflexion_cw_day_mie'] = "Wednesday";
$string['metareflexion_cw_day_jue'] = "Thursday";
$string['metareflexion_cw_day_vie'] = "Friday";
$string['metareflexion_cw_day_sab'] = "Saturday";
$string['metareflexion_cw_day_dom'] = "Sunday";
$string['metareflexion_myself'] = "My";

$string['metareflexion_inverted_time'] = "Time invested";
$string['metareflexion_inverted_time_course'] = "Average time invested (course)";
$string['metareflexion_planified_time'] = "Planned time";
$string['metareflexion_planified_time_course'] = "Average time planned (course)";
$string['metareflexion_belonging'] = "Belonging to";
$string['metareflexion_student'] = "Students";
$string['metareflexion_interaction_user'] = "Users who have interacted with this resource";
$string['metareflexion_hover_title_teacher'] = "Student effectiveness (average)";
$string['metareflexion_hover_title_student'] = "Efficiency per week";
$string['metareflexion_planning_week'] = "He/She is planning";
$string['metareflexion_planning_week_start'] = ", starts on ";
$string['metareflexion_planning_week_end'] = "and ends on";
$string['metareflexion_report_pw_avegare_hour_clases'] = "Average number of hours spent in face-to-face classes";
$string['metareflexion_report_pw_avegare_hour_outside_clases'] = "Average hours spent outside of class";
$string['metareflexion_report_pw_summary_card'] = "Average time spent in class";
$string['metareflexion_report_pw_learning_objetives'] = "Regarding learning objectives";
$string['metareflexion_report_pw_attending_classes'] = "Concerning the benefit of taking classes";
$string['metareflexion_report_pw_text_graphic_no_data'] = "No data for this week.";
$string['metareflexion_report_pw_description_teacher'] = "In these graphs, you can see the information obtained from students about their impressions of the previous week's courses.";
$string['metareflexion_weekly_planning_subtitle'] = "See the results of your weekly planning under the 'Efficiency by week' tab.";
$string['metareflexion_saved_form'] = "You've already completed the form on Metareflexion this week.";
$string['metareflexion_goals_title'] = "Choosing personal goals";
$string['metareflexion_pres_question_time_invested_outside'] = "How much time have I invested in working outside the Moodle platform?";
$string['metareflexion_pres_question_objectives_reached'] = "Did I achieve my goals?";
$string['metareflexion_pres_question_pres_question_feel'] = "What do I think about this week?";
$string['metareflexion_pres_question_learn_and_improvement'] = "What did I learn and how can I improve?";
$string['metareflexion_goals_reflexion_title'] = "The goals chosen for this week.";
$string['metareflexion_title_hours_plan'] = "Forecasted work time for the current week.";
$string['metareflexion_title_retrospective'] = "Retrospective of the week";
$string['metareflexion_title_metareflexion'] = "Planning";

/* Reports */
$string['student_reports:view_report_as_teacher'] = "View report as teacher";
$string['student_reports:view_report_as_student'] = "View report as student";
$string['student_reports:time_visualizations'] = "Working on the platform";
$string['student_reports:time_worked_session_report'] = "Report on 'Work on the platform'";
$string['student_reports:write'] = "Save changes to reports";

/* time_worked_session_report */
$string['twsr_title'] = "Study sessions per week";
$string['twsr_description_student'] = "A session is the time spent interacting within the platform and the average time of the respective sessions. Below is the list of weeks with the total number of sessions. By moving the cursor over the graph, you can find detailed information about the group of students in the course.";
$string['twsr_description_teacher'] = "A session is the amount of time students interacted with the Moodle course material. In this report, you will find a list of weeks with the total number of sessions that students in the course have completed. By placing your cursor over one of the bars, you can see the total number of sessions, the average session length, and the number of different students who participated in each session.";
$string['twsr_axis_x'] = "Number of sessions";
$string['twsr_axis_y'] = "Weeks of classes";
$string['twsr_student'] = "Student";
$string['twsr_students'] = "Students";
$string['twsr_sessions_by'] = "Sessions belonging to";
$string['twsr_sessions_middle_time'] = "Average duration of sessions";
$string['twsr_total_sessions'] = "Total sessions";

/* time_visualizations */
$string['tv_title'] = "Time spent (by hour)";
$string['tv_moons'] = "Monday";
$string['tv_martes'] = "Tuesday";
$string['tv_miercoles'] = "Wednesday";
$string['tv_jueves'] = "Thursday";
$string['tv_viernes'] = "Friday";
$string['tv_sabado'] = "Saturday";
$string['tv_domingo'] = "Sunday";
$string['tv_axis_x'] = "Days of the week";
$string['tv_axis_y'] = "Hours of the day";
$string['tv_url'] = "URL";
$string['tv_resource_document'] = "Document";
$string['tv_resource_image'] = "Image";
$string['tv_resource_audio'] = "Audio";
$string['tv_resource_video'] = "Video";
$string['tv_resource_file'] = "File";
$string['tv_resource_script'] = "Script/Code";
$string['tv_resource_text'] = "Text";
$string['tv_resource_download'] = "Downloads";
$string['tv_assign'] = "Task";
$string['tv_assignment'] = "Task";
$string['tv_book'] = "Book";
$string['tv_choice'] = "Selection";
$string['tv_feedback'] = "Return";
$string['tv_folder'] = "Folder";
$string['tv_forum'] = "Forum";
$string['tv_glossary'] = "Glossary";
$string['tv_label'] = "Label";
$string['tv_lesson'] = "Lesson";
$string['tv_page'] = "Page";
$string['tv_quiz'] = "Quiz";
$string['tv_survey'] = "Survey";
$string['tv_lti'] = "External tool";
$string['tv_other'] = "Un autre";
$string['tv_interaction'] = "Interaction par";
$string['tv_interactions'] = "Interaction par";
$string['tv_course_module'] = "Module";
$string['tv_course_modules'] = "Module";
$string['tv_student'] = "Étudiant";
$string['tv_students'] = "Étudiants";
$string['tv_average'] = "Interactions moyennes";
$string['tv_change_timezone'] = "Fuseau horaire:";
$string['tv_activity_inside_plataform_student'] = "Mon activité sur la plateforme";
$string['tv_activity_inside_plataform_teacher'] = "Activité des élèves sur la plateforme";
$string['tv_time_inside_plataform_student'] = "Mon temps sur la plateforme";
$string['tv_time_inside_plataform_teacher'] = "Temps moyen passé par les étudiants sur la plateforme cette semaine";
$string['tv_time_inside_plataform_description_teacher'] = "Le temps que l'étudiant a investi dans la semaine choisie, comparé au temps que l'enseignant a prévu d'investir. Le temps passé affiché correspond à la moyenne de tous les étudiants. Le temps prévu par l'enseignant est le temps attribué dans <i>Planification de la semaine</i> par l'enseignant.";
$string['tv_time_inside_plataform_description_student'] = "Temps passé cette semaine par rapport au temps que l'enseignant avait prévu de passer.";
$string['tv_activity_inside_plataform_description_teacher'] = "L'axe des Y indique les heures de la journée et l'axe des X les jours de la semaine. Dans le graphique, vous pouvez trouver plusieurs points qui, lorsque vous passez le curseur dessus, fournissent des informations détaillées sur les interactions des élèves, regroupées par type de ressource (nombre d'interactions, nombre d'élèves ayant interagi avec la ressource et nombre moyen d'interactions). <b>By clicking on the labels, you can filter by resource type, leaving only those that are not crossed out visible</b>.";
$string['tv_activity_inside_plataform_description_student'] = "This graph shows interactions by resource type and time. When you move your cursor over a visible point on the graph, you will see the number of interactions grouped by resource type. By clicking on the labels, you can filter by resource type.";
$string['tv_to'] = "in the";
$string['tv_time_spend'] = "Time spent";
$string['tv_time_spend_teacher'] = "Average time spent";
$string['tv_time_should_spend'] = "Time spent";
$string['tv_time_should_spend_teacher'] = "Average time you should spend";


/* activities_performed */
$string['ap_title'] = "Activités réalisées";
$string['ap_description_student'] = "Ce rapport présente les activités réalisées et en cours cette semaine. La barre de progression de la semaine correspond au pourcentage d'activités à réaliser cette semaine et prévues par l'enseignant. L'option Semaine vous permet de choisir et de visualiser le rapport correspondant à la semaine choisie. Les activités sont regroupées par type de ressources. Les activités marquées en vert sont celles qui ont été réalisées et celles marquées en rouge sont celles qui sont en cours. Pour qu'une activité soit considérée comme terminée, vous devez avoir interagi au moins une fois avec la ressource. <br/> <br/> En cliquant sur les étiquettes terminées ou en attente, vous pourrez filtrer les informations.";
$string['ap_description_teacher'] = "Text en attente";
$string['ap_axis_x'] = "Type de ressources";
$string['ap_axis_y'] = "Montant des ressources";
$string['ap_progress'] = "Progrès de la semaine";
$string['ap_to'] = "dans la";
$string['ap_week'] = "Semaine";
$string['ap_activities'] = "Durée moyenne d'interaction pour resource";

/* activities_performed teacher */
$string['apt_title'] = "Performed course activities";
$string['apt_description'] = "This report shows the available course activities. The Week option allows you to select and view the report for that week. If you move the cursor over one of the bars, you will find the average views and interactions. If you click on the bar, you will find more details (student name, email, interactions, average viewing time).";
$string['apt_axis_x'] = "Interactions";
$string['apt_axis_y'] = "Resources";
$string['apt_visualization_average'] = "Average displays";
$string['apt_interacctions_average'] = "Average interactions";
$string['apt_thead_name'] = "Name";
$string['apt_thead_lastname'] = "Last name";
$string['apt_thead_email'] = "Email";
$string['apt_thead_interactions'] = "Interactions";
$string['apt_thead_visualization'] = "Average interaction time for resource";

/* Pagination component */
$string['pagination_component_to'] = "in the";
$string['pagination_component_name'] = "Week";

/* Questions and Answers */

$string['question_number_one'] = "Did you meet the learning objectives last week?";
$string['answers_number_one_option_one'] = "Yes";
$string['answers_number_one_option_two'] = "No";


$string['question_number_two'] = "What is your learning outcome?";
$string['answers_number_two_option_one'] = "I have a clear idea of the topic(s) and know how to apply it.";
$string['answers_number_two_option_two'] = "I have a clear idea of the topic(s) and not how to apply it.";
$string['answers_number_two_option_three'] = "I don't have a clear idea about the topic(s).";

$string['question_number_three'] = "How useful was participating in the face-to-face classes to you?";
$string['answers_number_three_option_one'] = "Nothing useful.";
$string['answers_number_three_option_two'] = "Somewhat helpful";
$string['answers_number_three_option_three'] = "Useful";
$string['answers_number_three_option_four'] = "Very useful";
$string['answers_number_three_option_five'] = "Really useful";
$string['answers_number_three_option_six'] = "I didn't go to class";

$string['question_number_four'] = "How do you feel about the past week?";
$string['answers_number_four_option_one'] = "I am not satisfied with the way I organized myself to achieve my goals.";
$string['answers_number_four_option_two'] = "I am quite satisfied with the way I have organized myself to achieve my goals";
$string['answers_number_four_option_three'] = "I am satisfied with the way I have organized myself to achieve my goals";
$string['answers_number_four_option_four'] = "I am very satisfied with the way I have organized myself to achieve my goals";

/* Auto-evaluation */
$string['qst1'] = "I set standards for my assignments in online courses.";
$string['qst2'] = "I set short-term (daily or weekly) goals as well as long-term goals (monthly or for the semester).";
$string['qst3'] = "I keep a high standard for my learning in my online courses.";
$string['qst4'] = "I set goals to help me manage studying time for my online courses.";
$string['qst5'] = "I don't compromise the quality of my work because it is online.";
$string['qst6'] = "I choose the location where I study to avoid too much distraction.";
$string['qst7'] = "I find a comfortable place to study.";
$string['qst8'] = "I know where I can study most efficiently for online courses.";
$string['qst9'] = "I choose a time with few distractions for studying for my online courses.";
$string['qst10'] = "I try to take more thorough notes for my online courses because notes are even more important for learning online than in a regular classroom.";
$string['qst11'] = "I read aloud instructional materials posted online to fight against distractions.";
$string['qst12'] = "I prepare my questions before joining in the chat room and discussion.";
$string['qst13'] = "I work extra problems in my online courses in addition to the assigned ones to master the course content.";
$string['qst14'] = "I allocate extra studying time for my online courses because I know it is time-demanding.";
$string['qst15'] = "I try to schedule the same time everyday or every week to study for my online courses, and I observe the schedule.";
$string['qst16'] = "Although we don't have to attend daily classes, I still try to distribute my studying time evenly across days.";
$string['qst17'] = "I find someone who is knowledgeable in course content so that I can consult with him or her when I need help.";
$string['qst18'] = "I share my problems with my classmates online so we know what we are struggling with and how to solve our problems.";
$string['qst19'] = "If needed, I try to meet my classmates face-to-face.";
$string['qst20'] = "I am persistent in getting help from the instructor through e-mail.";
$string['qst21'] = "I summarize my learning in online courses to examine my understanding of what I have learned.";
$string['qst22'] = "I ask myself a lot of questions about the course material when studying for an online course.";
$string['qst23'] = "I communicate with my classmates to find out how I am doing in my online classes.";
$string['qst24'] = "I communicate with my classmates to find out what I am learning that is different from what they are learning.";

$string['btn_save_auto_eval'] = "Save";
$string['message_save_successful'] = "Auto-evaluation successfully saved";
$string['message_update_successful'] = "Auto-evaluation successfully updated";
$string['page_title_auto_eval'] = "self-assessment";
$string['form_questions_auto_eval'] = "Self-assessment questions";
$string['description_auto_eval'] = "This page allows you to self-assess your study habits. This questionnaire is specific to each student but common to all courses on Moodle. Once you have completed it, you can save your answers with the button at the bottom of the page and change your choices whenever you wish.";

/* Groups */
$string['group_allstudent'] = "All students";


/* Admin Settings */
$string['nmp_use_navbar_menu'] = 'Activate new menu';
$string['nmp_use_navbar_menu_desc'] = 'Display the plugin menu in the top navigation bar, otherwise it will include the menu in the navigation block.';
/* Gamification */
$string['tg_tab1'] = "Chart";
$string['tg_tab2'] = "Ranking";
$string['tg_tab3'] = "My profile";
$string['EnableGame'] = "Disable/Enable :";
$string['studentRanking1'] = "Do you want to appear in the Ranking Board ?";
$string['studentRanking2'] = " /!\ All the registered in this course having accepted you will see. Accepting is final:";
$string['studentRanking3'] = "Accept";
$string['gamification_denied'] = 'gamification has been desactivated by your teacher';
$string['tg_colon'] = '{$a->a}: {$a->b}';
$string['tg_section_title'] = 'Course gamification settings';
$string['tg_section_help_title'] = 'Course gamification settings';
$string['tg_section_help_description'] = 'You can give the option of gamification to the students of the course.';
$string['tg_save_warning_title'] = "Are you sure you want to save the changes?";
$string['tg_save_warning_content'] = "If you change the settings for the gamification when the course has already started, data may be lost...";
$string['tg_confirm_ok'] = "Save";
$string['tg_confirm_cancel'] = "Cancel";
$string['tg_save'] = "Save configuration";
$string['tg_section_preview'] = 'Preview';
$string['tg_section_experience'] = 'Experience points';
$string['tg_section_information'] = 'Information';
$string['tg_section_ranking'] = 'Ranking/Report';
$string['tg_section_settings'] = 'Settings';

$string['tg_section_points'] = 'points';
$string['tg_section_description'] = 'Description';
$string['tg_section_no_description'] = 'No description';
$string['tg_section_no_name'] = 'No name';

$string['tg_section_preview_next_level'] = 'to the next level';
$string['tg_section_ranking_ranking_text'] = 'Ranking';
$string['tg_section_ranking_level'] = 'Level';
$string['tg_section_ranking_student'] = 'Student';
$string['tg_section_ranking_total'] = 'Total';
$string['tg_section_ranking_progress'] = 'Progress %';

$string['tg_section_settings_appearance'] = 'Appearance';
$string['tg_section_settings_appearance_title'] = 'Title';
$string['tg_section_settings_levels'] = 'Level settings';
$string['tg_section_settings_levels_quantity'] = 'Levels';
$string['tg_section_settings_levels_base_points'] = 'Base Points';
$string['tg_section_settings_levels_calculated'] = 'Automatic';
$string['tg_section_settings_levels_manually'] = 'Manually';
$string['tg_section_settings_levels_name'] = 'Name';
$string['tg_section_settings_levels_required'] = 'Points required';
$string['tg_section_settings_rules'] = 'Rules setting';
$string['tg_section_settings_add_rule'] = 'Add new rule';
$string['tg_section_settings_earn'] = 'For this event win:';
$string['tg_section_settings_select_event'] = 'Select an event';
$string['gm_Chart_Title'] = 'Spreading chart';
$string['gm_Chart_Y'] = 'Students';

$string['tg_timenow'] = 'Just now';
$string['tg_timeseconds'] = '{$a}s ago';
$string['tg_timeminutes'] = '{$a}m ago';
$string['tg_timehours'] = '{$a}h ago';
$string['tg_timedays'] = '{$a}d ago';
$string['tg_timeweeks'] = '{$a}w ago';
$string['tg_timewithinayearformat'] = '%b %e';
$string['tg_timeolderyearformat'] = '%b %Y';

/* General Errors */
$string['nmp_api_error_network'] = "An error has occurred in communication with the server.";
$string['nmp_api_invalid_data'] = 'Incorrect data';
$string['nmp_api_json_decode_error'] = 'Error Decoding sent data';
$string['nmp_api_invalid_token_error'] = 'The token sent in the transaction is invalid, please refresh the page';
$string['nmp_api_invalid_transaction'] = 'The request is incorrect';
$string['nmp_api_invalid_profile'] = 'You cannot do this action, your profile is incorrect';
$string['nmp_api_save_successful'] = 'The data has been successfully saved on the server';
$string['nmp_api_cancel_action'] = 'You have canceled the action';
$string['overview'] = 'Overview : ';
$string['game_point_error'] = 'Points are required';
$string['game_event_error'] = 'Event is required';
$string['game_name_error'] = 'Name required';
