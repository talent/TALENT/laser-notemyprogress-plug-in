<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_notemyprogress\external;

use coding_exception;
use dml_exception;
use function end;
use external_api;
use external_function_parameters;
use external_multiple_structure;
use external_single_structure;
use external_value;
use invalid_parameter_exception;
use moodle_exception;
use restricted_context_exception;
use stdClass;

defined('MOODLE_INTERNAL') || die();

/**
 * Class levels
 *
 * @package    mod_millionaire\external
 * @copyright  2019 Benedikt Kulmann <b@kulmann.biz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class external extends external_api {

    /**
     * Definition of parameters for {@see get_levels}.
     *
     * @return external_function_parameters
     */
    public static function get_levels_parameters() {
        return new external_function_parameters([
            'courseid' => new external_value(PARAM_INT, 'course id'),
        ]);
    }

    /**
     * Definition of return type for {@see get_levels}.
     *
     * @return external_multiple_structure
     */
        public static function get_levels_returns() {
        return new external_multiple_structure(
            level_dto::get_read_structure()
        );
    }

    /**
     * Get all levels.
     *
     * @param int $courseid
     *
     * @return array
     * @throws coding_exception
     * @throws dml_exception
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @throws restricted_context_exception
     */
    public static function get_levels($courseid) {
        $params = ['courseid' => $courseid];
        self::validate_parameters(self::get_levels_parameters(), $params);
        global $DB;
        $levelsData = $DB->get_record_select(
            "notemyprogress_levels_data",
            'courseid = ? AND timedeleted IS NULL', $params);

        if($levelsData){
            $levelsData->levelsdata = json_decode($levelsData->levelsdata);
            $levelsData->settings = json_decode($levelsData->settings);
            $levelsData->rules = json_decode($levelsData->rules);
        }

        return $levelsData;
    }
}