<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_notemyprogress
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_notemyprogress;

use stdClass;

class student extends report
{

    function __construct($courseid, $userid)
    {
        parent::__construct($courseid, $userid);
        self::set_profile();
        self::set_users();
    }

    // public function get_auto_eval_answers()
    // {
    //     global $DB;
    //     $sql =  "select q.description, a.level from {notemyprogress_auto_aws} a, {notemyprogress_auto_qst} q where a.auto_qst_id = q.id and a.userid = ? ";
    //     $aws = $DB->get_records_sql($sql, array($this->users[0]));
    //     if (empty($aws)){
    //         $aws = [];
    //         for ($i = 1; $i <= 24; $i++) {
    //             $aw = array("description"=> "qst" . $i,"level"=> 0);
    //             $aws["qst" . $i] = $aw;
    //         } 
    //     }
    //     return $aws;
    // }


    public function get_modules_completion($weekcode = null)
    {
        if (!self::course_in_transit()) {
            return null;
        }
        if (!self::course_has_users()) {
            return null;
        }
        $week = $this->current_week;

        if (!empty($weekcode)) {
            $week = self::find_week($weekcode);
        }

        $course_modules = self::get_course_modules_from_sections($week->sections);
        $course_modules = self::set_finished_modules($course_modules);
        $summary = self::summary_finished_course_modules($course_modules);
        return $summary;
    }

    private function set_finished_modules($course_modules)
    {
        $finished = self::get_finished_course_module($course_modules);
        foreach ($course_modules as $key => $course_module) {
            if (empty($course_module)) {
                continue;
            }
            $course_module->finished = false;
            $findies = array_filter($finished, function ($e) use ($course_module) {
                return $e->contextinstanceid == $course_module->id;
            });
            if (!empty($findies)) {
                $findies = array_values($findies);
                $course_module->finished = self::to_format("Y-m-d H:i:s", $findies[0]->timecreated);
            }
        }
        return $course_modules;
    }

    private function get_finished_course_module($course_modules)
    {
        if (empty($course_modules)) {
            return array();
        }
        global $DB;
        $finished = array();
        $course_modules = self::extract_ids($course_modules);
        list($users_in, $users_values) = $DB->get_in_or_equal($this->users);
        list($course_modules_in, $course_modules_values) = $DB->get_in_or_equal($course_modules);
        $sql_values = array_merge($users_values, $course_modules_values);
        $sql = "select * from {logstore_standard_log} where courseid = {$this->course->id} AND userid $users_in AND contextinstanceid $course_modules_in order by timecreated asc";
        $logs = $DB->get_recordset_sql($sql, $sql_values);
        foreach ($logs as $key => $log) {
            $findies = array_filter($finished, function ($e) use ($log) {return $e->contextinstanceid == $log->contextinstanceid;});
            if (count($findies) == 0) {
                array_push($finished, $log);
            }
        }
        $logs->close();
        return $finished;
    }

    private function summary_finished_course_modules($course_modules)
    {
        $groups = array();
        foreach ($course_modules as $key => $course_module) {
            if (empty($course_module)) {
                continue;
            }
            if ($course_module->modname == "resource") {
                $course_module->modname = self::get_resource_type($course_module->id);
            }

            if (!isset($groups[$course_module->modname])) {
                $group = new stdClass();
                $group->count_all = 0;
                $group->count_pending = 0;
                $group->count_finished = 0;
                $groups[$course_module->modname] = $group;
            }
            $current_group = $groups[$course_module->modname];
            $current_group->count_all++;
            if ($course_module->finished) {
                $current_group->count_finished++;
            } else {
                $current_group->count_pending++;
            }
        }
        return $groups;
    }


    /**
     * Stores the display profile of the class in the class variable $profile
     */
    public function set_profile()
    {
        $this->profile = "student";
    }

    /**
     * Stores the student id in the $users variable of the class.
     */
    public function set_users()
    {
        $this->users = array($this->user->id);
        return $this->users;
    }

    public function get_general_indicators()
    {
        if (!self::course_in_transit()) {
            return null;
        }
        if (!self::course_has_users()) {
            return null;
        }

        $start = null;
        if (isset($this->course->startdate) && ((int)$this->course->startdate) > 0) {
            $start = $this->course->startdate;
        }
        $end = null;
        if (isset($this->course->enddate) && ((int)$this->course->enddate) > 0) {
            $end = $this->course->enddate;
        }
        $enable_completion = false;
        if (isset($this->course->enablecompletion) && ((int)$this->course->enablecompletion) == 1) {
            $enable_completion = true;
        }

        $cms = self::get_course_modules();
        $cms = array_filter($cms, function ($cm) {
            return $cm['modname'] != 'label';
        });
        $cms = array_values($cms);

        $user_sessions = self::get_work_sessions($start, $end);

        $user = self::get_progress_table($user_sessions, $cms, $enable_completion, true);
        $user = $this->get_users_course_grade($user);
        $user = $this->get_users_items_grades($user);

        $sessions = array_map(function ($user_sessions) {
            return $user_sessions->sessions;
        }, $user_sessions);
        $sessions = self::get_sessions_by_weeks($sessions);
        $sessions = self::get_sessions_by_weeks_summary($sessions, (int) $this->course->startdate);

        $configweeks = new \local_notemyprogress\configweeks($this->course->id, $this->user->id);

        $response = new stdClass();
        $response->cms = $cms;
        $response->user = $user[0];
        $response->sessions = $sessions;
        $response->sections = $configweeks->current_sections;

        return $response;
    }

    public function get_sessions($weekcode = null, $include_weeks = true)
    {
        if (!self::course_in_transit()) {
            return null;
        }
        if (!self::course_has_users()) {
            return null;
        }

        $week = $this->current_week;
        if (!empty($weekcode)) {
            $week = self::find_week($weekcode);
        }

        $work_sessions = self::get_work_sessions($week->weekstart, $week->weekend);
        $sessions = array_map(function ($user_sessions) {
            return $user_sessions->sessions;
        }, $work_sessions);
        $sessions = self::get_sessions_by_hours($sessions);
        $sessions = self::get_sessions_by_hours_summary($sessions);

        $inverted_time = array_map(function ($user_sessions) {
            return $user_sessions->summary;
        }, $work_sessions);
        $inverted_time = self::calculate_average("added", $inverted_time);
        $inverted_time = self::get_inverted_time_summary($inverted_time, (int) $week->hours_dedications, false);

        $response = new stdClass();
        $response->hours_sessions = $sessions;
        $response->inverted_time = $inverted_time;

        if ($include_weeks) {
            $start = null;
            if (isset($this->course->startdate) && ((int)$this->course->startdate) > 0) {
                $start = $this->course->startdate;
            }
            $end = null;
            if (isset($this->course->enddate) && ((int)$this->course->enddate) > 0) {
                $end = $this->course->enddate;
            }
            $enable_completion = false;
            if (isset($this->course->enablecompletion) && ((int)$this->course->enablecompletion) == 1) {
                $enable_completion = true;
            }

            $work_sessions = self::get_work_sessions($start, $end);
            $cms = self::get_course_modules();
            $cms = array_filter($cms, function ($cm) {
                return $cm['modname'] != 'label';
            });
            $cms = array_values($cms);
            $user = self::get_progress_table($work_sessions, $cms, $enable_completion);

            $response->course_cms = $cms;
            $response->user_cms = $user[0]->cms->modules;
            //            $response->weeks = $this->weeks;
            //            $response->sections = $this->current_sections;
        }
        $response->sections = $week->sections;
        return $response;
    }
}
