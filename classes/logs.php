<?php


/**
 * Plugin logs functions are defined here.
 *
 * @package     local_notemyprogress
 * @author      2021 Éric Bart <bart.eric@hotmail.com>
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


namespace local_notemyprogress;
require_once dirname(__FILE__).'/../../../config.php';
require_once dirname(__FILE__) . '/../../../course/lib.php';
require 'server/vendor/autoload.php';
defined('MOODLE_INTERNAL') || die();

use MongoDB\Client as MongoDBLink;
use context_course;

class logs {

    public $beginDate;
    public $lastDate;
    public $courseid;
    public $userid;
    public $client;
    public $dbName;

    public function __construct($courseid, $userid) {
        $this->courseid=$courseid;
        $this->userid=$userid;
        self::connectmongoDB();
    }

    /**
     * Fonction permettant de se connecter à la base de données MongoDB distante ou locale
     */
    public function connectmongoDB() {
        global $DB;
        $connexionInfo = $DB->get_records("config_plugins", array("plugin" => "local_notemyprogress"), "name, value");
        //On récupère toutes les informations des paramètres du plugin préalablement fixés par l'utilisateur
        foreach($connexionInfo as $co) {
            if($co->name == "mongoDBlink") {
                $dbLink = $co;
            }
            if($co->name == "mongoDBname") {
                $dbName = $co;
            }
            if($co->name === "mongoDBpassword") {
                $dbPassword = $co;
            }
            if($co->name == "mongoDBport") {
                $dbPort = $co;
            }
            if($co->name == "mongoDBusername") {
                $dbUsername = $co;
            }
        }
        //Si les zones lien, port et nom de la base de donnée sont vides, on utilise des valeurs par défaut.
        if(empty($dbLink->value)) {
            $dbLink->value = "localhost";
        }
        if(empty($dbPort->value)) {
            $dbPort->value = "27017";
        }
        if(empty($dbName->value)) {
            $dbName->value = "logs_notemyprogress";
        }
        $this->dbName = $dbName->value;
        //On vérifie que le port est valide (contient 5 chiffres grand maximum)
	    $dbPortSize = str_split($dbPort->value);
	    if(count($dbPortSize)<=5) {
            //Si le username est vide, on se connecte sans username, password.
            //Sinon, on fait la connexion avec username, password.
		    if($dbUsername->value=="") {
			    $this->client = new MongoDBLink("mongodb://$dbLink->value:$dbPort->value");
		    }   else if(!empty($dbUsername->value)){
			    $this->client = new MongoDBLink("mongodb://$dbUsername->value:$dbPassword->value@$dbLink->value:$dbPort->value/?authSource=$dbName->value");
		}
	}
    }

    /**
     * Fonction permettant d'ajouter une trace réalisée sur NoteMyProgress, au sein de la base de données MongoDB sous forme de document JSON.
     * 
     * @param $actionType        | Verbe d'action qualifiant l'action qui a été réalisée
     * @param $objectType        | Type de l'objet avec lequel il y a eu une interraction
     * @param $sectionName       | Section NoteMyProgress depuis laquelle l'action a été réalisée
     * @param $objectName        | Nom de l'objet sur lequel il y a eu une interraction
     * @param $currentLink       | URL depuis lequel l'utilisateur se trouve au moment où a été réalisée l'action
     * @param $objectDescription | Paramètre facultatif permettant d'ajouter un descriptif à l'objet sur lequel l'action a été réalisée
     */
    public function addLogsNMP($actionType, $objectType, $sectionName, $objectName, $currentLink, $objectDescription = null) {
        global $DB;
        //On récupère toutes les informations de l'utilisateur que l'on souhaite enregistrer
        $rolename = self::getUserRole();
        $userInformations = self::getUserIdentification($this->userid);
        //On se connecte et on ajoute la trace au format JSON.
        $dbCollection = self::getDBCollection();
        $dbCollection->insertOne(['actor' =>
                                        ['objectType'=>$rolename,
                                         'mbox'=>$userInformations['email'],
                                         'name'=>$userInformations['firstname'].' '.$userInformations['lastname'],
                                         'account' =>
                                            ['name'=>$userInformations['username']]
                                     ],
                                     'verb' =>
                                         ['id'=>'https://www.irit.fr/laser/nmp/xapi/verbs/'.$actionType,
                                             'display'=>
                                                 ['en-US'=>$actionType]
                                         ],
                                         'object' =>
                                             ['id'=>'https://www.irit.fr/laser/nmp/xapi/object/'.$objectType,
                                              'objectType'=>$objectType,
                                              'definition' => [
                                                  'name'=>
                                                      ['en-US'=>$objectName],
                                                  'description'=>
                                                      ['en-US'=>$objectDescription]
                                              ],
                                              'context'=>
                                                  [ 'contextActivities'=>
                                                      ['grouping'=>$sectionName]
                                                  ]
                                              ]
                                                ,
                                     'context'=>
                                         ['platform' => 'Moodle',
                                              'contextActivities'=>
                                                  ['grouping'=>
                                                      ['id'=>$currentLink]
                                                  ]
                                         ],
                                     'timestamp'=>time(),
                                     'courseid'=>$this->courseid
                ]
        );
    }

    /**
     * Fonction permettant de récupérer les informations que l'on souhaite sur la base de données MongoDB situées entre un intervalle de dates
     * 
     * @param $beginDate    | Date de début de l'intervalle
     * @param $lastDate     | Date de fin de l'intervalle
     * @return array        | Données récupérées converties au format JSON
     */
    public function searchLogsNMP($beginDate, $lastDate)
    {
        $dbCollection = self::getDBCollection();
        //Ici, on utilise l'intervalle de dates pour aller chercher les informations dans la base de données
        $this->beginDate=$beginDate;
        $this->lastDate=$lastDate;
        $lastDate = strtotime("+1 day", strtotime($lastDate));
        $beginDate = strtotime($beginDate);
        $data = $dbCollection->find(
            ['$and'=>
                [['timestamp' => 
                     ['$gt'=>$beginDate,'$lt'=> $lastDate]], 
                ['courseid'=>$this->courseid]]
            ]
        );
        $jsonData = self::logsNMPToJSON($data);
        return $jsonData;
    }

    /**
     * Fonction permettant de convertir les données récoltées sur la base de données MongoDB en fichier JSON traitable en JavaScript ensuite
     * 
     * @param $data     | Tableau contenant toutes les données récoltées sur la base de données MongoDB
     * @return array    | Tableau mis au format JSON.
     */
    public function logsNMPToJSON($data) {
        //Ici, on va parcourir le résultat que nous a donné la précedente recherche pour le mettre sous la forme d'un tableau
        //dans le but d'aller le traiter avec du javascript après.
        $arrayData = [];
        foreach($data as $row) {
            $row->object->definition->name->{'en-US'} = self::accent_remover($row->object->definition->name->{'en-US'});
            if(str_contains($row->object->definition->name->{'en-US'}, '-') || str_contains($row->object->definition->name->{'en-US'}, ' ')) {
                $row->object->definition->name->{'en-US'} = str_replace(array(' ', '-'), array('_','_'), $row->object->definition->name->{'en-US'});
            }
            //$url = parse_url($row->context->contextActivities->grouping->id);
            //$url = parse_str($url['query'], $params);
            array_push($arrayData, array("user" => array(
                                            "fullname"=>$row->actor->name,
                                            "email"=>$row->actor->mbox,
                                            "username"=>$row->actor->account->name,
                                            "role"=>$row->actor->objectType,
                                            ),
                                    "time" => array(
                                            "timestamp"=>$data->timecreated,
                                            "date"=>date('d-m-y', $row->timestamp),
                                            "hour"=>date("H:i:s", $row->timestamp),
                                    ),
                                    "course" => array(
                                                "courseid"=>$row->courseid
                                    ),
                                    "action" => array(
                                                "sectionname"=>$row->object->context->contextActivities->grouping,
                                                "actiontype"=>$row->verb->display->{'en-US'} . '-' . $row->object->definition->name->{'en-US'} . '-' . $row->object->objectType
                                    )
                    ));
        }
        return $arrayData;
    }

    /**
     * Cette fonction a pour but d'aller chercher dans la base de donnée toutes les logs qui sont contenues entre
     * les deux dates qui sont spécifiées par l'utilisateur ($beginDate & $lastDate)
     *
     * @param $beginDate    | Date minimum de la recherche
     * @param $lastDate     | Date maximum de la recherche
     * @return mixed        | Tous les logs contenus entre cet interval
     */
    public function searchLogsMoodle($beginDate, $lastDate)
    {
        global $DB;
        $this->beginDate=$beginDate;
        $this->lastDate=$lastDate;
        $lastDate = strtotime("+1 day", strtotime($lastDate));
        $beginDate = strtotime($beginDate);
        $sql = "SELECT * from {logstore_standard_log} WHERE timecreated>={$beginDate} AND timecreated<={$lastDate} AND courseid={$this->courseid}";
        $data = $DB->get_records_sql($sql);
        $jsonData = self::logsMoodleToJSON($data);
        return $jsonData;
    }
    

    /**
     * Fonction permettant de convertir les données récoltées sur la base de données MariaDB de Moodle 
     * en fichier JSON traitable en JavaScript ensuite
     * 
     * @param $data  | Tableau contenant toutes les données récoltées sur la base de données MariaDB de Moodle
     * @return array | Tableau mis au format JSON.
     */
    public function logsMoodleToJSON($data) {
        $arrData = array();
        foreach ($data as $row) {
            $name = self::getUserIdentification($row->userid);
            $course = self::getCourse($row->courseid);
            $objectName = self::getDetail($row->objectid, $row->objecttable);
            $rolename = self::getUserRole();
            array_push($arrData, array("user" => array(
                                        "firstname"=>$name['firstname'],
                                        "lastname"=>$name['lastname'], 
                                        "email"=>$name['email'],
                                        "username"=>$name['username'],
                                        "userid"=>$name['id'],
                                        "role"=>$rolename
                                        ),
                                        "time" => array(
                                                "timestamp"=>$row->timecreated,
                                                "date"=>date("d-m-y", $row->timecreated),
                                                "hour"=>date("H:i:s", $row->timecreated)
                                        ),
                                        "course" => array(
                                                    "coursename"=>$course,
                                                    "courseid"=>$row->courseid
                                        ),
                                        "action" => array(
                                                    "actionverb"=>$row->action,
                                                    "objectid"=>$row->objectid,
                                                    "objectname"=>$objectName,
                                                    "objecttype"=>$row->objecttable
                                        )
            ));
        }
        json_encode($arrData);
        return $arrData;
    }


    /**
     * Fonction permettant de récupérer le rôle de l'utilisateur au moment où l'action a été réalisée (étudiant, enseignant...)
     * 
     * @return string | Le role de l'utilisateur
     */
    public function getUserRole() {
        $context = context_course::instance($this->courseid);
        $roles = get_user_roles($context, $this->userid);
        $rolename = "";
        foreach ($roles as $role) {
            $rolename = $role->shortname;
        }
        if(is_siteadmin($this->userid)) {
            $rolename = "administrator";
        }
        return $rolename;
    }

    /**
     * Fonction permettant de récupérer le nom de la base de données MongoDB spécifié et voulu 
     * par l'utilisateur dans les paramètres du plugin
     * 
     * @return string | Nom de la base de données MongoDB souhaité
     */
    public function getDBCollection() {
        global $DB;
        $connexionInfo = $DB->get_records("config_plugins", array("plugin" => "local_notemyprogress"), "name, value");
        $dbName = "";
        foreach($connexionInfo as $co) {   
            if($co->name == "mongoDBname") {
                if(empty($co->value)) {
                    $co->value = "logs_notemyprogress";
                    $dbName = "logs_notemyprogress";
                } else {
                   $dbName = $co->value;
                }
            }
        }
        $dbFolder = $this->client->$dbName;
        $dbCollection = $dbFolder->$dbName;
        return $dbCollection;
    }


    /**
     * Retourne un tableau associatif contenant l'username, le nom ainsi que le prénom de la personne visée
     *
     * @param $userid | Identifiant de l'utilisateur
     * @return array  | Tableau associatif contenant username, firstname & lastname de l'utilisateur
     */
    public function getUserIdentification($userid)
    {
        global $DB;
        $name = (array)$DB->get_record("user", array("id" => $userid), "id, username, lastname, firstname, email"); //On cherche le nom de la personne qui a fait la log
        if ($name) {
            if (empty($name['username'])) {
                $name['username'] = "Undefined";
            }
            if (empty($name['lastname'])) {
                $name['lastname'] = "Undefined";
            }
            if (empty($name['firstname'])) {
                $name['firstname'] = "Undefined";
            }
            if (empty($name['email'])) {
                $name['email'] = "Undefined";
            }
            return $name;
        }
        $name['username'] = "Not found";
        $name['username'] = "Not found";
        $name['firstname'] = "Not found";
        return $name;
    }

    /**
     * Retourne un tableau associatif contenant le nom complet du cours visé
     *
     * @param $courseid | Identifiant du cours
     * @return array    | Tableau associatif contenant le nom complet du cours
     */
    public function getCourse($courseid)
    {
        global $DB;
        $course = (array)$DB->get_record("course", array("id" => $courseid), "fullname");
        $course = self::accent_remover($course['fullname']);
        if ($course) {
            return $course;
        }
        $course = "";
        return $course;
    }

    /**
     * Fonction permettant de récupérer les informations précises des objets concernés par l'action qui a été réalisée (nom de l'objet)
     *
     * @param $objectid         | ID de l'objet visé (test, dépôt, etc..)
     * @param $datatable        | Table de la base de données où aller chercher l'information
     * @return string|string[]  | Informations de l'objet
     */
    public function getDetail($objectid, $datatable)
    {
        global $DB;
        if (!empty($objectid) && !empty($datatable)) {
            if ($datatable == 'assign' || $datatable == 'assignment' || $datatable == 'book'
                || $datatable == 'chat' || $datatable == 'choice' || $datatable == 'data' || $datatable == 'forum'
                || $datatable == 'glossary' || $datatable == 'imscp' || $datatable == 'lesson' || $datatable == 'label'
                || $datatable == 'lti' || $datatable == 'page' || $datatable == 'quiz' || $datatable == 'resource'
                || $datatable == 'scorm' || $datatable == 'url' || $datatable == 'wiki' || $datatable == 'workshop'
                || $datatable == 'folder' || $datatable == 'course_sections' || $datatable == "enrol") {
                $detail = (array)$DB->get_record($datatable, array("id" => $objectid), "name", "id");
                $detail = self::accent_remover($detail['name']);
            } else if ($datatable == 'grade_items') {
                $detail = (array)$DB->get_record($datatable, array("id" => $objectid), "itemname", "id");
                $detail = self::accent_remover($detail['itemname']);
            }
            return $detail;
        }
    }

    /**
     * Fonction permettant d'harmoniser le fichier csv avec un encodage sans accent, permet d'éviter d'eventuels
     * bugs de rendu
     *
     * @param $cadena           | chaîne à vérifier
     * @return string|string[]  | chaîne $cadena sans accent
     */
    public function accent_remover($cadena)
    {
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );
        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena);
        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena);
        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena);
        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena);
        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );
        return $cadena;
    }

}
?>
