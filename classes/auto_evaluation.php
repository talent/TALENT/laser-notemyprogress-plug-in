<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_notemyprogress
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_notemyprogress;

defined('MOODLE_INTERNAL') || die;

require_once('lib_trait.php');

use stdClass;


class auto_evaluation
{
    use \lib_trait;
    public $user;
    public $auto_eval_answers;
   
    function __construct($userid)
    {
        $this->user = self::get_user($userid);
    }
    
    public function is_saved(){
        global $DB;
        $sql =  "select * from {notemyprogress_auto_aws} a where a.userid = ? limit 1 ";
        $aws = $DB->get_records_sql($sql, array($this->user->id));
        return !(empty($aws));
    }

    public function get_auto_eval_answers()
    {
        global $DB;
        $sql =  "select q.description, a.level from {notemyprogress_auto_aws} a, {notemyprogress_auto_qst} q where a.auto_qst_id = q.id and a.userid = ? ";
        $aws = $DB->get_records_sql($sql, array($this->user->id));
        if (empty($aws)){
            $aws = [];
            for ($i = 1; $i <= 24; $i++) {
                $aw = array("description"=> "qst" . $i,"level"=> 0);
                $aws["qst" . $i] = $aw;
            } 
        }
        return $aws;
    }
    public function save_answers()
    {
        if (!isset($this->user) || !isset($this->auto_eval_answers)) {
            return false;
        }
        global $DB;
        $auto_evaluation = new stdClass();
        $auto_evaluation->userid = $this->user->id;
        foreach ($this->auto_eval_answers as $key => $answer) {
            $auto_evaluation->auto_qst_id = self::get_qst_id($answer->description);
            $auto_evaluation->level = $answer->level;
            $meta = $DB->insert_record("notemyprogress_auto_aws", $auto_evaluation, true);
        }
        return $meta;
    }

    public function update_answers()
    {
        if (!isset($this->user) || !isset($this->auto_eval_answers)) {
            return false;
        }
        global $DB;
        foreach ($this->auto_eval_answers as $key => $answer) {
            $sql = "update {notemyprogress_auto_aws} a, {notemyprogress_auto_qst} q set a.level = ? where a.auto_qst_id = q.id and a.userid = ? and q.id = ?";
            $meta = $DB->execute($sql, array($answer->level,$this->user->id,self::get_qst_id($answer->description)));
        }
        return $meta;
    }

    public function get_qst_id($desc)
    {
        return (int)filter_var($desc, FILTER_SANITIZE_NUMBER_INT);
    }
}
