<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * User sessions visualizations
 *
 * @package     local_notemyprogress
 * @author      Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_notemyprogress;

defined('MOODLE_INTERNAL') || die;

require_once('lib_trait.php');

use stdClass;

/**
 * Class report
 *
 * @author      Edisson Sigua
 * @author      Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class report
{
  use \lib_trait;

  const MINUTES_TO_NEW_SESSION = 30;
  const USER_FIELDS = "id, username, firstname, lastname, email, lastaccess, picture, deleted";
  protected $course;
  protected $user;
  protected $profile;
  protected $users;
  protected $current_week;
  protected $past_week;
  protected $weeks;
  protected $current_sections;
  public $timezone;

  function __construct($courseid, $userid)
  {
    $this->user = self::get_user($userid);
    $this->course = self::get_course($courseid);
    $this->timezone = self::get_timezone($userid);
    date_default_timezone_set($this->timezone);
    $this->users = array();
    $configweeks = new \local_notemyprogress\configweeks($this->course->id, $this->user->id);
    $this->weeks = $configweeks->weeks;
    $this->current_sections = $configweeks->current_sections;
    $this->current_week = $configweeks->get_current_week();
    $this->past_week = $configweeks->get_past_week();
  }

    /**
     * Stores the student id(s) in the $users variable of the class.
     */
  abstract public function set_users();
    
  /**
     * Stores the display profile of the class in the class variable $profile
  */
  abstract public function set_profile();

      
  /**
     * Get the information about the reflexion questions of a specific week
     *
     * @param string $weekcode code of the week assigned to the questions
     * @return object the informations about the questions
  */
  public function questions_report_metereflexion($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }

    $last_week = new stdClass();
    $last_week->userid = $this->user->id;
    $last_week->weekcode = $week->weekcode;
    $last_week->classroom_hours = 0;
    $last_week->hours_off_course = 0;
    $last_week->objectives_reached = null;
    $last_week->previous_class_learning = null;
    $last_week->benefit_going_class = null;
    $last_week->feeling = null;

    $query_last_week = self::get_last($week->weekcode);
    if ($query_last_week != null){
      $last_week = $query_last_week;
    }

    //Questions
    $questions = self::find_questions($last_week);
    $last_week->questions = $questions;

    return $last_week;
  }

  /**
     * Get the questions and answers selected of a specific week with their text assigned
     *
     * @param object $last_week the object containing the structure of the informations about the questions
     * @return object object containing the questions and answers
  */
  private function find_questions($last_week)
  {
      global $DB;
      $sql =  "select * from {notemyprogress_questions}";
      $questions = $DB->get_records_sql($sql);
      foreach ($questions as $key => $question) {
          $answers = self::find_answers($question->id);
          if ($question->id == 1) {
              $objectives_reached = isset($last_week->objectives_reached) ? $last_week->objectives_reached : null;
              $question->answer_selected = $objectives_reached;
          } elseif ($question->id == 2) {
              $previous_class_learning = isset($last_week->previous_class_learning) ? $last_week->previous_class_learning : null;
              $question->answer_selected = $previous_class_learning;
          } elseif ($question->id == 3) {
              $benefit_going_class = isset($last_week->benefit_going_class) ? $last_week->benefit_going_class : null;
              $question->answer_selected = $benefit_going_class;
          } elseif ($question->id == 4) {
              $feeling = isset($last_week->feeling) ? $last_week->feeling : null;
              $question->answer_selected = $feeling;
          }

          $question->answers = $answers;
      }
      $questions = self::update_texts_from_strings($questions);
      return $questions;
    }
  
    /**
     * Assign text corresponding to the questions and answers specified
     *
     * @param array $questions array containing questions and answers
     * @return array an array with the text added
  */
  private function update_texts_from_strings($questions)
  {
      foreach ($questions as $question) {
          $question->enunciated = get_string($question->enunciated, 'local_notemyprogress');
          foreach ($question->answers as $answer) {
              $answer->enunciated = get_string($answer->enunciated, 'local_notemyprogress');
          }
      }
      return $questions;
  }
    /**
     * Find the answers for a specific question id
     *
     * @param string $id_question id of the question
     * @return object the answer for the question id
     */
  public function find_answers($id_question)
  {
      global $DB;
      $sql =  "select * from {notemyprogress_answers} where questionid = ?";
      $answers = $DB->get_records_sql($sql, array($id_question));
      return $answers;
  }

    /**
     * Find the notemyprogress_last_weeks entry corresponding to a weekcode
     *
     * @param string $weekcode identifier of the week 
     * @return object the answer for the question id
     */
  public function get_last($weekcode){
    global $DB;
    $sql =  "select * from {notemyprogress_last_weeks} where weekcode = ? and userid = ? order by id desc limit 1";
    $last_week_query = $DB->get_record_sql($sql, array($weekcode, $this->user->id));
    return $last_week_query;
  }
    
  /**
     * Returns a array showing wich week have a weekschedule
     *
     * @return array an associtive array containing boolean values to show the weeks which have schedules
     */

  public function get_week_schedule()
  {
      global $DB;
      $weeks = $this->weeks;
      $array_ws = [];
      $sql =  "select * from {notemyprogress_wk} where userid = ?";
      $week_schedules = $DB->get_records_sql($sql, array($this->user->id));
      foreach($weeks as $week){
          $array_ws[$week->weekcode] = false;
          foreach($week_schedules as $week_schedule){
              if ($week->weekcode == $week_schedule->weekcode){
                  $array_ws[$week->weekcode] = true;
              }
          }
      }
      return $array_ws;
  }
  
  /**
     * Give the profile of the user
     *
     * @return string the profile of the user
     */
  public function render_has()
  {
    return $this->profile;
  }

  /**
   * Returns if the course is valid
   *
   * @return boolean course validity
  */
  protected function course_is_valid()
  {
    $in_transit = isset($this->current_week) || isset($this->past_week) ? true : false;
    $has_users = count($this->users) > 0 ? true : false;
    return $in_transit && $has_users;
  }

  /**
   * Check if the course is not yet finished or if the time elapsed since the end of the configured weeks of NoteMyProgress is less than a week.
   * configured weeks of Fliplearning is less than one week
   *
   * @return boolean boolean value indicating if the course is still active
   */
  protected function course_in_transit()
  {
    $in_transit = isset($this->current_week) || isset($this->past_week) ? true : false;
    return $in_transit;
  }

  /**
   * Verify if the course has students
   *
   * @return boolean boolean value indicating if the course has students
   */
  protected function course_has_users()
  {
    $has_users = count($this->users) > 0 ? true : false;
    return $has_users;
  }

  /**
   * Searches for the week with code equal to the $weekcode parameter and returns it.
   *
   * @param string $weekcode identifier of the week you want to get.
   *
   * @return object object with the week that matches the parameter
   */
  protected function find_week($weekcode)
  {
    foreach ($this->weeks as $week) {
      if ($weekcode == $week->weekcode) {
        return $week;
      }
    }
  }


  /**
   * Get informations about the course modules and the users
   *
   * @param array $users information about sessions
   * @param array $cms information about the modules
   * @param boolean $enable_completion if module completion is handled
   * @param boolean $include_sessions if sessions should be included in the value returned
   *
   * @return array an array containing objects with informations about the modules and the users
   */  
  protected function get_progress_table($users, $cms, $enable_completion, $include_sessions = false)
  {
    $table = array();
    $total_cms = count($cms);
    if ($total_cms > 0) {
      foreach ($users as $user) {
        $cms_interaction = self::cms_interactions($cms, $user, $enable_completion);
        $progress_percentage = (int)(($cms_interaction->complete * 100) / $total_cms);
        $inverted_time_label = self::convert_time($user->time_format, $user->summary->added, "hour");
        $user_record = self::get_user($user->userid);

        $record = new stdClass();
        $record->id = $user_record->id;
        $record->firstname = $user_record->firstname;
        $record->lastname = $user_record->lastname;
        $record->username = $user_record->username;
        $record->email = $user_record->email;
        $record->progress_percentage = $progress_percentage;
        $record->cms = $cms_interaction;
        $record->sessions_number = $user->summary->count;
        $record->inverted_time = $user->summary->added;
        $record->inverted_time_label = $inverted_time_label;

        if ($include_sessions) {
          $record->sessions = $user->sessions;
        }
        array_push($table, $record);
      }
    }
    return $table;
  }


  /**
   * Get informations about the course modules
   *
   * @param array $users information about sessions
   * @param array $cms information about the modules
   * @param boolean $cms_completion_enabled if module completion is handled
   *
   * @return object an object with informations about the modules
   */ 
  private function cms_interactions($cms, $user, $cms_completion_enabled)
  {
    $complete_cms = 0;
    $cms_ids = array();
    $viewed_cms = 0;
    foreach ($cms as $module) {
      $finished = null;
      if ($cms_completion_enabled) {
        $module_completion_configure = $module['completion'] != 0;
        if ($module_completion_configure) {
          $finished = self::finished_cm_by_conditions($user->userid, $module['id']);
        }
      }
      $interactions = self::count_cm_interactions($user, $module['id']);
      $viewed = ($interactions > 0);
      $finished = (!isset($finished)) ? $viewed : $finished;

      $cm = new stdClass();
      $cm->id = $module['id'];
      $cm->interactions = $interactions;
      $cm->complete = false;
      $cm->viewed = false;
      if ($viewed) {
        $viewed_cms++;
        $cm->viewed = true;
      }
      if ($finished) {
        $complete_cms++;
        $cm->complete = true;
      }
      if ($viewed || $finished) {
        $cmid = "cm" . $module['id'];
        $cms_ids[$cmid] = $cm;
      }
    }
    $interaction = new stdClass();
    $interaction->complete = $complete_cms;
    $interaction->viewed = $viewed_cms;
    $interaction->modules = $cms_ids;
    $interaction->total = count($cms);
    return $interaction;
  
  }
  /**
   * Returns if a module if a specific module is completed by a specific user
   *
   * @param string $userid id of the user
   * @param string $cm_id id of the module
   *
   * @return boolean if the module has beend completed of not
   */ 
  private function finished_cm_by_conditions($userid, $cm_id)
  {
    global $DB;
    $complete = false;
    $item = $DB->get_record('course_modules_completion', array('coursemoduleid' => $cm_id, 'userid' => $userid),'id, timemodified');
    if ($item) {
      $complete = true;
    }
    return $complete;
  }

    /**
   * Returns how many times a user has interacted with a course module
   *
   * @param object $user user
   * @param string $cm_id id of the module
   *
   * @return object interactions of the user on the module
   */ 

  private function count_cm_interactions($user, $cm_id)
  {
    $cm_logs = 0;
    foreach ($user->logs as $log) {
      if ($log->contextlevel == 70 && $log->contextinstanceid == $cm_id) {
        $cm_logs++;
      }
    }
    return $cm_logs;
  }


  protected function get_sessions_by_weeks($user_sessions)
  {
    $months = array();
    foreach ($user_sessions as $sessions) {
      foreach ($sessions as $session) {
        $resp = self::get_month_and_week_number((int) $session->start);
        $month = $resp->month;
        $week = $resp->week;

        if (!isset($months[$month])) {
          $months[$month] = array();
        }
        if (!isset($months[$month][$week])) {
          $months[$month][$week] = 1;
        } else {
          $months[$month][$week]++;
        }
      }
    }
    return $months;
  }

  protected function get_sessions_by_weeks_summary($months, $startdate)
  {
    $startdate = strtotime('first day of this month', $startdate);
    $month_number = ((int) date("n", $startdate)) - 1;

    $summary = array();
    $categories = array();
    $week_dates = array();
    if (!empty($months)) {
      for ($y = 0; $y <= 11; $y++) {
        $month_code = self::get_month_code($month_number);
        if (isset($months[$month_code])) {
          $weeks = $months[$month_code];
        }
        for ($x = 0; $x <= 4; $x++) {
          $value = 0;
          if (isset($weeks)) {
            if (isset($weeks[$x])) {
              $value = $weeks[$x];
            }
          }
          $element = array("x" => $x, "y" => $y, "value" => $value);
          array_push($summary, $element);
        }
        $weeks = null;

        $dates = self::get_weeks_of_month($startdate);
        array_push($week_dates, $dates);

        $month_number++;
        if ($month_number > 11) {
          $month_number = 0;
        }

        $month_name = get_string("nmp_" . $month_code . "_short", "local_notemyprogress");
        $year = date("Y", $startdate);
        $category_name = "$month_name $year";
        array_push($categories, $category_name);

        $startdate = strtotime('first day of +1 month', $startdate);
      }
    }
    $response = new stdClass();
    $response->data = $summary;
    $response->categories = $categories;
    $response->weeks = $week_dates;
    return $response;
  }

  private function get_month_code($key)
  {
    $months = array("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
    return $months[$key];
  }

  private function get_weeks_of_month($date)
  {
    $weeks = array();
    $month_code = strtolower(date("M", $date));
    $date = strtotime("first monday of this month", $date);
    while (strtolower(date("M", $date)) == $month_code) {

      $day_code = strtolower(date("D", $date));
      $start_day_name = get_string("nmp_$day_code", "local_notemyprogress");
      $start_day_number = strtolower(date("d", $date));

      $end = strtotime("+ 7 days", $date) - 1;
      $day_code = strtolower(date("D", $end));
      $end_day_name = get_string("nmp_$day_code", "local_notemyprogress");
      $end_day_number = strtolower(date("d", $end));

      $label = "$start_day_name $start_day_number - $end_day_name $end_day_number";
      array_push($weeks, $label);
      $month_code = strtolower(date("M", $date));
      $date = strtotime("+ 7 days", $date);
    }
    return $weeks;
  }

  private function get_month_and_week_number($date)
  {
    $monday_of_week = strtotime('monday this week', $date);
    $first_monday_month = strtotime("first monday of this month", $monday_of_week);
    $first_sunday_month = strtotime("+ 7 days", $first_monday_month) - 1;
    $week_number = 0;
    while ($first_sunday_month < $date) {
      $first_sunday_month = strtotime("+ 7 days", $first_sunday_month);
      $week_number++;
    }
    $resp = new stdClass();
    $resp->month = strtolower(date("M", $first_monday_month));
    $resp->week = $week_number;
    return $resp;
  }

  protected function get_sessions_by_hours($user_sessions)
  {
    $schedules = array();
    foreach ($user_sessions as $sessions) {
      foreach ($sessions as $session) {
        $start = (int) $session->start;
        $day = strtolower(date("D", $start));
        $hour = date("G", $start);

        if (!isset($schedules[$day])) {
          $schedules[$day] = array();
        }
        if (!isset($schedules[$day][$hour])) {
          $schedules[$day][$hour] = 1;
        } else {
          $schedules[$day][$hour]++;
        }
      }
    }
    return $schedules;
  }

  protected function get_sessions_by_hours_summary($schedules)
  {
    $summary = array();
    if (!empty($schedules)) {
      for ($x = 0; $x <= 6; $x++) {
        $day_code = self::get_day_code($x);
        if (isset($schedules[$day_code])) {
          $hours = $schedules[$day_code];
        }
        for ($y = 0; $y <= 23; $y++) {
          $value = 0;
          if (isset($hours)) {
            if (isset($hours[$y])) {
              $value = $hours[$y];
            }
          }
          $element = array(
            "x" => $x,
            "y" => $y,
            "value" => $value,
          );
          array_push($summary, $element);
        }
        $hours = null;
      }
    }
    return $summary;
  }

  public function get_inverted_time_summary($inverted_time, $expected_time, $average_time = true)
  {
    $response = new stdClass();
    $response->expected_time = $expected_time;
    $response->expected_time_converted = self::convert_time("hours", $expected_time, "string");
    $response->inverted_time = self::minutes_to_hours($inverted_time->average, -1);
    $response->inverted_time_converted = self::convert_time("hours", $response->inverted_time, "string");

    $inverted_time = new stdClass();
    $inverted_time->name = get_string("nmp_inverted_time", "local_notemyprogress");
    $inverted_time->y = $response->inverted_time;

    $expected_time = new stdClass();
    $expected_time->name = get_string("nmp_expected_time", "local_notemyprogress");
    $expected_time->y = $response->expected_time;

    if (!$average_time) {
      $inverted_time->name = get_string("nmp_student_inverted_time", "local_notemyprogress");
      $expected_time->name = get_string("nmp_student_expected_time", "local_notemyprogress");
    }
    $data[] = $inverted_time;
    $data[] = $expected_time;

    $response->data = $data;
    return $response;
  }

  protected function get_day_code($key)
  {
    $days = array("mon", "tue", "wed", "thu", "fri", "sat", "sun");
    return $days[$key];
  }

  protected function get_work_sessions($start, $end)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    $conditions = self::conditions_for_work_sessions($start, $end);
    $sessions_users = self::get_sessions_from_logs($conditions);
    return $sessions_users;
  }

  protected function get_sessions_from_logs($conditions)
  {
    $users = array();
    $user_logs = self::get_logs($conditions);
    foreach ($user_logs as $userid => $logs) {
      $sessions = self::get_sessions($logs);
      $summary = self::calculate_average("duration", $sessions);
      $active_days = self::get_active_days($logs);
      $user = new stdClass();
      $user->userid = $userid;
      $user->count_logs = count($logs);
      $user->active_days = $active_days;
      $user->time_format = "minutes";
      $user->summary = $summary;
      $user->sessions = $sessions;
      $user->logs = $logs;
      $users[] = $user;
    }
    return $users;
  }

  /**
   * Gets a list indexed by user id containing in each position the user's logs.
   *
   * @param array $filters list of conditions for searching the logs, if not specified, it is taken as an empty list.
   *
   * @return array list of users with their logs.
   */
  protected function get_logs($filters = array())
  {
    global $DB;
    $users = array();
    $conditions = self::get_query_from_conditions($filters);
    list($in, $invalues) = $DB->get_in_or_equal($this->users);
    $sql = "SELECT * FROM {logstore_standard_log} 
                WHERE courseid = {$this->course->id} {$conditions} AND userid $in ORDER BY timecreated ASC";
    $logs = $DB->get_recordset_sql($sql, $invalues);
    foreach ($logs as $key => $log) {
      if (!isset($users[$log->userid])) {
        $users[$log->userid] = array();
      }
      $users[$log->userid][] = $log;
    }
    $logs->close();
    foreach ($this->users as $userid) {
      if (!isset($users[$userid])) {
        $users[$userid] = array();
      }
    }
    return $users;
  }

  /**
   * Gets a text string representing a 'where' search condition in sql language.
   * whose fields are concatenated based on the $filters parameter with the $prefix prefix
   *
   * @param array $filters list of conditions for the text string representing the condition
   * @param string $prefix prefix with which each condition of the $filters variable is bound. If omitted, it defaults to
   * omitted, it defaults to and
   *
   * @return string string representing a 'where' conditional in the sql language.
   */

  private function get_query_from_conditions($filters = array(), $prefix = "and")
  {
    $conditions = "";
    foreach ($filters as $filter) {
      $operator = isset($filter->operator) ? $filter->operator : "=";
      $conditions .= " {$prefix} {$filter->field} {$operator} '{$filter->value}' ";
    }
    return $conditions;
  }


  private function get_sessions($logs)
  {
    $sessions = array();
    if (count($logs) == 0) {
      return $sessions;
    }
    $session = new stdClass();
    $session->duration = 0;
    $session->start = $logs[0]->timecreated;
    $session->end = null;;
    $previous = $logs[0];
    foreach ($logs as $key => $log) {
      $time_difference = self::diff_in_minutes($log->timecreated, $previous->timecreated);
      if ($time_difference >= self::MINUTES_TO_NEW_SESSION) {
        $session->end = $previous->timecreated;
        $session->duration = self::diff_in_minutes($session->end, $session->start);
        $sessions[] = $session;

        $session = new stdClass();
        $session->duration = 0;
        $session->start = $log->timecreated;
        $session->end = null;
      }
      $previous = $log;
    }
    if (!isset($session->end)) {
      $session->end = $previous->timecreated;
      $time_difference = self::diff_in_minutes($session->end, $session->start);
      $session->duration = $time_difference;
      $sessions[] = $session;
    }
    return $sessions;
  }

  private function diff_in_minutes($timestamp1, $timestamp2)
  {
    if (gettype($timestamp1) == "string") {
      $timestamp1 = (int) $timestamp1;
    }
    if (gettype($timestamp2) == "string") {
      $timestamp2 = (int) $timestamp2;
    }
    $interval = ($timestamp1 - $timestamp2) / 60;
    return $interval;
  }

  protected function calculate_average($field, $values, $consider_zero_elements = true)
  {
    $counter = 0;
    $total = 0;
    foreach ($values as $value) {
      if (gettype($value) == "object") {
        if (isset($value->$field)) {
          if (!$consider_zero_elements && $value->$field == 0) {
            continue;
          }
          $counter++;
          $total += $value->$field;
        }
      } elseif (gettype($value) == "array") {
        if (isset($value[$field])) {
          if (!$consider_zero_elements && $value[$field] == 0) {
            continue;
          }
          $counter++;
          $total += $value[$field];
        }
      }
    }

    $average = $counter > 0 ? ($total / $counter) : 0;
    $result = new stdClass();
    $result->count = $counter;
    $result->added = $total;
    $result->average = $average;
    return $result;
  }

  private function get_active_days($logs)
  {
    $days_count = 0;
    if (count($logs) == 0) {
      return $days_count;
    }
    $days = array();
    foreach ($logs as $key => $log) {
      $year = date("Y", $log->timecreated);
      $month = date("m", $log->timecreated);
      $day = date("d", $log->timecreated);
      $label = $year . $month . $day;
      if (!isset($days[$label])) {
        $days[$label] = 1;
      }
    }
    $days_count = count($days);
    return $days_count;
  }

  protected function get_users_course_grade($users)
  {
    global $DB;
    $item = $DB->get_record(
      'grade_items',
      array('courseid' => $this->course->id, 'itemtype' => 'course'),
      'id, courseid, grademax'
    );
    if ($item) {
      $sql = "SELECT id, userid, rawgrademax, finalgrade FROM {grade_grades} 
                WHERE itemid = {$item->id} AND finalgrade IS NOT NULL";
      $rows = $DB->get_records_sql($sql);
      $grades = array();
      foreach ($rows as $row) {
        $grades[$row->userid] = $row;
      }

      foreach ($users as $user) {
        $grade = new stdClass();
        $grade->finalgrade = 0;
        $grade->maxgrade = $item->grademax;
        if (isset($grades[$user->id])) {
          $grade->finalgrade = $grades[$user->id]->finalgrade;
        }
        $user->coursegrade = $grade;
      }
    } else {
      foreach ($users as $user) {
        $grade = new stdClass();
        $grade->finalgrade = 0;
        $grade->maxgrade = 0;
        $user->coursegrade = $grade;
      }
    }
    return $users;
  }

  protected function get_users_items_grades($users)
  {
    global $DB;
    $items = $this->get_grade_items();
    $items = $this->format_items($items);
    $items = $this->set_average_max_min_grade($items, $users);

    $itemsids = $this->extract_elements_field($items, 'id');
    if (count($itemsids) > 0) {
      list($in, $invalues) = $DB->get_in_or_equal($itemsids);
      $sql = "SELECT id, itemid, userid, finalgrade FROM {grade_grades} 
                WHERE itemid $in AND finalgrade IS NOT NULL ORDER BY itemid, userid";
      $rows = $DB->get_recordset_sql($sql, $invalues);

      $itemsgraded = array();
      foreach ($rows as $row) {
        $itemsgraded[$row->itemid][$row->userid] = $row;
      }
      $rows->close();

      foreach ($users as $user) {
        $useritems = array();
        foreach ($items as $item) {
          $useritem = new stdClass();
          $useritem->average = $item->average;
          $useritem->average_percentage = $item->average_percentage;
          $useritem->categoryid = $item->categoryid;
          $useritem->coursemoduleid = $item->coursemoduleid;
          $useritem->finalgrade = 0;
          $useritem->gradecount = $item->gradecount;
          $useritem->grademax = $item->grademax;
          $useritem->grademin = $item->grademin;
          $useritem->id = $item->id;
          $useritem->iteminstance = $item->iteminstance;
          $useritem->itemmodule = $item->itemmodule;
          $useritem->itemname = $item->itemname;
          $useritem->maxrating = $item->maxrating;
          $useritem->minrating = $item->minrating;
          if (isset($itemsgraded[$item->id][$user->id])) {
            $useritem->finalgrade = $itemsgraded[$item->id][$user->id]->finalgrade;
          }
          array_push($useritems, $useritem);
        }
        $user->gradeitems = $useritems;
      }
    }
    return $users;
  }

  protected function get_grade_categories()
  {
    global $DB;
    $sql = "SELECT * FROM {grade_categories} WHERE courseid = {$this->course->id} ORDER BY path";
    $result = $DB->get_records_sql($sql);
    $result = array_values($result);
    return $result;
  }

  protected function get_grade_items()
  {
    global $DB;
    $items = $DB->get_records(
      'grade_items',
      array('courseid' => $this->course->id, 'itemtype' => 'mod')
    );
    if (!$items) {
      $items = array();
    }

    //        $sql = "SELECT * FROM {grade_items} WHERE courseid = {$this->course->id} AND itemtype = 'mod' and gradetype = 1";
    //        $result = $DB->get_records_sql($sql);
    //        $result = array_values($result);
    return $items;
  }

  protected function format_items($items)
  {
    $response = array();
    foreach ($items as $item) {
      $format_item = new stdClass();
      $format_item->id = (int) $item->id;
      $format_item->categoryid = (int) $item->categoryid;
      $format_item->itemname = $item->itemname;
      $format_item->itemmodule = $item->itemmodule;
      $format_item->iteminstance = (int) $item->iteminstance;
      $format_item->grademax = (int) $item->grademax;
      $format_item->grademin = (int) $item->grademin;
      $coursemoduleid = $this->get_course_module_id($item);
      $format_item->coursemoduleid = $coursemoduleid;
      array_push($response, $format_item);
    }
    return $response;
  }

  protected function get_course_module_id($item)
  {
    global $DB;
    $coursemoduleid = false;
    if (isset($item->itemmodule)) {
      $result = $DB->get_record('modules', array('name' => $item->itemmodule), 'id', MUST_EXIST);
      $moduleid =  $result->id;
      $result = $DB->get_record('course_modules', array('course' => $this->course->id, 'module' => $moduleid, 'instance' => $item->iteminstance),'id', MUST_EXIST);
      $coursemoduleid = (int) $result->id;
    }
    return $coursemoduleid;
  }

  protected function set_average_max_min_grade($items, $users)
  {
    foreach ($items as $item) {
      $result = $this->get_average_max_min_grade($item->id);
      $grades = $this->get_item_grades($item->id, $users);
      $item->average_percentage = $this->convert_value_to_percentage($result->avg, $item->grademax);
      $item->average = $result->avg;
      $item->maxrating = $result->max;
      $item->minrating = $result->min;
      $item->gradecount = (int) $result->count;
      $item->grades = $grades;
    }
    return $items;
  }

  private function get_item_grades($itemid, $users)
  {
    global $DB;
    list($in, $invalues) = $DB->get_in_or_equal($this->users);
    $sql = "SELECT id, rawgrade, rawgrademax, rawgrademin, userid FROM {grade_grades} 
                WHERE itemid = {$itemid} AND rawgrade IS NOT NULL AND userid {$in}";
    $grades = $DB->get_records_sql($sql, $invalues);
    $grades = array_values($grades);
    foreach ($grades as $grade) {
      $grade->rawgrade = (int) $grade->rawgrade;
      $grade->rawgrademax = (int) $grade->rawgrademax;
      $grade->rawgrademin = (int) $grade->rawgrademin;
      $grade->userid = (int) $grade->userid;
      if (isset($users[$grade->userid])) {
        $grade->user = $users[$grade->userid];
      }
    }
    return $grades;
  }

  private function convert_value_to_percentage($value, $maxvalue)
  {
    $percentage = 0;
    if ($maxvalue > 0) {
      $percentage = ($value * 100) / $maxvalue;
    }
    return $percentage;
  }

  private function get_average_max_min_grade($itemid)
  {
    global $DB;
    list($in, $invalues) = $DB->get_in_or_equal($this->users);
    $sql = "SELECT COUNT(*) as count, MAX(rawgrade) as max, MIN(rawgrade) as min, AVG(rawgrade) as avg
                FROM {grade_grades} WHERE itemid = {$itemid} AND rawgrade IS NOT NULL AND userid {$in}";
    $result = $DB->get_records_sql($sql, $invalues);
    $result = array_values($result);
    return $result[0];
  }

  public function get_chart_langs()
  {
    $langs = array(
      "loading" => get_string("chart_loading", "local_notemyprogress"),
      "exportButtonTitle" => get_string("chart_exportButtonTitle", "local_notemyprogress"),
      "printButtonTitle" => get_string("chart_printButtonTitle", "local_notemyprogress"),
      "rangeSelectorFrom" => get_string("chart_rangeSelectorFrom", "local_notemyprogress"),
      "rangeSelectorTo" => get_string("chart_rangeSelectorTo", "local_notemyprogress"),
      "rangeSelectorZoom" => get_string("chart_rangeSelectorZoom", "local_notemyprogress"),
      "downloadPNG" => get_string("chart_downloadPNG", "local_notemyprogress"),
      "downloadJPEG" => get_string("chart_downloadJPEG", "local_notemyprogress"),
      "downloadPDF" => get_string("chart_downloadPDF", "local_notemyprogress"),
      "downloadSVG" => get_string("chart_downloadSVG", "local_notemyprogress"),
      "downloadCSV" => get_string("chart_downloadCSV", "local_notemyprogress"),
      "downloadXLS" => get_string("chart_downloadXLS", "local_notemyprogress"),
      "exitFullscreen" => get_string("chart_exitFullscreen", "local_notemyprogress"),
      "hideData" => get_string("chart_hideData", "local_notemyprogress"),
      "noData" => get_string("chart_noData", "local_notemyprogress"),
      "printChart" => get_string("chart_printChart", "local_notemyprogress"),
      "viewData" => get_string("chart_viewData", "local_notemyprogress"),
      "viewFullscreen" => get_string("chart_viewFullscreen", "local_notemyprogress"),
      "resetZoom" => get_string("chart_resetZoom", "local_notemyprogress"),
      "resetZoomTitle" => get_string("chart_resetZoomTitle", "local_notemyprogress"),
      "months" => array(
        get_string("nmp_jan", "local_notemyprogress"),
        get_string("nmp_feb", "local_notemyprogress"),
        get_string("nmp_mar", "local_notemyprogress"),
        get_string("nmp_apr", "local_notemyprogress"),
        get_string("nmp_may", "local_notemyprogress"),
        get_string("nmp_jun", "local_notemyprogress"),
        get_string("nmp_jul", "local_notemyprogress"),
        get_string("nmp_aug", "local_notemyprogress"),
        get_string("nmp_sep", "local_notemyprogress"),
        get_string("nmp_oct", "local_notemyprogress"),
        get_string("nmp_nov", "local_notemyprogress"),
        get_string("nmp_dec", "local_notemyprogress"),
      ),
      "shortMonths" => array(
        get_string("nmp_jan_short", "local_notemyprogress"),
        get_string("nmp_feb_short", "local_notemyprogress"),
        get_string("nmp_mar_short", "local_notemyprogress"),
        get_string("nmp_apr_short", "local_notemyprogress"),
        get_string("nmp_may_short", "local_notemyprogress"),
        get_string("nmp_jun_short", "local_notemyprogress"),
        get_string("nmp_jul_short", "local_notemyprogress"),
        get_string("nmp_aug_short", "local_notemyprogress"),
        get_string("nmp_sep_short", "local_notemyprogress"),
        get_string("nmp_oct_short", "local_notemyprogress"),
        get_string("nmp_nov_short", "local_notemyprogress"),
        get_string("nmp_dec_short", "local_notemyprogress"),
      ),
      "weekdays" => array(
        get_string("nmp_sun", "local_notemyprogress"),
        get_string("nmp_mon", "local_notemyprogress"),
        get_string("nmp_tue", "local_notemyprogress"),
        get_string("nmp_wed", "local_notemyprogress"),
        get_string("nmp_thu", "local_notemyprogress"),
        get_string("nmp_fri", "local_notemyprogress"),
        get_string("nmp_sat", "local_notemyprogress"),
      ),
      "shortWeekdays" => array(
        get_string("nmp_sun_short", "local_notemyprogress"),
        get_string("nmp_mon_short", "local_notemyprogress"),
        get_string("nmp_tue_short", "local_notemyprogress"),
        get_string("nmp_wed_short", "local_notemyprogress"),
        get_string("nmp_thu_short", "local_notemyprogress"),
        get_string("nmp_fri_short", "local_notemyprogress"),
        get_string("nmp_sat_short", "local_notemyprogress"),
      ),
    );
    return $langs;
  }

  //Planning

  /**
   * Get an array representing the status of planned days (planned/pending/completed/failed) for a specific week
   *
   * @param string $weekcode code of the week
   *
   * @return array an array with the status of the days of the week
   */ 
  public function status_planning($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    $tz = self::get_timezone();
    date_default_timezone_set($tz);
    $planifications = self::days_report_metereflexion($weekcode);
    $status = self::get_respect_planning($planifications,$week);
    return $status;
  }


    /**
   * Returns if the day is completed or not
   *
   * @param object $day day specified
   * @param string $userid id of the user
   * @param object $course_sessions sessions of the course
   *
   * @return boolean if the day is completed or not
   */
  private function create_planifications_week_summary()
  {
    $summary = new stdClass();
    $summary->count_planned = 0;
    $summary->count_accomplished = 0;
    $summary->students = array();
    $week = array(
      'lun' => clone $summary,
      'mar' => clone $summary,
      'mie' => clone $summary,
      'jue' => clone $summary,
      'vie' => clone $summary,
      'sab' => clone $summary,
      'dom' => clone $summary,
    );
    return $week;
  }


  /**
   * Get the schedule for a specific week and the current user
   *
   * @param string $weekcode code of the week
   *
   * @return object the schedule for a specific week and the current user
   */
  private function get_schedule($weekcode)
  {
    global $DB;
    $planifications = $DB->get_records('notemyprogress_wk', array('weekcode' => $weekcode,'userid' => $this->user->id));
    return reset($planifications);
  }


  /**
   * Get the informations about the modules planned and worked for each day of a specific week
   *
   * @param string $weekcode code of the week
   *
   * @return object the informations about the modules planned and worked
   */
  public function days_report_metereflexion($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }

    $days_module_planned =  self::get_modules_days_planned($week->weekcode);
    
    $work_sessions = self::get_work_sessions($week->weekstart, $week->weekend);
    $days_module_worked = self::get_modules_days_worked($work_sessions);


    $data_report_meta = new stdClass();
    $data_report_meta->days_planned = $days_module_planned;
    $data_report_meta->days_worked = $days_module_worked;
    return $data_report_meta;
  }


    /**
   * Gets the module id from mdl_course_modules with the instance and the type of the 'mdl_{type}' table"
   *
   * @param string $instance instance int the 'mdl_{type}' table"
   * @param string $type name of the table
   *
   * @return object the id of the module
   */

  protected function get_module_id($instance, $type)
  {
    global $DB;
    $sql = "select cm.id from {course_modules} cm,{modules} m where cm.module = m.id and cm.instance = ? and m.name = ?";
    $id = $DB->get_record_sql($sql, array($instance, $type));
    return $id;
  }


  /**
   * Get the modules accessed for each day of the specified week and for the current user 
   * 
   * @param object $work_sessions informations containing the sessions and the logs of the current user
   *
   * @return object modules accessed for each week
   */
  protected function get_modules_days_worked($work_sessions)
  {
    $days_worked = array(
      "monday" => array(),
      "tuesday" => array(),
      "wednesday" => array(),
      "thursday" => array(),
      "friday" => array(),
      "saturday" => array(),
      "sunday" => array(),
    );
    $logs = $work_sessions[0]->logs;
    foreach ($logs as $log) {
      if (strpos($log->eventname, "course_module_viewed")) {
        $moduleid = $this->get_module_id($log->objectid, $log->objecttable);
        switch (date('N', $log->timecreated)) {
          case 1:
            array_push($days_worked["monday"], $moduleid->id);
            break;
          case 2:
            array_push($days_worked["tuesday"], $moduleid->id);
            break;
          case 3:
            array_push($days_worked["wednesday"], $moduleid->id);
            break;
          case 4:
            array_push($days_worked["thursday"], $moduleid->id);
            break;
          case 5:
            array_push($days_worked["friday"], $moduleid->id);
            break;
          case 6:
            array_push($days_worked["saturday"], $moduleid->id);
            break;
          case 7:
            array_push($days_worked["sunday"], $moduleid->id);
            break;
        }
      }
    }
    return $days_worked;
  }
  
  /**
   * Get the modules planned for each day of the specified week for the current user 
   * 
   * @param string $weekcode code of the week specified
   *
   * @return object the modules planned for each day of the specified week
   */
  protected function get_modules_days_planned($weekcode)
  {
    $days_planned = array(
      "monday" => array(),
      "tuesday" => array(),
      "wednesday" => array(),
      "thursday" => array(),
      "friday" => array(),
      "saturday" => array(),
      "sunday" => array(),
    );
    $mods_days =  self::get_modules_days_by_week($weekcode);
    foreach ($mods_days as $key => $mod_day) {
      array_push($days_planned[$mod_day->daydesc], $mod_day->cmid);
    }
    return $days_planned;
  }


    /**
   * Get the notemyprogress_days_modules ids, the course module ids and the days description for a specified week.
   * 
   * @param string $weekcode code of the week specified
   *
   * @return object the notemyprogress_days_modules ids, the course module ids and the days description
   */
  protected function get_modules_days_by_week($weekcode)
  {
    global $DB;
    $sql = "select dm.id, dm.id_course_modules cmid, dm.day_description daydesc from {notemyprogress_days_modules} dm, {notemyprogress_wk} ws where dm.id_weekly_schedules = ws.id and ws.weekcode = ? and ws.userid = ? ";
    $mods_weeks = $DB->get_records_sql($sql, array($weekcode,$this->user->id));
    return $mods_weeks;
  }

   /**
   * Give the index of the day string specified
   * 
   * @param object $day day specified
   *
   * @return object the index of the day string specified
   */
  protected function get_num_day($day){
    if ($day == "monday" ){
      return 1;
    }
    elseif($day == "tuesday" ){
      return 2;
    }
    elseif($day == "wednesday" ){
      return 3;
    }
    elseif($day == "thursday" ){
      return 4;
    }
    elseif($day == "friday" ){
      return 5;
    }
    elseif($day == "saturday" ){
      return 6;
    }
    elseif($day == "sunday" ){
      return 7;
    }
  }

   /**
   * Get an array containing the status (planned,pending,failed,completed) of each day of the specified week
   * 
   * @param object $planifications informations about the days planned and the days worked
   * @param object $week week specified
   *
   * @return array array containing the status of each day of the specified week
   */
  protected function get_respect_planning($planifications,$week)
  {
    $days_module_planned = $planifications->days_planned;
    $days_module_worked = $planifications->days_worked;
    $status = [];
    $weekstart = $week->weekstart;
    $current_time = time();
    foreach ($days_module_planned as $key => $day_planned) {
      $day_num =  self::get_num_day($key);
      $current_day = strtotime("+ $day_num day ",$weekstart);
      if ($day_planned) {
        if (!array_diff($day_planned, $days_module_worked[$key])) {
          array_push($status, 'completed');
        } else {
          if ($current_day < $current_time) {
            array_push($status, 'failed');
          } else {
            array_push($status, 'pending');
          }
        }
      } else {
        array_push($status, 'unplanned');
      }
    }
    return $status;
  }

   /**
   * Get the goals selected for a specific week
   * 
   * @param string $weekcode code of the week specified
   *
   * @return array the goals selected for a specific week
   */
  public function goals_report_metereflexion($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    $schedule = self::get_schedule($week->weekcode);
    if ($schedule != null) {
      $query_goals = $this->get_goals_id($schedule);
    } else {
      $query_goals = [];
    }
    return $query_goals;
  }

     /**
   * Get the goals selected for a specific week
   * 
   * @param string $weekcode code of the week specified
   *
   * @return array the goals selected for a specific week
   */
  public function get_goals_id($week_schedule)
  {
    global $DB;
    $sql =  "select g.id_goals_categories from {notemyprogress_goals} g,{notemyprogress_wk} ws where g.id_weekly_schedules = ws.id and g.id_weekly_schedules = ? and ws.userid = ? ";
    $goals_res = array_values($DB->get_records_sql($sql, array($week_schedule->id,$this->user->id)));
    $arr_goal = [];
    foreach($goals_res as $key=>$id_obj){
      array_push($arr_goal,$id_obj->id_goals_categories);
    }
    return $arr_goal;
  }

     /**
   * Get informations about the hours planned and worked by the user for a specific week
   * 
   * @param string $weekcode code of the week specified
   *
   * @return object informations about the hours planned and worked
   */
  public function hours_report_metereflexion($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }

    $work_sessions = self::get_work_sessions($week->weekstart, $week->weekend);
    $user_with_planning = 0;
    $sum_hours_work = 0;

    if (self::render_has() == 'student') {
      $label_horas = self::convert_time('minutes', round($work_sessions[0]->summary->added, 0));
      $sum_hours_work = round($work_sessions[0]->summary->added, 2) / 60;
    } else {
      foreach ($work_sessions as $key => $work_session) {
        if ($work_session->summary->added > 0) {
          $user_with_planning++;
          $sum_hours_work += $work_session->summary->added;
        }
      }
      $label_horas = self::convert_time('minutes', $sum_hours_work / count($work_sessions));
      $sum_hours_work = round($sum_hours_work / count($work_sessions), 2) / 60;
    }

    $sum_hours_per_week = self::planned_week($weekcode);

    $data_report_meta_hours = new stdClass();
    $data_report_meta_hours->title = 'Time invested';
    $data_report_meta_hours->hours_worked = $sum_hours_work;
    $data_report_meta_hours->label_hours_worked = $label_horas;
    $data_report_meta_hours->hours_planned = $sum_hours_per_week != null ? $sum_hours_per_week : 0 ;
    $data_report_meta_hours->label_hours_planned = self::convert_time('hours', $sum_hours_per_week);
    $data_report_meta_hours->total_users = count($work_sessions);
    $data_report_meta_hours->interacted_users = $user_with_planning;
    return $data_report_meta_hours;
  }

   /**
   * Get the hours planned for a specific week
   * 
   * @param string $weekcode code of the week specified
   *
   * @return string hours planned for a specific week
   */
  protected function planned_week($weekcode = null)
  {
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    global $DB;
    if (empty($this->users)) {
      return 0;
    }
    list($in, $invalues) = $DB->get_in_or_equal($this->users);
    $sql =  "select * from {notemyprogress_wk} where weekcode = ? and userid $in";
    $params = array_merge(array($week->weekcode), $invalues);
    $week_schedules = $DB->get_records_sql($sql, $params);
    $result = self::calculate_average("hours", $week_schedules);
    $result = $result->average;
    return $result;
  }
  
  /**
   * Get the hours planned for a specific week
   * 
   * @param string $weekcode code of the week specified
   *
   * @return string hours planned for a specific week
   */
  public function classroom_report_metareflexion($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }

    $average_classroom = self::average_classroom($weekcode);
    $average_of_classroom = self::average_of_classroom($weekcode);

    $data_last_week = new stdClass();
    $data_last_week->average_classroom = $average_classroom;
    $data_last_week->average_of_classroom = $average_of_classroom;

    return $data_last_week;
  }

  protected function average_classroom($weekcode = null)
  {
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    global $DB;
    if (empty($this->users)) {
      return 0;
    }

    list($in, $invalues) = $DB->get_in_or_equal($this->users);
    $sql = "select * from {notemyprogress_last_weeks} where weekcode = ? and userid $in";
    $params = array_merge(array($week->weekcode), $invalues);
    $presence_clases = $DB->get_records_sql($sql, $params);

    $sum_classroom_hours = 0;
    foreach ($presence_clases as $key => $presence_clase) {
      $sum_classroom_hours += $presence_clase->classroom_hours;
    }

    $average_horas_presence_clases = 0;

    if (!empty($presence_clases)) {
      $average_horas_presence_clases = round($sum_classroom_hours / count($presence_clases), 1);
    }

    return $average_horas_presence_clases;
  }

  protected function average_of_classroom($weekcode = null)
  {
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    global $DB;
    if (empty($this->users)) {
      return 0;
    }

    list($in, $invalues) = $DB->get_in_or_equal($this->users);
    $sql = "select * from {notemyprogress_last_weeks} where weekcode = ? and userid $in";
    $params = array_merge(array($week->weekcode), $invalues);
    $presence_clases = $DB->get_records_sql($sql, $params);

    $sum_hours_off_course = 0;
    foreach ($presence_clases as $key => $presence_clase) {
      $sum_hours_off_course += $presence_clase->hours_off_course;
    }

    $average_horas_presence_clases = 0;

    if (!empty($presence_clases)) {
      $average_horas_presence_clases = round($sum_hours_off_course / count($presence_clases), 1);
    }

    return $average_horas_presence_clases;
  }

    /**
   * Get reflection questions of the week specified
   * 
   * @param string $weekcode code of the week specified
   *
   * @return object reflection questions of the week specified
   */
  public function report_last_week($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    $work = self::get_work_last_week($week);

    return $work;
  }


  /**
   * Get questions from the notemyprogress_questions table for a specific week
   * 
   * @param object $week code of the week specified
   *
   * @return object questions from the notemyprogress_questions table
   */
  private function get_work_last_week($week)
  {
    global $DB;
    $sql = "select * from {notemyprogress_questions}";
    $questions = $DB->get_records_sql($sql);
    $questions = self::add_question_alternatives($questions, $week);
    return $questions;
  }
  
  /**
   * Add possible answers to the questions structure for a specific week
   * 
   * @param object $questions question structure
   * @param object $week specified week
   *
   * @return object questions with possible answers
   */
  private function add_question_alternatives($questions, $week)
  {
    global $DB;
    if (empty($this->users)) {
      return null;
    }
    list($in, $invalues) = $DB->get_in_or_equal($this->users);
    $questions = array_values($questions);
    foreach ($questions as $question) {
      $question->enunciated = get_string($question->enunciated, "local_notemyprogress");
      $params = array();
      array_push($params, $week->weekcode);
      $params = array_merge($params, $invalues);
      array_push($params, $question->id);
      $sql = "select a.*, (select count(*) from {notemyprogress_last_weeks} where weekcode = ? and userid $in and (objectives_reached = a.id or previous_class_learning = a.id or benefit_going_class = a.id or feeling = a.id)) as count_selections 
              from {notemyprogress_answers} a where a.questionid = ?";
      $answers = $DB->get_records_sql($sql, $params);
      foreach ($answers as $key => $answer) {
        $answer->enunciated = get_string($answer->enunciated, "local_notemyprogress");
      }
      $question->alternatives = array_values($answers);
    }
    return $questions;
  }


  //////// Old functions : was used to make a planning based on the interactions on each day of the week (not the completion of planned modules) //////////

    /**
   * Get statistics about the progression of the students for a specific week
   *
   * @param string $weekcode code of the week
   *
   * @return array an array with statistics about the progression of the students
   */ 
  public function students_planification_summary($weekcode = null)
  {
    if (!self::course_in_transit()) {
      return null;
    }
    if (!self::course_has_users()) {
      return null;
    }
    $week = $this->current_week;
    if (!empty($weekcode)) {
      $week = self::find_week($weekcode);
    }
    $schedule = self::get_schedule($week->weekcode);
    $worked_sessions = self::get_work_sessions($week->weekstart, $week->weekend);
    $summary = self::verify_students_planifications($worked_sessions, $schedule);
    $summary = self::summary_process($summary);
    return $summary;
  }
  
  /**
   * Get the percentage of courses comleted among the courses planned by day
   *
   * @param object $days statistics on module completion
   *
   * @return object an object with statistics about the progression of the courses completed
   */ 
  private function summary_process($days)
  {
    foreach ($days as $day) {
      $day->percentage = 0;
      $day->count_students = count($day->students);
      if ($day->count_planned > 0) {
        $day->percentage = round(($day->count_accomplished * 100 / $day->count_planned), 1);
      }
    }
    return $days;
  }
  
  /**
   * Get the percentage of courses comleted among the courses planned by day
   *
   * @param object $worked_sessions sessions of the week
   * @param object $planifications schedule informations
   *
   * @return object an object with statistics about the progression of courses completed
   */ 
  private function verify_students_planifications($worked_sessions, $planifications)
  {
    $worked_sessions = self::update_array_key_by('userid', $worked_sessions);
    $summary = self::create_planifications_week_summary();
    foreach ($planifications as $planification) {
      $days = explode(',', $planification->days);
      foreach ($days as $day) {
        if (!in_array($planification->userid, $summary[$day]->students)) {
          $summary[$day]->count_planned++;
          if (self::planned_accomplished($day, $planification->userid, $worked_sessions)) {
            $summary[$day]->count_accomplished++;
          }
          array_push($summary[$day]->students, $planification->userid);
        }
      }
    }
    return $summary;
  }


  /**
   * Returns if the day is completed or not
   *
   * @param object $day day specified
   * @param string $userid id of the user
   * @param object $course_sessions sessions of the course
   *
   * @return boolean if the day is completed or not
   */
  private function planned_accomplished($day, $userid, $course_sessions)
  {
    $tz = self::get_timezone();
    date_default_timezone_set($tz);
    $accomplished = false;
    if (isset($course_sessions[$userid])) {
      $user_sessions = $course_sessions[$userid];
      foreach ($user_sessions->sessions as $session) {
        if (self::day_to_english($day) == strtolower(date('D', $session->start))) {
          $accomplished = true;
          break;
        }
      }
    }
    return $accomplished;
  }
}
