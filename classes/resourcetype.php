<?php

namespace local_notemyprogress;

require_once("lib_trait.php");

use stdClass;


class resourcetype
{

  const DEFAULT_TYPE = "other";

  public static function get_default_type()
  {
    return self::DEFAULT_TYPE;
  }

  public static function get_type($mimetype)
  {
    $groups = array(
      "resource_document" => self::get_document_mimetypes(),
      "resource_image" => self::get_image_mimetypes(),
      "resource_audio" => self::get_audio_mimetypes(),
      "resource_video" => self::get_video_mimetypes(),
      "resource_file" => self::get_file_mimetypes(),
      "resource_script" => self::get_script_mimetypes(),
      "resource_text" => self::get_text_mimetypes(),
      "resource_download" => self::get_download_mimetypes()
    );
    $type = self::DEFAULT_TYPE;
    foreach ($groups as $group_name => $group) {
      if (in_array($mimetype, $group)) {
        $type = $group_name;
        break;
      }
    }
    return $type;
  }

  public static function get_document_mimetypes()
  {
    $documents = [
      "application/pdf", "application/x-abiword", "application/octet-stream", "application/msword",
      "application/vnd.oasis.opendocument.presentation", "application/vnd.oasis.opendocument.spreadsheet",
      "application/vnd.oasis.opendocument.text", "application/vnd.ms-powerpoint", "application/vnd.visio",
      "application/vnd.ms-excel"
    ];
    return $documents;
  }

  public static function get_image_mimetypes()
  {
    $images = [
      "image/jpeg", "image/png", "image/gif", "image/x-icon", "image/svg+xml", "image/webp", "audio/3gpp",
      "audio/3gpp2"
    ];
    return $images;
  }

  public static function get_audio_mimetypes()
  {
    $audio = ["audio/aac", "audio/midi", "audio/ogg", "audio/x-wav", "audio/webm"];
    return $audio;
  }
  public static function get_video_mimetypes()
  {
    $video = ["video/mp4", "video/x-msvideo", "video/mpeg", "video/ogg", "video/webm", "video/3gpp", "video/3gpp2"];
    return $video;
  }

  public static function get_file_mimetypes()
  {
    $files = [
      "application/x-rar-compressed", "application/zip", "application/x-bzip", "application/x-bzip2",
      "application/x-7z-compressed"
    ];
    return $files;
  }

  public static function get_script_mimetypes()
  {
    $scripts = [
      "application/octet-stream", "application/x-csh", "text/css", "application/java-archive",
      "application/javascript", "application/json", "application/x-sh", "application/xml"
    ];
    return $scripts;
  }

  public static function get_text_mimetypes()
  {
    $text = ["text/csv", "text/plain", "text/html", "text/calendar", "application/rtf", "application/xhtml+xml"];
    return $text;
  }
  public static function get_download_mimetypes()
  {
    $downloads = ["file_download"];
    return $downloads;
  }
}
