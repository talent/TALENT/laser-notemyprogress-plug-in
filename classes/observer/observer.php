<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * notemyprogress Observer
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_notemyprogress\observer;
defined('MOODLE_INTERNAL') || die();

/**
 * notemyprogress observer class.
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class observer
{

    /**
     * When a course is deleted.
     *
     * @param \core\event\course_deleted $event The event.
     * @return void
     */
    public static function course_deleted(\core\event\course_deleted $event)
    {
        global $DB;

        $courseid = $event->objectid;

        // Clean up the data that could be left behind.
        $conditions = array('courseid' => $courseid);
//        $DB->delete_records('block_xp', $conditions);
//        $DB->delete_records('block_xp_config', $conditions);
//        $DB->delete_records('block_xp_filters', $conditions);
//        $DB->delete_records('block_xp_log', $conditions);

        // Flags. Note that this is based on the actually implementation.
//        $sql = $DB->sql_like('name', ':name');
//        $DB->delete_records_select('user_preferences', $sql, [
//            'name' => 'block_xp-notice-block_intro_' . $courseid
//        ]);
//        $DB->delete_records_select('user_preferences', $sql, [
//            'name' => 'block_xp_notify_level_up_' . $courseid
//        ]);

        // Delete the files.
//        $fs = get_file_storage();
//        $fs->delete_area_files($event->contextid, 'block_xp', 'badges');
    }

    /**
     * Observe all events.
     *
     * @param \core\event\base $event The event.
     * @return void
     */
    public static function catch_all(\core\event\base $event) {

        global $COURSE, $USER;
        $userid = $event->userid;
        
        if ($event->component === 'local_notemyprogress') {
          // omit your own events.
            return;
        } else if (!$userid || isguestuser($userid) || is_siteadmin($userid)) {
            // Omitting users and invited that in Hayan started Sesión.
            return;
        }
        else if ($event->anonymous) {
            // Omit all events marked as anonymous.
            return;
        }
        else if (!in_array($event->contextlevel, array(CONTEXT_COURSE, CONTEXT_MODULE))) {
            // Omit events that are not in the right context.
            return;
        } else if ($event->edulevel !== \core\event\base::LEVEL_PARTICIPATING) {
            // Ignore events that are not participating.
            return;
        } else if (!$event->get_context()) {
            // If for some reason the context does not exist ...
            return;
        }

        try {
            // It has been reported that this can generate an exception when the context is missing ...
            $canearn = has_capability('local/notemyprogress:earnxp', $event->get_context(), $userid);
        } catch (moodle_exception $e) {
            return;
        }

        // Omit events if the user does not have the ability to earn points.
        if (!$canearn) {
            return;
        }

        $cs = new \local_notemyprogress\event_strategy($COURSE, $USER);
        $cs->collect_event($event);
    }

}