<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event Collector Observer
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_notemyprogress;

defined('MOODLE_INTERNAL') || die();

require_once("lib_trait.php");

use stdClass;
use DateTime;
use dml_exception;
use lang_string;

class event_strategy {

    use \lib_trait;

    protected $table_xp = 'notemyprogress_xp';
    protected $table_log = 'notemyprogress_xp_log';
    public $course;
    public $user;
    public $configgamification;

    public function __construct($course, $userid) {
        $this->course = self::get_course($course);
        $this->user = self::get_user($userid);
        $this->configgamification = new \local_notemyprogress\configgamification($course, $userid);
    }

    /**
     * Manejador de eventos.
     *
     * @param \core\event\base $event The event.
     * @return void
     */
    public function collect_event(\core\event\base $event) {

        if (!self::can_capture_event($event)) {
            return;
        }

        $points = self::get_points_for_event($event);
        if ($points === null) {
            return;
        }

        if ($points > 0) {
            self::increase($this->user->id, $this->course->id, $points);
            self::save_log($this->user->id, $this->course->id, $points, $event->eventname);
        }
    }

    /**
     * Compruebe si el usuario puede capturar este evento.
     *
     * @param \core\event\base $event El evento.
     * @return bool True cuando el evento es permitido.
     */
    protected function can_capture_event(\core\event\base $event) {
        global $SESSION;

        $now = time();
        $maxcount = 64;

        /*
         * El número máximo de acciones que contarán como puntos durante el período de tiempo dado.
         * Cualquier acción posterior será ignorada. Cuando este valor está vacío o es igual a cero, no se aplica.*/
        $maxactions = 10;

        /* El período de tiempo (segundos) durante el cual el usuario no debe exceder un número máximo de acciones.*/
        $maxtime = 60;

        /*
         * Se acepta nuevamente el tiempo mínimo requerido antes de que una acción que ya sucedió anteriormente.
         * Una acción se considera idéntica si se colocó en el mismo contexto y objeto.
         * Cuando este valor está vacío o es igual a cero, no se aplica.*/
        $actiontime = 180;

        $actionkey = $event->eventname . ':' . $event->contextid . ':' . $event->objectid . ':' . $event->relateduserid;

        if (!isset($SESSION->local_notemyprogress_cheatguard)) {
            // Init the session variable.
            $SESSION->local_notemyprogress_cheatguard = [];
        } else {
            // Ensure that all entries are arrays, they may not be when we just upgraded the plugin.
            $SESSION->local_notemyprogress_cheatguard = array_map(function($entry) {
                return is_array($entry) ? $entry : [$entry];
            }, $SESSION->local_notemyprogress_cheatguard);
        }

        // Perform the check.
        if (!static::is_action_accepted($actionkey, $now, $SESSION->local_notemyprogress_cheatguard, $maxactions, $maxtime, $actiontime)) {
            return false;
        }

        // Move the action at the end of the array.
        $times = isset($SESSION->local_notemyprogress_cheatguard[$actionkey]) ? $SESSION->local_notemyprogress_cheatguard[$actionkey] : [];
        unset($SESSION->local_notemyprogress_cheatguard[$actionkey]);
        $SESSION->local_notemyprogress_cheatguard[$actionkey] = $times;

        // Log the time at which this event happened.
        $SESSION->local_notemyprogress_cheatguard[$actionkey][] = time();

        // Limit the timestamps of each action to a maximum of $maxcount within the timeframe desired or
        // the last 15min. We want to keep at least 15 min so that when teachers are testing changes,
        // they do not get confused because actions they had just performed no longer gets blocked.
        $timethreshold = $now - max([$maxtime, $actiontime, 900]);
        $SESSION->local_notemyprogress_cheatguard = array_filter(array_map(function($times) use ($maxcount, $timethreshold) {
            return array_slice(array_filter($times, function($time) use ($timethreshold) {
                return $time > $timethreshold;
            }), -$maxcount);
        }, $SESSION->local_notemyprogress_cheatguard));

        // Limit the array of events to $maxcount, we do not want to flood the session for no reason.
        $SESSION->local_notemyprogress_cheatguard = array_slice($SESSION->local_notemyprogress_cheatguard, -$maxcount, null, true);

        return true;
    }

    /**
     * Comprueba si la acción es aceptada.
     *
     * @param string $action Clave de la acción.
     * @param int $now Timestamp.
     * @param array $log Array where keys are actions, and values are timestamp arrays.
     * @param int $maxactions El máximo número de acciones.
     * @param int $maxintime Tiempo durante el cual el máximo número de acciones es permitido.
     * @param int $timebetweenrepeats Tiempo entre acciones repetidas.
     * @return bool
     */
    public static function is_action_accepted($action, $now, array $log, $maxactions, $maxintime, $timebetweenrepeats) {

        if ($maxactions > 0 && $maxintime > 0) {
            $timethreshold = $now - $maxintime;
            $actionsintimeframe = array_reduce($log, function($carry, $times) use ($timethreshold) {
                return $carry + array_reduce($times, function($carry, $time) use ($timethreshold) {
                        return $carry + ($time > $timethreshold ? 1 : 0);
                    });
            }, 0);
            if ($actionsintimeframe >= $maxactions) {
                return false;
            }
        }

        if ($timebetweenrepeats > 0) {
            $timethreshold = $now - $timebetweenrepeats;
            $times = isset($log[$action]) ? $log[$action] : [];
            if (!empty($times) && max($times) > $timethreshold) {
                return false;
            }
        }

        return true;
    }

    /**
     * Obtiene los puntos para un evento.
     *
     * @param \core\event\base $event event.
     * @return int Points, or null.
     */
    public function get_points_for_event(\core\event\base $event) {
        $ruleset = $this->configgamification->get_levels_data()->rules;
        foreach ($ruleset as $rule) {
            if ($rule->rule == $event->eventname) {
                return $rule->points;
            }
        }
        return null;
    }

    /**
     * Agregar puntos de experiencia.
     *
     * @param int $id The receiver.
     * @param int $amount The amount.
     */
    public function increase($userid, $courseid, $amount) {
        global $DB;
        $prexp = 0;
        $postxp = $amount;

        if ($record = self::get_user_data($userid, $courseid)) {
            $prexp = (int)$record->points;
            $postxp = $prexp + $amount;

            $sql = "UPDATE {{$this->table_xp}}
                       SET points = points + :points
                     WHERE courseid = :courseid
                       AND userid = :userid";
            $params = [
                'points' => $amount,
                'courseid' => $courseid,
                'userid' => $userid
            ];
            $DB->execute($sql, $params);

            $newxp = (int)$record->points + $amount;
            $newlevel = self::get_level_from_xp($newxp)->lvl;
            if ($record->level != $newlevel) {
                $DB->set_field($this->table_xp, 'level', $newlevel, ['courseid' => $courseid, 'userid' => $userid]);
            }
        } else {
            self::insert($userid, $courseid, $amount);
        }
    }

    /**
     * Log a thing.
     *
     * @param int $userid The target.
     * @param int $courseid The target.
     * @param int $points The points.
     * @param reason $reason The reason.
     * @param DateTime|null $time When that happened.
     * @return void
     */
    public function save_log($userid, $courseid, $points, $eventName) {
        global $DB;
        $time = new DateTime();
        $record = new stdClass();
        $record->courseid = $courseid;
        $record->userid = $userid;
        $record->event = $eventName;
        $record->points = $points;
        $record->timecreated = $time->getTimestamp();
        try {
            $DB->insert_record($this->table_log, $record);
        } catch (dml_exception $e) {}

    }

    /**
     * Agregar puntos de experiencia a un usuario en la tabla notemyprogress_xp.
     *
     * @param int $id The receiver.
     * @param int $amount The amount.
     */
    public function insert($userid, $courseid, $amount) {
        global $DB;
        $record = new stdClass();
        $record->courseid = $courseid;
        $record->userid = $userid;
        $record->points = $amount;
        $record->level = self::get_level_from_xp($amount)->lvl;
        try {
            $DB->insert_record($this->table_xp, $record); //*
        } catch (dml_exception $e) {}
    }

    /**
     * Give the level from the xp points.
     *
     * @param int $xp The xp points.
     * @return level
     */
    public function get_level_from_xp($points) {
        $levels = $this->configgamification->get_levels_data()->levelsdata;
        for ($i = count($levels)-1; $i >= 0; $i--) {
            $level = $levels[$i];
            if ($level->points <= $points) {
                return $level;
            }
        }
        return $level;
    }

    /**
     * Obtiene el próximo nivel de un usuario
     *
     * @return null|level
     */
    public function get_next_level($userid) {
        $configgamification = $this->configgamification->get_levels_data();
        $levels = $configgamification->levelsdata;
        $userData = self::get_user_data($userid, $this->course->id);
        $levelnum = $userData->level;

        if ($levelnum > count($levels)-1) {
            $nextlevel = false;
        } else {
            $nextlevel = $levels[$levelnum];
        }

        // TODO: validar que pasa cuando supera el ultimo nivel
        if ($nextlevel === false) {
            return null;
        }

        return $nextlevel;
    }

    /**
     * Obtiene la información de puntos de experiencia de un usuario
     *
     * @return null|response
     */
    public function get_user_info() {

        $response = new stdClass();
        $recentCount = 3;
        if (self::get_user_data($this->user->id, $this->course->id)) {

            $userData = self::get_user_data($this->user->id, $this->course->id);
            $nextlevel = self::get_next_level($this->user->id);
            $activity = self::get_user_recent_activity($this->user->id, $this->course->id, $recentCount);
            $levelInfo = self::get_level_from_xp($userData->points);

            $response->points = $userData->points;
            $response->level = $userData->level;
            $response->activity = $activity;
            $response->levelInfo = $levelInfo;
            $response->uid=$this->user->id;
            $response->cid=$this->course->id;

            // $response->points = 1;
            // $response->level = 2;
            // $response->activity = 3;
            // $response->levelInfo = $userData->points;

            if(is_null($nextlevel)) {
                // Si no existe un siguiente nivel
                $response->pointsToNext = 0;
                $response->progress = 100;
            }else{
                $response->pointsToNext = $nextlevel->points - $userData->points;
                $response->progress = ($userData->points*100)/$nextlevel->points;
            }
        }else{return self::get_user_data($this->user->id, $this->course->id);}
        return $response;
    }

    /**
     * Obtiene un ranking con los puntos de experiencia de los usuarios
     *
     * @return users
     */
    public function get_ranking($int) {
        global $DB;

        $users = array();
        if ($int==1){
            $sql = "SELECT * FROM {{$this->table_xp}} WHERE courseid = {$this->course->id} and rankable = 1 ORDER BY points DESC";
        }else{
            $sql = "SELECT * FROM {{$this->table_xp}} WHERE courseid = {$this->course->id} ORDER BY points DESC";
        }
        
        $rank = $DB->get_recordset_sql($sql);
        $index = 1;

        foreach($rank as $user){

            $completeUser = self::get_user_from_id($user->userid);
            $nextlevel = self::get_next_level($user->userid);

            $aux = new stdClass();
            $aux->ranking = $index;
            $aux->level = $user->level;
            $aux->student = $completeUser->firstname.' '.$completeUser->lastname;
            $aux->total = $user->points;

            if(is_null($nextlevel)) {
                // Si no existe un siguiente nivel
                $aux->progress_percentage = 100;
            }else{
                $aux->progress_percentage = ($user->points*100)/$nextlevel->points;
            }

            $users[] = $aux;

            $index++;
        }
        $rank->close();
        return $users;
    }

    /**
     * Verifica si el usuario existe en la tabla de puntos de experiencia.
     *
     * @param int $id The receiver.
     * @return stdClass|false
     */
    public function get_user_data($userid, $courseid) {
        global $DB;
        $params = [];
        $params['userid'] = $userid;
        $params['courseid'] = $courseid;
        return $DB->get_record($this->table_xp, array("userid"=>$userid,"courseid"=>$courseid));
    }

    /**
     * Obtener la actividad reciente del usuario de la tabla notemyprogress_xp_log.
     *
     * @param int $userid El identificador del usuario.
     * @param int $courseid El identificador del curso.
     * @param int $count El número de registros a recuperar.
     * @return activity
     */
    public function get_user_recent_activity($userid, $courseid, $count = 0) {
        global $DB;
        $results = $DB->get_records_select($this->table_log, 'courseid = :courseid AND userid = :userid AND points > 0', [
            'courseid' => $courseid,
            'userid' => $userid,
        ], 'timecreated DESC, id DESC', '*', 0, $count);

        $activities = array();
        foreach($results as $row){
            $desc = '';
            $class = $row->event;
            if (class_exists($class) && is_subclass_of($class, 'core\event\base')) {
                $desc = $class::get_name();
            } else {
                $desc = new lang_string('somethinghappened', 'local_notemyprogress');
            }
            $activity = new stdClass();
            $activity->timeago = self::time_ago(new DateTime('@' . $row->timecreated));
            $activity->description = $desc;
            $activity->points = $row->points;
            $activities[] = $activity;
        }

//        return array_map(function($row) {
//            $desc = '';
//            $class = $row->event;
//            if (class_exists($class) && is_subclass_of($class, 'core\event\base')) {
//                $desc = $class::get_name();
//            } else {
//                $desc = new lang_string('somethinghappened', 'local_notemyprogress');
//            }
//            $activity = new stdClass();
//            $activity->timeago = self::time_ago(new DateTime('@' . $row->timecreated));
//            $activity->description = $desc;
//            $activity->points = $row->points;
//            return $activity;
//        }, $results);

        return $activities;
    }

    /**
     * Obtener un timestamp de la forma hace 5 min.
     *
     * @param DateTime $dt Objeto de tipo DateTime.
     * @return string
     */
    public function time_ago(DateTime $dt) {
        $now = new \DateTime();
        $diff = $now->getTimestamp() - $dt->getTimestamp();
        $ago = '?';

        if ($diff < 15) {
            $ago = get_string('tg_timenow', 'local_notemyprogress');
        } else if ($diff < 45) {
            $ago = get_string('tg_timeseconds', 'local_notemyprogress', $diff);
        } else if ($diff < HOURSECS * 0.7) {
            $ago = get_string('tg_timeminutes', 'local_notemyprogress', round($diff / 60));
        } else if ($diff < DAYSECS * 0.7) {
            $ago = get_string('tg_timehours', 'local_notemyprogress', round($diff / HOURSECS));
        } else if ($diff < DAYSECS * 7 * 0.7) {
            $ago = get_string('tg_timedays', 'local_notemyprogress', round($diff / DAYSECS));
        } else if ($diff < DAYSECS * 30 * 0.7) {
            $ago = get_string('tg_timeweeks', 'local_notemyprogress', round($diff / (DAYSECS * 7)));
        } else if ($diff < DAYSECS * 365) {
            $ago = userdate($dt->getTimestamp(), get_string('tg_timewithinayearformat', 'local_notemyprogress'));
        } else {
            $ago = userdate($dt->getTimestamp(), get_string('tg_timeolderyearformat', 'local_notemyprogress'));
        }

        return $ago;
    }
}