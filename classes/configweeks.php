<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Config Weeks
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_notemyprogress;

require_once("lib_trait.php");

use stdClass;

class configweeks
{
    use \lib_trait;

    public $course;
    public $user;
    public $weeks;
    public $instance;
    public $current_sections;
    public $startin;

    function __construct($course, $userid)
    {
        global $DB;
        $this->course = self::get_course($course);
        $this->user = self::get_user($userid);
        $this->instance = self::last_instance();
        $this->weeks = self::get_weeks();
        $this->current_sections = self::get_course_sections();
        $this->startin = isset($this->weeks[0]) ? $this->weeks[0]->weekstart : 999999999999;
        $this->weeks = self::get_weeks_with_sections();
    }

    /**
     * Gets the last instance configured for NoteMyProgress weeks. If no weeks have been 
     * configured in a course yet, the last instance is the one created by default by the plugin.
     *
     * @return mixed a fieldset object that contains the first record that matches the query.
     */
    public function last_instance()
    {
        global $DB;
        $sql = "select * from {notemyprogress_instances} where courseid = ? order by id desc LIMIT 1";
        $instance = $DB->get_record_sql($sql, array($this->course->id));
        if (!isset($instance) || empty($instance)) {
            $instance = self::create_instance($this->course->id);
        }
        return $instance;
    }

    /**
     * Create a new instance for the setting of weeks of Note My Progress.This is the instance
     * That is created by default.
     *
     * @return mixed A FieldSet object that contains the created record
     */
    public function create_instance()
    {
        global $DB;
        $instance = new stdClass();
        $instance->courseid = $this->course->id;
        $instance->year = date("Y");
        $id = $DB->insert_record("notemyprogress_instances", $instance, true);
        $instance->id = $id;
        $this->instance = $instance;
        return $instance;
    }

    /**
     * Obtain the weeks configured in Note My progress based on the IDs of class attributes
     * $ Course and $ INTANCE.It can be specified
     *
     * @return Array a list with the weeks configured in a course
     */
    public function get_weeks()
    {
        global $DB;
        $sql = "SELECT * FROM {notemyprogress_weeks} 
                WHERE courseid = ? AND instanceid = ? AND timedeleted IS NULL ORDER BY POSITION ASC";
        $weeks = $DB->get_records_sql($sql, array($this->course->id, $this->instance->id));
        $weeks = array_values($weeks);
        return $weeks;
    }

    /**
     * Add the Sections field to the class variable weeks to store the sections assigned to each week.
     *
     * @return Array a list with the weeks of the course and sections assigned to each
     * @throws Coding_Exception |DML_EXCEPTION.
     */
    public function get_weeks_with_sections()
    {
        $weeks = $this->weeks;
        if (count($weeks) == 0) {
            $weeks[] = self::create_first_week();
            $this->weeks = $weeks;
        }
        $course_sections = self::get_course_sections();
        foreach ($weeks as $position => $week) {
            $week->removable = true;
            if ($position == 0) {
                $week->removable = false;
            }
            $week->sections = array();
            $week->name = get_string('setweeks_week', 'local_notemyprogress');
            if (!isset($week->date_format)) {
                $week->date_format = "Y-m-d";
                $week->weekstartlabel = self::to_format("Y-m-d", $week->weekstart);
                $week->weekendlabel = self::to_format("Y-m-d", $week->weekend);
            }
            $week->weekstart = intval($week->weekstart);
            $week->weekend = intval($week->weekend);
            $week->position = $position;
            $week->delete_confirm = false;
            $sections = self::get_week_sections($week->weekcode);
            foreach ($sections as $key => $section) {
                $section->name = $section->section_name;
                $section->visible = self::get_current_visibility($section->sectionid);
                $section = self::validate_section($section, $course_sections);
                $week->sections[] = $section;
            }
        }
        return $weeks;
    }

    /**
     * Create the first week (default week) of Note My progress in a course.This function is executed
     * automatically for each course.
     *
     * @return stdClass An object with the week created
     * @throws dml_exception
     */
    private function create_first_week()
    {
        global $DB;
        $start = strtotime('next monday');
        $end = strtotime('next monday + 6 day') + 86399;
        $week = new stdClass();
        $week->hours_dedications = 0;
        $week->courseid = $this->course->id;
        $week->weekstart = $start;
        $week->weekend = $end;
        $week->position = 0;
        $week->modified_by = $this->user->id;
        $week->created_by = $this->user->id;
        $week->timecreated = self::now_timestamp();
        $week->timemodified = self::now_timestamp();
        $week->weekcode = self::generate_week_code(0);
        $week->instanceid = $this->instance->id;
        $id = $DB->insert_record("notemyprogress_weeks", $week, true);
        $week->id = $id;
        return $week;
    }

    /**
     * Generates an identification code for one week of NoteMyProgress. The code is formed
     * based on the current year, id of the NoteMyProgress instance in the course and position in the week.
     *
     * @return int integer representing the week identifier.
     */
    private function generate_week_code($weekposition)
    {
        $code = $this->instance->year . $this->instance->id . $this->course->id . $weekposition;
        $code = (int) $code;
        return $code;
    }

    /**
     * Gets the sections of a week identified by the parameter $weekcode
     *
     * @param string $weekcode identifier of the week from which the weeks are to be obtained
     *
     * @return array list with the sections assigned to the week
     */
    public function get_week_sections($weekcode)
    {
        global $DB;
        $sql = "select * from {notemyprogress_sections} where weekcode = ? and timedeleted IS NULL order by position asc";
        $week_sections = $DB->get_records_sql($sql, array($weekcode));
        return $week_sections;
    }

    /**
     * Returns a Boolean value representing whether the week is visible or not.
     *
     * @param int $sectionid id of the section
     *
     * @return boolean boolean value representing whether the section is visible or not. Return null in case of
     * the section is not found
     */
    private function get_current_visibility($sectionid)
    {
        foreach ($this->current_sections as $section) {
            if ($section['sectionid'] == $sectionid) {
                return $section['visible'];
            }
        }
        return null;
    }

    /**
     * Updates the name of a section ($section parameter) if it exists in the list of sections in the $course_sections parameter.
     * parameter $course_sections. In case the section already has a name, its name is not updated.
     *
     * @param object $section object to check for existence
     * @param object $course_sections list of sections to validate
     *
     * @return object object with the section updated
     */

    private function validate_section($section, $course_sections)
    {
        $exist = false;
        foreach ($course_sections as $key => $course_section) {
            if ($section->sectionid == $course_section['sectionid']) {
                $exist = true;
                if ($section->name != $course_section['name']) {
                    self::update_section_name($section->sectionid, $course_section['name']);
                    $section->name = $course_section['name'];
                }
                break;
            }
        }
        $section->exists = $exist;
        return $section;
    }

    /**
     * Update the name of a NoteMyProgress section.
     *
     * @param object $sectionid id of the section to be updated
     * @param object $name new section name
     *
     * @return void
     */
    private function update_section_name($sectionid, $name)
    {
        global $DB;
        $sql = "update {notemyprogress_sections} set section_name = ? where sectionid = ?";
        $DB->execute($sql, array($name, $sectionid));
    }

    /**
     * Verify if a course has NoteMyProgress weeks configured.
     *
     * @return boolean boolean value representing if the weeks have been configured
     */
    public function is_set()
    {
        $is_set = true;
        $settings = self::get_settings();
        foreach ($settings as $configured) {
            if (!$configured) {
                $is_set = false;
                break;
            }
        }
        return $is_set;
    }

    /**
     * Gets the NoteMyProgress configurations in a course
     *
     * @return array list of boolean values with course settings
     */
    public function get_settings()
    {
        $tz = self::get_timezone();
        date_default_timezone_set($tz);
        $course_start = $this->startin;
        $weeks = self::get_weeks_with_sections();
        $settings = [
            "weeks" => false,
            "course_start" => false,
            "has_students" => false
        ];
        $first_week = new stdClass();
        $first_week->has_sections = isset($weeks[0]) && !empty($weeks[0]->sections);
        $first_week->started = time() >= $course_start;
        if ($first_week->has_sections) {
            $settings['weeks'] = true;
        }
        if ($first_week->started) {
            $settings['course_start'] = true;
        }
        $students = self::get_student_ids();
        if (!empty($students)) {
            $settings['has_students'] = true;
        }
        return $settings;
    }

    /**
     * Gets a list of the sections of a course (without the NoteMyProgress week configuration)
     *
     * @return array list of course sections
     */
    public function get_sections_without_week()
    {
        $course_sections = self::get_course_sections();
        $weeks = self::get_weeks_with_sections();
        foreach ($weeks as $key => $week) {
            foreach ($week->sections as $section) {
                foreach ($course_sections as $index => $course_section) {
                    if ($course_section['sectionid'] == $section->sectionid) {
                        unset($course_sections[$index]);
                    }
                }
            }
        }
        $course_sections = array_values($course_sections);
        return $course_sections;
    }

    /**
     * Saves NoteMyProgress weeks configured in a course
     *
     * @param array $weeks weeks to save
     *
     * @return void
     * @throws Exception
     */
    public function save_weeks($weeks)
    {
        global $DB;
        self::delete_weeks();
        foreach ($weeks as $key => $week) {
            $week = self::save_week($week, $key);
            self::save_week_sections($week->weekcode, $week->sections);
        }
    }

    /**
     * Eliminates the weeks configured in a course
     *
     * @return void
     * @throws dml_exception
     */
    public function delete_weeks()
    {
        global $DB;
        $weeks = $this->weeks;
        foreach ($weeks as $week) {
            self::delete_week_sections($week->weekcode);
            $sql = "update {notemyprogress_weeks} set timedeleted = ? where id = ?";
            $DB->execute($sql, array(self::now_timestamp(), $week->id));
        }
    }

    /**
     * Eliminates sections assigned to a week
     *
     * @param string $weekcode ID of the week to eliminate
     *
     * @return void
     * @throws dml_exception
     */
    public function delete_week_sections($weekcode)
    {
        global $DB;
        $sql = "update {notemyprogress_sections} set timedeleted = ? where weekcode = ?";
        $DB->execute($sql, array(self::now_timestamp(), $weekcode));
    }

    /**
     * Save a week of Note My Progress configured in a course
     *
     * @param Object $Week week to save
     * @param int $Position position of the week
     *
     * @return void
     * @throws Exception
     */
    private function save_week($week, $position)
    {
        global $DB;
        $week->weekcode = self::generate_week_code($position);
        $week->position = $position;
        $week->weekstart = self::to_timestamp($week->s);
        $week->weekend = self::to_timestamp($week->e) + 86399;
        $week->hours_dedications = $week->h;
        $week->courseid = $this->course->id;
        $week->created_by = $this->user->id;
        $week->modified_by = $this->user->id;
        $week->timecreated = self::now_timestamp();
        $week->timemodified = self::now_timestamp();
        $week->instanceid = $this->instance->id;
        $id = $DB->insert_record("notemyprogress_weeks", $week, true);
        $week->id = $id;
        return $week;
    }

    /**
     * Save the sections assigned to a week
     *
     * @param string $Weekcode ID of the week to which the sections belong
     * @param array $sections List of Sections to Save
     *
     * @return void
     * @throws dml_exception
     */
    public function save_week_sections($weekcode, $sections)
    {
        self::delete_week_sections($weekcode);
        foreach ($sections as $position => $section) {
            self::save_week_section($section, $weekcode, $position);
        }
    }

    /**
     * Save a section assigned to a week
     *
     * @param object $section section saved
     * @param int $weekcode ID of the week to which the section belongs
     * @param int $position Position of the section
     *
     * @return void
     */
    private function save_week_section($section, $weekcode, $position)
    {
        global $DB;
        $section->sectionid = $section->sid;
        $section->section_name = self::get_section_name_from_id($section->sectionid, $position);
        $section->weekcode = $weekcode;
        $section->position = $position;
        $section->timecreated = self::now_timestamp();
        $section->timemodified = self::now_timestamp();
        $id = $DB->insert_record("notemyprogress_sections", $section, true);
        $section->id = $id;
        return $section;
    }

    /**
     * Gets the name of a given section your ID
     *
     * @param int $sectionid Section ID.
     * @param int $position Position of the section
     *
     * @return void
     */
    private function get_section_name_from_id($sectionid, $position)
    {
        global $DB;
        $result = $DB->get_record("course_sections", ["id" => $sectionid]);
        $name = self::get_section_name($result, $position);
        return $name;
    }

    /**
     * Returns the current week of the scheduled weeks of Note My progress.In case the consultation
     * is done after the course is over, the last week is returned
     *
     * @param int $last_if_course_finished Optional parameter wich makes the function return the last week configured
     * in case the course is finished
     *
     * @return Object object with the current week at the last week
     */
    public function get_current_week($last_if_course_finished = true)
    {
        $current = null;
        $now = time();
        $lastweek = null;
        foreach ($this->weeks as $week) {
            $lastweek = $week;
            if ($now >= $week->weekstart  && $now <= $week->weekend) {
                $current = $week;
                break;
            }
        }
        if ($last_if_course_finished) {
            $current = $lastweek;
        }
        return $current;
    }

    /**
     * Returns the week before the week wich indludes the current date.
     *
     * @return object Object with the week wich indludes the current date. If the week is not found, Null is returned.
     */
    public function get_past_week()
    {
        $past = null;
        $day_past_week = strtotime("-7 day", time());
        foreach ($this->weeks as $week) {
            if ($day_past_week >= $week->weekstart && $day_past_week <= $week->weekend) {
                $past = $week;
                break;
            }
        }
        return $past;
    }
    
    /**
     * Creates a paginator composed of pages. Each page is an object corresponding to a week.
     *
     * @return object paginator composed of pages for each week.
     */
    public function get_weeks_paginator()
    {
        $pages = array();
        $current_week = self::get_current_week();
        foreach ($this->weeks as $key => $week) {
            $page = new stdClass();
            $page->number = $key + 1;
            $page->weekcode = $week->weekcode;
            $page->weekid = $week->id;
            $page->weekstart = $week->weekstartlabel;
            $page->weekend = $week->weekendlabel;
            $page->selected = $week->weekcode == $current_week->weekcode ? true : false;
            $page->is_current_week = $week->weekcode == $current_week->weekcode ? true : false;
            array_push($pages, $page);
        }
        return $pages;
    }
}
