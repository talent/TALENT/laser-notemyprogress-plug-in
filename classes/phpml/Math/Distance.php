<?php

declare(strict_types=1);

namespace local_notemyprogress\phpml\Math;

interface Distance
{
    public function distance(array $a, array $b): float;
}
