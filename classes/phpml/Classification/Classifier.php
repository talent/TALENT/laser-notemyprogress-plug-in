<?php

declare(strict_types=1);

namespace local_notemyprogress\phpml\Classification;

use local_notemyprogress\phpml\Estimator;

interface Classifier extends Estimator
{
}
