<?php

declare(strict_types=1);

namespace local_notemyprogress\phpml\Clustering;

interface Clusterer
{
    public function cluster(array $samples): array;
}
