<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Config Gamification
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_notemyprogress;

require_once("lib_trait.php");

use stdClass;
use DirectoryIterator;
use core_collator;
use core_component;
use core_plugin_manager;

class configgamification {

    use \lib_trait;

    public $course;
    public $user;
    public $levelsData;

    function __construct($course, $userid){
        $this->course = self::get_course($course);
        $this->user = self::get_user($userid);
        $this->levelsData = self::get_levels_data();
        $this->levelsData = self::get_first_levels();
    }

    /**
     *verify That There Are Levels,If There Are No Creation ByDefault
     *
     * @return array A list with gaming levels of the course
     * @throws coding_exception | dml_exception
     */
    public function get_first_levels(){
        $levelsData = $this->levelsData;

        if(!$levelsData){
            $levelsData = self::create_first_levels();
            $this->levelsData = $levelsData;
        }
        return $levelsData;
    }

    /**
     * Create the first two levels (default levels) of NoteMyProgress in a course.This function is executed
     * automatically for each course.
     *
     * @return stdClass An object with the levels created
     * @throws dml_exception
     */
    private function create_first_levels(){
        global $DB;

        $levelsInfo[] = array('lvl' => 1, 'name' => 'Level 1', 'des' => '', 'points' => 0);
        $levelsInfo[] = array('lvl' => 2, 'name' => 'Level 2', 'des' => '', 'points' => 120);

        $settings = array(
            'pointsbase' => 120,
            'tit' => 'Raise your level while learning',
            'des' => 'Interact with your course make you stronger'
        );

        $levelsData = new stdClass();
        $levelsData->levelsdata = json_encode($levelsInfo);
        $levelsData->settings = json_encode($settings);
        $levelsData->rules = json_encode(self::default_rules());
        $levelsData->courseid = $this->course->id;
        $levelsData->created_by = $this->user->id;
        $levelsData->modified_by = $this->user->id;
        $levelsData->timecreated = self::now_timestamp();
        $levelsData->timemodified = self::now_timestamp();

        $id = $DB->insert_record("notemyprogress_levels_data", $levelsData, true);
        $levelsData->id = $id;
        return $levelsData;
    }

    /**
     * Gets information about the level configuration
     *
     * @return array a list with the weeks configured in a course
     */
    public function get_levels_data(){
        global $DB;
        $levelsData = $DB->get_record_select(
            "notemyprogress_levels_data",
            'courseid = ? AND timedeleted IS NULL',
            array($this->course->id));

        if($levelsData){
            $levelsData->levelsdata = json_decode($levelsData->levelsdata);
            $levelsData->settings = json_decode($levelsData->settings);
            $levelsData->rules = json_decode($levelsData->rules);

            

            }
            $sql = "SELECT enablegamification from {notemyprogress_gamification} where courseid=? AND timecreated=? ";
            $value = $DB->get_record_sql($sql, array("courseid="=>($courseid),"timecreated="=>$timecrated->maximum));

        return $levelsData;
    }

    /**
     * Save the Gaming Levels of NotemyProgress configured in a course
     *
     * @param array $ Levels Levels to Save
     *
     * @return void
     * @throws Exception
     */
    public function save_levels($levelsInfo, $settings, $rules){
        global $DB;
        self::delete_levels();
        $levelsData = new stdClass();
        $levelsData->levelsdata = ($levelsInfo);
        $levelsData->settings = ($settings);
        $levelsData->rules = ($rules);
        $levelsData->courseid = $this->course->id;
        $levelsData->created_by = $this->user->id;
        $levelsData->modified_by = $this->user->id;
        $levelsData->timecreated = self::now_timestamp();
        $levelsData->timemodified = self::now_timestamp();
        $id = $DB->insert_record("notemyprogress_levels_data", $levelsData, true);
        $levelsData->id = $id;
        return $levelsData;
    }

    public function save_enable($enable){
        global $DB;
        $param = new stdClass();
        $param->courseid = $this->course->id;
        $param->userid = $this->user->id;
        if ($enable){ $param->enablegamification =1;}else{ $param->enablegamification =0;}
        $param->timecreated = self::now_timestamp();
        $DB->insert_record("notemyprogress_gamification", $param,true);
    }
    /**
     * Eliminates the Gaming Levels of NoteMyProgress configured in a course
     *
     * @return void
     * @throws dml_exception
     */
    public function delete_levels(){
        global $DB;
        $levelsData = $this->levelsData;
        $id = $levelsData->id;
        $sql = "update {notemyprogress_levels_data} set timedeleted = ? where id = ?";
        $DB->execute($sql, array(self::now_timestamp() , $id));
    }

    /**
     * Get the default gamification rules
     *
     * @return array a list of rules configured in a course
     */
    public function default_rules() {
        $ruleset[] = array('rule' => '\mod_book\event\course_module_viewed', 'points' => 5);
        $ruleset[] = array('rule' => '\mod_page\event\course_module_viewed', 'points' => 10);
        $ruleset[] = array('rule' => '\mod_forum\event\discussion_subscription_created', 'points' => 20);
        $ruleset[] = array('rule' => '\core\event\course_viewed', 'points' => 2);
        return $ruleset;
    }

    /**
     * Get a list of events.
     */
    public final function events_list() {
        $list = [];

        $coreevents = $this->get_core_events();
        $coreevents = [get_string('coresystem') => array_reduce(array_keys($coreevents), function($carry, $prefix) use ($coreevents) {
            return array_merge($carry, array_reduce($coreevents[$prefix], function($carry, $eventclass) use ($prefix) {
                $infos = self::get_event_infos($eventclass);
                if ($infos) {
                    $name = get_string('tg_colon', 'local_notemyprogress', [
                        'a' => $prefix,
                        'b' => $infos['name']
                    ]);
                    $carry = array('name' => $name, 'event' => $infos['eventname']);
                }
                return $carry;
            }, []));
        }, [])];

        foreach ($coreevents as $event) {
            $list[] = $event;
        }

        // Get module events.
        $list = array_merge($list, self::get_events_list_from_plugintype('mod'));

        return $list;
    }

    /**
     * Get the core events.
     *
     * @return array The keys are translated subsystem names, the values are the classes.
     */
    protected function get_core_events() {
        // Add some system events.
        return [get_string('course') => [
            '\\core\\event\\course_viewed',
        ]];
    }


    /**
     * Return the info about an event.
     *
     * The key 'name' is added to contain the readable name of the event.
     * It is done here because debugging is turned off and some events use
     * deprecated strings.
     *
     * We also add the key 'isdeprecated' which indicates whether the event
     * is obsolete or not.
     *
     * @param  string $class The name of the event class.
     * @return array|false
     */
    public static function get_event_infos($class) {
        global $CFG;
        $infos = false;

        // We need to disable debugging as some events can be deprecated.
        $debuglevel = $CFG->debug;
        $debugdisplay = $CFG->debugdisplay;
        set_debugging(0, false);

        // Check that the event exists, and is not an abstract event.
        if (method_exists($class, 'get_static_info')) {
            $ref = new \ReflectionClass($class);
            if (!$ref->isAbstract()) {
                $infos = $class::get_static_info();
                $infos['name'] = method_exists($class, 'get_name_with_info') ? $class::get_name_with_info() : $class::get_name();
                $infos['isdeprecated'] = method_exists($class, 'is_deprecated') ? $class::is_deprecated() : false;
            }
        }

        // Restore debugging.
        set_debugging($debuglevel, $debugdisplay);

        return $infos;
    }

    /**
     * Get events from plugin type.
     *
     * @param string $plugintype Plugin type.
     * @return array
     */
    protected static function get_events_list_from_plugintype($plugintype) {
        $list = [];

        // Loop over each plugin of the type.
        $pluginlist = core_component::get_plugin_list($plugintype);
        foreach ($pluginlist as $plugin => $directory) {
            $component = $plugintype . '_' . $plugin;
            $events = self::get_events_list_from_plugin($component);

            // If we found events for this plugin, we add them to the list.
            if (!empty($events)) {
                //$pluginmanager = core_plugin_manager::instance();
                //$plugininfo = $pluginmanager->get_plugin_info($component);
                foreach ($events as $event) {
                    $list[] = $event;
                }
            }
        }

        return $list;
    }

    /**
     * Get the events list from a plugin.
     *
     * From 3.1 we could be using core_component::get_component_classes_in_namespace().
     *
     * @param string $component The plugin's component name.
     * @return array
     */
    protected static function get_events_list_from_plugin($component) {
        $directory = core_component::get_component_directory($component);
        $plugindirectory = $directory . '/classes/event';
        if (!is_dir($plugindirectory)) {
            return [];
        }

        // Get the plugin's events.
        $eventclasses = static::get_event_classes_from_component($component);

        $pluginmanager = core_plugin_manager::instance();
        $plugininfo = $pluginmanager->get_plugin_info($component);

        // Reduce to the participating, non-deprecated event.
        $events = array_reduce($eventclasses, function($carry, $class) use ($plugininfo) {
            $infos = self::get_event_infos($class);
            if (empty($infos)) {
                // Skip rare case where infos aren't found.
                return $carry;
            } else if ($infos['edulevel'] != \core\event\base::LEVEL_PARTICIPATING) {
                // Skip events that are not of level 'participating'.
                return $carry;
            }

//            $carry[$infos['eventname']] = get_string('tg_colon', 'local_notemyprogress', [
//                'a' => $plugininfo->displayname,
//                'b' => $infos['name']
//            ]);

            $name = get_string('tg_colon', 'local_notemyprogress', [
                'a' => $plugininfo->displayname,
                'b' => $infos['name']
            ]);

            $carry[] = array('name' => $name, 'event' => $infos['eventname']);

            return $carry;
        }, []);

        // Order alphabetically.
        //core_collator::asort($events, core_collator::SORT_NATURAL);

        return $events;
    }

    /**
     * Get the events classes from a component.
     *
     * @param string $component The component.
     * @return Array of classes. Those may not be relevant (abstract, invalid, ...)
     */
    public static function get_event_classes_from_component($component) {
        $directory = core_component::get_component_directory($component);
        $plugindirectory = $directory . '/classes/event';
        if (!is_dir($plugindirectory)) {
            return [];
        }

        $eventclasses = [];
        $diriter = new DirectoryIterator($plugindirectory);
        foreach ($diriter as $file) {
            if ($file->isDot() || $file->isDir()) {
                continue;
            }

            // It's a good idea to use the leading slashes because the event's property
            // 'eventname' includes them as well, so for consistency sake... Also we do
            // not check if the class exists because that would cause the class to be
            // autoloaded which would potentially trigger debugging messages when
            // it is deprecated.
            $name = substr($file->getFileName(), 0, -4);
            $classname = '\\' . $component . '\\event\\' . $name;
            $eventclasses[] = $classname;
        }
        return $eventclasses;
    }
    /**
     * Get the data needed to complete the graph.
     *
     * @param int $courseid the course id.
     * @return array an array containing the apropriate data well formated 
     */
    public static function get_spread_chart($courseid){
        Global $DB;
        $resultat = [];
        $levelsData = "SELECT MAX(level) as lvl from {notemyprogress_xp} where courseid=?";
        $lvl = $DB->get_record_sql($levelsData, array("courseid="=>($courseid)));
        $i = 1;
        while($i<=$lvl->lvl){
            $levelCount = "SELECT COUNT(*) as cpt from {notemyprogress_xp} where courseid=? and level=?";
            $cpt = $DB->get_record_sql($levelCount, array("courseid="=>($courseid),"level="=>($i)));
            $string = "level {$i} :";
            array_push( $resultat,[$string,intval($cpt->cpt)]);
            $i = $i+1;
        }
        return $resultat;
    }
}