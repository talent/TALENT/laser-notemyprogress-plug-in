<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $DB, $COURSE, $USER, $PAGE, $OUTPUT;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/student_gamification.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:student_gamification', $context);

$configgamification = new \local_notemyprogress\configgamification($COURSE, $USER);
$es = new \local_notemyprogress\event_strategy($COURSE, $USER);

$userid=$es->get_user_info()->uid;

$selectRankable = "SELECT rankable from {notemyprogress_xp} where courseid=? AND userid=? ";
$rankable = $DB->get_record_sql($selectRankable, array("courseid="=>($courseid),"timecreated="=>$userid));
if($rankable->rankable =='1'){
    $rankableSend = "true";
}
$content = [
    'strings' =>[
        'title' => get_string('tg_section_title', 'local_notemyprogress'),
        'help_title' => get_string('tg_section_help_title', 'local_notemyprogress'),
        'help_description' => get_string('tg_section_help_description', 'local_notemyprogress'),
        'helplabel' => get_string("helplabel","local_notemyprogress"),
        'error_network' => get_string('nmp_api_error_network', 'local_notemyprogress'),
        'save_successful' => get_string('nmp_api_save_successful', 'local_notemyprogress'),
        'cancel_action' => get_string('nmp_api_cancel_action', 'local_notemyprogress'),
        'save_warning_title' => get_string('tg_save_warning_title', 'local_notemyprogress'),
        'save_warning_content' => get_string('tg_save_warning_content', 'local_notemyprogress'),
        'confirm_ok' => get_string('tg_confirm_ok', 'local_notemyprogress'),
        'confirm_cancel' => get_string('tg_confirm_cancel', 'local_notemyprogress'),
        'exitbutton' => get_string("exitbutton","local_notemyprogress"),
        'save' => get_string("tg_save","local_notemyprogress"),
        'experience' => get_string("tg_section_experience","local_notemyprogress"),
        'information' => get_string("tg_section_information","local_notemyprogress"),
        'ranking' => get_string("tg_section_ranking","local_notemyprogress"),
        'points' => get_string("tg_section_points","local_notemyprogress"),
        'no_description' => get_string("tg_section_no_description","local_notemyprogress"),
        'description' => get_string("tg_section_description","local_notemyprogress"),
        'to_next_level' => get_string("tg_section_preview_next_level","local_notemyprogress"),
        'ranking_text' => get_string("tg_section_ranking_ranking_text","local_notemyprogress"),
        'level' => get_string("tg_section_ranking_level","local_notemyprogress"),
        'student' => get_string("tg_section_ranking_student","local_notemyprogress"),
        'total' => get_string("tg_section_ranking_total","local_notemyprogress"),
        'progress' => get_string("tg_section_ranking_progress","local_notemyprogress"),
        'rankable'=> $rankableSend,
        'studentRanking1'=>get_string("studentRanking1","local_notemyprogress"),
        'studentRanking2'=>get_string("studentRanking2","local_notemyprogress"),
        'studentRanking3'=>get_string("studentRanking3","local_notemyprogress"),
        'overview'=>get_string("overview","local_notemyprogress"),
        'chartTitle'=>get_string("gm_Chart_Title","local_notemyprogress"),
        'chartYaxis'=>get_string("gm_Chart_Y","local_notemyprogress"),
        'chart_data' => $configgamification->get_spread_chart($courseid),
        'tg_tab1'=> get_string("tg_tab1","local_notemyprogress"),
        'tg_tab2' =>get_string("tg_tab2","local_notemyprogress"),
        'tg_tab3' =>get_string("tg_tab3","local_notemyprogress")
    ],
    'levels_data' => $configgamification->get_levels_data(),
    'indicators' => $es->get_user_info(),
    'ranking' => $es->get_ranking(1),
    
    
];

$templatecontext = [
    'image' => $OUTPUT->image_url('badge', 'local_notemyprogress'),
    'rankImage' => $OUTPUT->image_url('rankImage', 'local_notemyprogress')
];

$maxTimeSql = "SELECT MAX(timecreated) as maximum from {notemyprogress_gamification} where courseid=? ";
$timecrated = $DB->get_record_sql($maxTimeSql, array("courseid="=>($courseid)));

$sql = "SELECT enablegamification from {notemyprogress_gamification} where courseid=? AND timecreated=? ";
$value = $DB->get_record_sql($sql, array("courseid="=>($courseid),"timecreated="=>$timecrated->maximum));

$PAGE->requires->js_call_amd('local_notemyprogress/student_gamification','init', ['content' => $content]);
echo $OUTPUT->header();
if ($value->enablegamification == 0){
    // echo " non available gamification value : ";
    // echo $value->enablegamification;
    // echo " timecrated :";
    // echo $timecrated->maximum; 
    echo  get_string("gamification_denied","local_notemyprogress");
}else{
   echo $OUTPUT->render_from_template('local_notemyprogress/student_gamification', $templatecontext); 
}

echo $OUTPUT->footer();