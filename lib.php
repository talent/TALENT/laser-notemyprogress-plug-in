<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @autor       Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/local/notemyprogress/locallib.php');

function local_notemyprogress_render_navbar_output(\renderer_base $renderer)
{

    global $CFG, $COURSE, $PAGE, $SESSION, $SITE, $USER;

    $items = [];

    if (isset($COURSE) && $COURSE->id <= 1) {
        return null;
    }

    $context = context_course::instance($COURSE->id);

    $configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
    $configuration_is_set = $configweeks->is_set();

    if (!has_capability('local/notemyprogress:usepluggin', $context)) {
        return null;
    }

    $hidden_for_student = !$configuration_is_set && !is_siteadmin();
    if (has_capability('local/notemyprogress:view_as_student', $context) && $hidden_for_student) {
        return null;
    }

    if (has_capability('local/notemyprogress:setweeks', $context)) {
        $text = get_string('menu_setweek', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/setweeks.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:teacher_general', $context) && $configuration_is_set) {
        $text = get_string('menu_general', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/teacher.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:teacher_sessions', $context) && $configuration_is_set) {
        $text = get_string('menu_sessions', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/sessions.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:assignments', $context) && $configuration_is_set) {
        $text = get_string('menu_assignments', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/assignments.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:grades', $context) && $configuration_is_set) {
        $text = get_string('menu_grades', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/grades.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:quiz', $context) && $configuration_is_set) {
        $text = get_string('menu_quiz', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/quiz.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:dropout', $context) && $configuration_is_set) {
        $text = get_string('menu_dropout', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/dropout.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }


    if (has_capability('local/notemyprogress:student_general', $context) && !is_siteadmin() && $configuration_is_set) {
        $text = get_string('menu_general', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/student.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:student_sessions', $context) && !is_siteadmin() && $configuration_is_set) {
        $text = get_string('menu_sessions', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/student_sessions.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    //ADD menu items
    if (has_capability('local/notemyprogress:logs', $context) && $configuration_is_set) {
        $text = get_string('menu_logs', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/logs.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    // if (has_capability('local/notemyprogress:teacher_planning', $context) && $configuration_is_set) {
    //     $text = get_string('menu_planning', 'local_notemyprogress');
    //     $url = new moodle_url('/local/notemyprogress/metareflexion.php?courseid=' . $COURSE->id);
    //     array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    // }

    if (has_capability('local/notemyprogress:student_planning', $context) && !is_siteadmin() && $configuration_is_set) {
        $text = get_string('menu_planning', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/metareflexion.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }


    if (has_capability('local/notemyprogress:teacher_gamification', $context) && $configuration_is_set) {
        $text = 'Gamification';
        $url = new moodle_url('/local/notemyprogress/gamification.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }
    if (has_capability('local/notemyprogress:student_gamification', $context) && !is_siteadmin() && $configuration_is_set) {
        $text = 'Gamification';
        $url = new moodle_url('/local/notemyprogress/student_gamification.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    if (has_capability('local/notemyprogress:auto_evaluation', $context) && $configuration_is_set) {
        $text = get_string('menu_auto_evaluation', 'local_notemyprogress');
        $url = new moodle_url('/local/notemyprogress/auto_evaluation.php?courseid=' . $COURSE->id);
        array_push($items, local_notemyprogress_new_menu_item(s($text), $url));
    }

    $params = [
        "title" => get_string('menu_main_title', 'local_notemyprogress'),
        "items" => $items
    ];
    return $renderer->render_from_template('local_notemyprogress/navbar_popover', $params);
}

function local_notemyprogress_get_fontawesome_icon_map()
{
    return [
        'local_notemyprogress:icon' => 'fa-pie-chart',
    ];
}
