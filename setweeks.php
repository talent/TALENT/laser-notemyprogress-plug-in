<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Set weeks page
 *
 * @package     local_notemyprogress
 * @author      Edisson Sigua, Bryan Aguilar
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/setweeks.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_teacher', $context);
require_capability('local/notemyprogress:setweeks', $context);

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "CONFIGURATION_COURSE_WEEK", "configuration_of_the_weeks", $actualLink, "Section where you can set up the study weeks");

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);

$content = [
    'strings' => [
        'title' => get_string('setweeks_title', 'local_notemyprogress'),
        'description' => get_string('setweeks_description', 'local_notemyprogress'),
        'sections' => get_string('setweeks_sections', 'local_notemyprogress'),
        'weeks_of_course' => get_string('setweeks_weeks_of_course', 'local_notemyprogress'),
        'add_new_week' => get_string('setweeks_add_new_week', 'local_notemyprogress'),
        'start' => get_string('setweeks_start', 'local_notemyprogress'),
        'week' => get_string('setweeks_week', 'local_notemyprogress'),
        'end' => get_string('setweeks_end', 'local_notemyprogress'),
        'save' => get_string('setweeks_save', 'local_notemyprogress'),
        'error_empty_week' => get_string('setweeks_error_empty_week', 'local_notemyprogress'),
        'error_network' => get_string('api_error_network', 'local_notemyprogress'),
        'error_conditions_setweek' => get_string('api_error_conditions_setweek', 'local_notemyprogress'),
        'save_successful' => get_string('api_save_successful', 'local_notemyprogress'),
        'enable_scroll' => get_string('setweeks_enable_scroll', 'local_notemyprogress'),
        'cancel_action' => get_string('api_cancel_action', 'local_notemyprogress'),
        'save_warning_title' => get_string('setweeks_save_warning_title', 'local_notemyprogress'),
        'save_warning_content' => get_string('setweeks_save_warning_content', 'local_notemyprogress'),
        'confirm_ok' => get_string('setweeks_confirm_ok', 'local_notemyprogress'),
        'confirm_cancel' => get_string('setweeks_confirm_cancel', 'local_notemyprogress'),
        'error_section_removed' => get_string('setweeks_error_section_removed', 'local_notemyprogress'),
        'label_section_removed' => get_string('setweeks_label_section_removed', 'local_notemyprogress'),
        'new_group_title' => get_string('setweeks_new_group_title', 'local_notemyprogress'),
        'new_group_text' => get_string('setweeks_new_group_text', 'local_notemyprogress'),
        'new_group_button_label' => get_string('setweeks_new_group_button_label', 'local_notemyprogress'),
        'time_dedication' => get_string('setweeks_time_dedication', 'local_notemyprogress'),
        'requirements_title' => get_string('plugin_requirements_title', 'local_notemyprogress'),
        'requirements_descriptions' => get_string('plugin_requirements_descriptions', 'local_notemyprogress'),
        'requirements_has_users' => get_string('plugin_requirements_has_users', 'local_notemyprogress'),
        'requirements_course_start' => get_string('plugin_requirements_course_start', 'local_notemyprogress'),
        'requirements_has_sections' => get_string('plugin_requirements_has_sections', 'local_notemyprogress'),
        'plugin_visible' => get_string('plugin_visible', 'local_notemyprogress'),
        'plugin_hidden' => get_string('plugin_hidden', 'local_notemyprogress'),
        "helplabel" => get_string("helplabel", "local_notemyprogress"),
        "exitbutton" => get_string("exitbutton", "local_notemyprogress"),
        "title_conditions" => get_string("title_conditions", "local_notemyprogress"),
    ],
    'sections' => $configweeks->get_sections_without_week(),
    'userid' => $USER->id,
    'courseid' => $courseid,
    'weeks' => $configweeks->get_weeks_with_sections(),
    'settings' => $configweeks->get_settings(),
    'timezone' => $configweeks->get_timezone(),
];

$PAGE->requires->js_call_amd('local_notemyprogress/setweeks', 'init', ['content' => $content]);

echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/setweeks', ['content' => $content]);
echo $OUTPUT->footer();
