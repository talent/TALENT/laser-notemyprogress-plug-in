<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/dropout.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_teacher', $context);
require_capability('local/notemyprogress:dropout', $context);


$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "DROPOUT", "dropout", $actualLink, "Section where potentially dropout students can be consulted");


$reports = new \local_notemyprogress\teacher($COURSE->id, $USER->id);

$scriptname = pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_FILENAME);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

//Actualise la base de données notemyprogress_clustering en cas où il y aurait un nouvel utilisateur d'inscrit entre temps
$clustering = new \local_notemyprogress\dropout($COURSE->id, $USER->id);
$clustering->generate_data();


$content = [
    'strings' => [
        "section_help_title" => get_string("td_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("td_section_help_description", "local_notemyprogress"),
        "group_students_help_title" => get_string("td_group_students_help_title", "local_notemyprogress"),
        "group_students_help_description_p1" => get_string("td_group_students_help_description_p1", "local_notemyprogress"),
        "group_students_help_description_p2" => get_string("td_group_students_help_description_p2", "local_notemyprogress"),
        "modules_access_help_title" => get_string("td_modules_access_help_title", "local_notemyprogress"),
        "modules_access_help_description_p1" => get_string("td_modules_access_help_description_p1", "local_notemyprogress"),
        "modules_access_help_description_p2" => get_string("td_modules_access_help_description_p2", "local_notemyprogress"),
        "modules_access_help_description_p3" => get_string("td_modules_access_help_description_p3", "local_notemyprogress"),
        "week_modules_help_title" => get_string("td_week_modules_help_title", "local_notemyprogress"),
        "week_modules_help_description_p1" => get_string("td_week_modules_help_description_p1", "local_notemyprogress"),
        "week_modules_help_description_p2" => get_string("td_week_modules_help_description_p2", "local_notemyprogress"),
        "week_modules_help_description_p3" => get_string("td_week_modules_help_description_p3", "local_notemyprogress"),
        "sessions_evolution_help_title" => get_string("td_sessions_evolution_help_title", "local_notemyprogress"),
        "sessions_evolution_help_description_p1" => get_string("td_sessions_evolution_help_description_p1", "local_notemyprogress"),
        "sessions_evolution_help_description_p2" => get_string("td_sessions_evolution_help_description_p2", "local_notemyprogress"),
        "sessions_evolution_help_description_p3" => get_string("td_sessions_evolution_help_description_p3", "local_notemyprogress"),
        "user_grades_help_title" => get_string("td_user_grades_help_title", "local_notemyprogress"),
        "user_grades_help_description_p1" => get_string("td_user_grades_help_description_p1", "local_notemyprogress"),
        "user_grades_help_description_p2" => get_string("td_user_grades_help_description_p2", "local_notemyprogress"),
        "user_grades_help_description_p3" => get_string("td_user_grades_help_description_p3", "local_notemyprogress"),

        "chart" => $reports->get_chart_langs(),
        "title" => get_string("menu_dropout","local_notemyprogress"),
        "no_data" => get_string("no_data", "local_notemyprogress"),
        "pagination" => get_string("pagination", "local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "pagination_name" => get_string("pagination_component_name", "local_notemyprogress"),
        "pagination_separator" => get_string("pagination_component_to", "local_notemyprogress"),
        "pagination_title" => get_string("pagination_title", "local_notemyprogress"),
        "helplabel" => get_string("helplabel", "local_notemyprogress"),
        "exitbutton" => get_string("exitbutton", "local_notemyprogress"),
        "dropout_no_data" => get_string("nmp_dropout_no_data", "local_notemyprogress"),
        "dropout_no_users_cluster" => get_string("nmp_dropout_no_users_cluster", "local_notemyprogress"),
        "generate_data_manually" => get_string("nmp_dropout_generate_data_manually", "local_notemyprogress"),
        "generating_data" => get_string("nmp_dropout_generating_data", "local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),
        "about_table" => get_string("nmp_about_table", "local_notemyprogress"),

        "cluster_label" => get_string("nmp_cluster_label", "local_notemyprogress"),
        "cluster_select" => get_string("nmp_cluster_select", "local_notemyprogress"),
        "thead_name" => get_string("thead_name", "local_notemyprogress"),
        "thead_lastname" => get_string("thead_lastname", "local_notemyprogress"),
        "thead_progress" => get_string("thead_progress", "local_notemyprogress"),
        "table_title" => get_string("nmp_dropout_table_title", "local_notemyprogress"),
        "see_profile" => get_string("nmp_dropout_see_profile", "local_notemyprogress"),
        "send_mail_to_user" => get_string("nmp_send_mail_to_user", "local_notemyprogress"),
        "send_mail_to_group" => get_string("nmp_send_mail_to_group", "local_notemyprogress"),
        "email_strings" => array(
            "validation_subject_text" => get_string("nmp_validation_subject_text","local_notemyprogress"),
            "validation_message_text" => get_string("nmp_validation_message_text","local_notemyprogress"),
            "subject" => "",
            "subject_prefix" => $COURSE->fullname,
            "subject_label" => get_string("nmp_subject_label","local_notemyprogress"),
            "message_label" => get_string("nmp_message_label","local_notemyprogress"),

            "submit_button" => get_string("nmp_submit_button","local_notemyprogress"),
            "cancel_button" => get_string("nmp_cancel_button","local_notemyprogress"),
            "emailform_title" => get_string("nmp_emailform_title","local_notemyprogress"),
            "sending_text" => get_string("nmp_sending_text","local_notemyprogress"),
            "recipients_label" => get_string("nmp_recipients_label","local_notemyprogress"),
            "mailsended_text" => get_string("nmp_mailsended_text","local_notemyprogress"),
            "api_error_network" => get_string("api_error_network", "local_notemyprogress"),

            "scriptname" => $scriptname,
        ),
        "modules_strings" => array(
            "title" => get_string("nmp_modules_access_chart_title","local_notemyprogress"),
            "modules_no_viewed" => get_string("nmp_modules_no_viewed","local_notemyprogress"),
            "modules_viewed" => get_string("nmp_modules_viewed","local_notemyprogress"),
            "modules_complete" => get_string("nmp_modules_complete","local_notemyprogress"),
            "close_button" => get_string("nmp_close_button","local_notemyprogress"),
            "modules_interaction" => get_string("nmp_modules_interaction","local_notemyprogress"),
            "modules_interactions" => get_string("nmp_modules_interactions","local_notemyprogress"),
        ),

        "student_progress_title" => get_string("nmp_dropout_student_progress_title", "local_notemyprogress"),
        "module_label" => get_string("nmp_module_label", "local_notemyprogress"),
        "modules_label" => get_string("nmp_modules_label", "local_notemyprogress"),
        "of_conector" => get_string("nmp_of_conector", "local_notemyprogress"),
        "finished_label" => get_string("nmp_finished_label", "local_notemyprogress"),
        "finisheds_label" => get_string("nmp_finisheds_label", "local_notemyprogress"),
        "inverted_time_title" => get_string("thead_time", "local_notemyprogress"),
        "count_sessions_title" => get_string("thead_sessions", "local_notemyprogress"),
        "student_grade_title" => get_string("nmp_dropout_student_grade_title", "local_notemyprogress"),
        "modules_access_chart_title" => get_string("nmp_modules_access_chart_title", "local_notemyprogress"),
        "modules_access_chart_series_total" => get_string("nmp_modules_access_chart_series_total", "local_notemyprogress"),
        "modules_access_chart_series_complete" => get_string("nmp_modules_access_chart_series_complete", "local_notemyprogress"),
        "modules_access_chart_series_viewed" => get_string("nmp_modules_access_chart_series_viewed", "local_notemyprogress"),
        "week_modules_chart_title" => get_string("nmp_week_modules_chart_title", "local_notemyprogress"),
        "modules_amount" => get_string("nmp_modules_amount", "local_notemyprogress"),
        "modules_details" => get_string("nmp_modules_details", "local_notemyprogress"),
        "sessions_evolution_chart_title" => get_string("nmp_sessions_evolution_chart_title", "local_notemyprogress"),
        "sessions_evolution_chart_xaxis1" => get_string("nmp_sessions_evolution_chart_xaxis1", "local_notemyprogress"),
        "sessions_evolution_chart_xaxis2" => get_string("nmp_sessions_evolution_chart_xaxis2", "local_notemyprogress"),
        "sessions_evolution_chart_legend1" => get_string("nmp_sessions_evolution_chart_legend1", "local_notemyprogress"),
        "sessions_evolution_chart_legend2" => get_string("nmp_sessions_evolution_chart_legend2", "local_notemyprogress"),
        "session_text" => get_string("nmp_session_text", "local_notemyprogress"),
        "sessions_text" => get_string("nmp_sessions_text", "local_notemyprogress"),
        "hours_short" => get_string("nmp_hours_short", "local_notemyprogress"),
        "minutes_short" => get_string("nmp_minutes_short", "local_notemyprogress"),
        "seconds_short" => get_string("nmp_seconds_short", "local_notemyprogress"),
        "user_grades_chart_title" => get_string("nmp_user_grades_chart_title", "local_notemyprogress"),
        "user_grades_chart_yaxis" => get_string("nmp_user_grades_chart_yaxis", "local_notemyprogress"),
        "user_grades_chart_xaxis" => get_string("nmp_user_grades_chart_xaxis", "local_notemyprogress"),
        "user_grades_chart_legend" => get_string("nmp_user_grades_chart_legend", "local_notemyprogress"),
        "user_grades_chart_tooltip_no_graded" => get_string("nmp_user_grades_chart_tooltip_no_graded", "local_notemyprogress"),
        "user_grades_chart_view_activity" => get_string("nmp_user_grades_chart_view_activity", "local_notemyprogress"),

    ],
    'modules_access_colors' => array('#FFD166', '#06D6A0', '#118AB2'),
    'week_modules_colors' => array('#FFD166', '#06D6A0'),
    'sessions_evolution_colors' => array('#118AB2', '#073B4C'),
    'user_grades_colors' => array('#118AB2', '#073B4C'),
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'dropout' => $reports->get_dropout_clusters(),
    'profile_render' => $reports->render_has(),
    'groups' => local_notemyprogress_get_groups($course, $USER),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/dropout', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/dropout', ['content' => $content]);
echo $OUTPUT->footer();