<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/time.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_teacher', $context);
require_capability('local/notemyprogress:grades', $context);

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "GRADES_MONITORING", "grades_monitoring", $actualLink, "Section where you can consult statistics on the grades that students have obtained");

$reports = new \local_notemyprogress\teacher($COURSE->id, $USER->id);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);

$scriptname = pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_FILENAME);

if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}

$content = [
    'strings' => [
        "section_help_title" => get_string("tr_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("tr_section_help_description", "local_notemyprogress"),
        "grade_items_average_help_title" => get_string("tr_grade_items_average_help_title", "local_notemyprogress"),
        "grade_items_average_help_description_p1" => get_string("tr_grade_items_average_help_description_p1", "local_notemyprogress"),
        "grade_items_average_help_description_p2" => get_string("tr_grade_items_average_help_description_p2", "local_notemyprogress"),
        "grade_items_average_help_description_p3" => get_string("tr_grade_items_average_help_description_p3", "local_notemyprogress"),
        "item_grades_details_help_title" => get_string("tr_item_grades_details_help_title", "local_notemyprogress"),
        "item_grades_details_help_description_p1" => get_string("tr_item_grades_details_help_description_p1", "local_notemyprogress"),
        "item_grades_details_help_description_p2" => get_string("tr_item_grades_details_help_description_p2", "local_notemyprogress"),
        "item_grades_distribution_help_title" => get_string("tr_item_grades_distribution_help_title", "local_notemyprogress"),
        "item_grades_distribution_help_description_p1" => get_string("tr_item_grades_distribution_help_description_p1", "local_notemyprogress"),
        "item_grades_distribution_help_description_p2" => get_string("tr_item_grades_distribution_help_description_p2", "local_notemyprogress"),
        "item_grades_distribution_help_description_p3" => get_string("tr_item_grades_distribution_help_description_p3", "local_notemyprogress"),

        "chart" => $reports->get_chart_langs(),
        "title" => get_string("menu_grades","local_notemyprogress"),
        "no_data" => get_string("no_data", "local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "helplabel" => get_string("helplabel", "local_notemyprogress"),
        "exitbutton" => get_string("exitbutton", "local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),

        "grades_select_label" => get_string("nmp_grades_select_label", "local_notemyprogress"),
        "grades_chart_title" => get_string("nmp_grades_chart_title", 'local_notemyprogress'),
        "grades_yaxis_title" => get_string("nmp_grades_yaxis_title", 'local_notemyprogress'),
        "grades_tooltip_average" => get_string("nmp_grades_tooltip_average", 'local_notemyprogress'),
        "grades_tooltip_grade" => get_string("nmp_grades_tooltip_grade", 'local_notemyprogress'),
        "grades_tooltip_student" => get_string("nmp_grades_tooltip_student", 'local_notemyprogress'),
        "grades_tooltip_students" => get_string("nmp_grades_tooltip_students", 'local_notemyprogress'),
        "grades_greater_than" => get_string("nmp_grades_distribution_greater_than", 'local_notemyprogress'),
        "grades_smaller_than" => get_string("nmp_grades_distribution_smaller_than", 'local_notemyprogress'),
        "grades_details_subtitle" => get_string("nmp_grades_details_subtitle", 'local_notemyprogress'),
        "grades_distribution_subtitle" => get_string("nmp_grades_distribution_subtitle", 'local_notemyprogress'),
        "grades_distribution_yaxis_title" => get_string("nmp_grades_distribution_yaxis_title", 'local_notemyprogress'),
        "grades_distribution_tooltip_prefix" => get_string("nmp_grades_distribution_tooltip_prefix", 'local_notemyprogress'),
        "grades_distribution_tooltip_suffix" => get_string("nmp_grades_distribution_tooltip_suffix", 'local_notemyprogress'),
        "student_text" => get_string("nmp_student_text", "local_notemyprogress"),
        "students_text" => get_string("nmp_students_text", "local_notemyprogress"),

        "email_strings" => array(
            "validation_subject_text" => get_string("nmp_validation_subject_text","local_notemyprogress"),
            "validation_message_text" => get_string("nmp_validation_message_text","local_notemyprogress"),
            "subject" => "",
            "subject_prefix" => $COURSE->fullname,
            "subject_label" => get_string("nmp_subject_label","local_notemyprogress"),
            "message_label" => get_string("nmp_message_label","local_notemyprogress"),

            "submit_button" => get_string("nmp_submit_button","local_notemyprogress"),
            "cancel_button" => get_string("nmp_cancel_button","local_notemyprogress"),
            "emailform_title" => get_string("nmp_emailform_title","local_notemyprogress"),
            "sending_text" => get_string("nmp_sending_text","local_notemyprogress"),
            "recipients_label" => get_string("nmp_recipients_label","local_notemyprogress"),
            "mailsended_text" => get_string("nmp_mailsended_text","local_notemyprogress"),
            "api_error_network" => get_string("api_error_network", "local_notemyprogress"),

            "scriptname" => $scriptname,
        ),

        "grade_item_details_categories" => array(
            get_string("nmp_grades_best_grade","local_notemyprogress"),
            get_string("nmp_grades_average_grade","local_notemyprogress"),
            get_string("nmp_grades_worst_grade","local_notemyprogress"),
        ),
        "grades_best_grade" => get_string("nmp_grades_best_grade","local_notemyprogress"),
        "grades_average_grade" => get_string("nmp_grades_average_grade","local_notemyprogress"),
        "grades_worst_grade" => get_string("nmp_grades_worst_grade","local_notemyprogress"),

        "grade_item_details_title" => "",
        "view_details" => get_string("nmp_view_details", "local_notemyprogress"),
        "send_mail" => get_string("nmp_send_mail", "local_notemyprogress"),

    ],
    'grade_items_average_colors' => array('#118AB2'),
    'item_grades_details_colors' => array('#06D6A0', '#FFD166', '#EF476F'),
    'item_grades_distribution_colors' => array('#118AB2', '#073B4C'),
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'grades' => $reports->grade_items(),
    'profile_render' => $reports->render_has(),
    'groups' => local_notemyprogress_get_groups($course, $USER),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/grades', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/grades', ['content' => $content]);
echo $OUTPUT->footer();