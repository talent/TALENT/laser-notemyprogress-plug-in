<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local notemyprogress
 *
 * @package     local_notemyprogress
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('locallib.php');
global $COURSE, $USER;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);

$url = '/local/notemyprogress/assignments.php';
local_notemyprogress_set_page($course, $url);

require_capability('local/notemyprogress:usepluggin', $context);
require_capability('local/notemyprogress:view_as_teacher', $context);
require_capability('local/notemyprogress:assignments', $context);


$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$logs = new \local_notemyprogress\logs($COURSE->id, $USER->id);
$logs->addLogsNMP("viewed", "section", "TASKS_MONITORING", "tasks_monitoring", $actualLink, "Section where you can consult the assignments that have been submitted by the students");

$reports = new \local_notemyprogress\teacher($COURSE->id, $USER->id);

$scriptname = pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_FILENAME);

$configweeks = new \local_notemyprogress\configweeks($COURSE, $USER);
if (!$configweeks->is_set()) {
    $message = get_string("weeks_not_config", "local_notemyprogress");
    print_error($message);
}



$content = [
    'strings' => [
        "section_help_title" => get_string("ta_section_help_title", "local_notemyprogress"),
        "section_help_description" => get_string("ta_section_help_description", "local_notemyprogress"),
        "assigns_submissions_help_title" => get_string("ta_assigns_submissions_help_title", "local_notemyprogress"),
        "assigns_submissions_help_description_p1" => get_string("ta_assigns_submissions_help_description_p1", "local_notemyprogress"),
        "assigns_submissions_help_description_p2" => get_string("ta_assigns_submissions_help_description_p2", "local_notemyprogress"),
        "access_content_help_title" => get_string("ta_access_content_help_title", "local_notemyprogress"),
        "access_content_help_description_p1" => get_string("ta_access_content_help_description_p1", "local_notemyprogress"),
        "access_content_help_description_p2" => get_string("ta_access_content_help_description_p2", "local_notemyprogress"),

        "title" => get_string("menu_assignments","local_notemyprogress"),
        "chart" => $reports->get_chart_langs(),
        "pagination" => get_string("pagination", "local_notemyprogress"),
        "ss_change_timezone" => get_string("ss_change_timezone", "local_notemyprogress"),
        "graph_generating" => get_string("graph_generating", "local_notemyprogress"),
        "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
        "pagination_name" => get_string("pagination_component_name","local_notemyprogress"),
        "pagination_separator" => get_string("pagination_component_to","local_notemyprogress"),
        "pagination_title" => get_string("pagination_title","local_notemyprogress"),
        "helplabel" => get_string("helplabel","local_notemyprogress"),
        "exitbutton" => get_string("exitbutton","local_notemyprogress"),
        "about" => get_string("nmp_about", "local_notemyprogress"),
        "email_strings" => array(
            "validation_subject_text" => get_string("nmp_validation_subject_text","local_notemyprogress"),
            "validation_message_text" => get_string("nmp_validation_message_text","local_notemyprogress"),
            "subject" => "",
            "subject_prefix" => $COURSE->fullname,
            "subject_label" => get_string("nmp_subject_label","local_notemyprogress"),
            "message_label" => get_string("nmp_message_label","local_notemyprogress"),

            "submit_button" => get_string("nmp_submit_button","local_notemyprogress"),
            "cancel_button" => get_string("nmp_cancel_button","local_notemyprogress"),
            "emailform_title" => get_string("nmp_emailform_title","local_notemyprogress"),
            "sending_text" => get_string("nmp_sending_text","local_notemyprogress"),
            "recipients_label" => get_string("nmp_recipients_label","local_notemyprogress"),
            "mailsended_text" => get_string("nmp_mailsended_text","local_notemyprogress"),
            "api_error_network" => get_string("api_error_network", "local_notemyprogress"),
            "scriptname" => $scriptname,
        ),

        "access" => get_string("nmp_access", "local_notemyprogress"),
        "no_access" => get_string("nmp_no_access", "local_notemyprogress"),
        "access_chart_title" => get_string("nmp_access_chart_title", "local_notemyprogress"),
        "access_chart_yaxis_label" => get_string("nmp_access_chart_yaxis_label", "local_notemyprogress"),
        "access_chart_suffix" => get_string("nmp_access_chart_suffix", "local_notemyprogress"),
        "send_mail" => get_string("nmp_send_mail", "local_notemyprogress"),
        "student_text" => get_string("nmp_student_text", "local_notemyprogress"),
        "students_text" => get_string("nmp_students_text", "local_notemyprogress"),

        "no_data" => get_string("no_data", "local_notemyprogress"),
        "assignsubs_chart_title" => get_string("nmp_assignsubs_title", "local_notemyprogress"),
        "assignsubs_chart_yaxis" => get_string("nmp_assignsubs_yaxis", "local_notemyprogress"),
    ],
    'assigns_submissions_colors' => array('#06D6A0', '#FFD166', '#EF476F'),
    'access_content_colors' => array('#06D6A0', '#EF476F'),
    'courseid' => $COURSE->id,
    'userid' => $USER->id,
    'submissions' => $reports->assignments_submissions(),
    'access' => $reports->resources_access(),
    'pages' => $configweeks->get_weeks_paginator(),
    'profile_render' => $reports->render_has(),
    'groups' => local_notemyprogress_get_groups($course, $USER),
    'timezone' => $reports->timezone,
];

$PAGE->requires->js_call_amd('local_notemyprogress/assignments', 'init', ['content' => $content]);
echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_notemyprogress/assignments', ['content' => $content]);
echo $OUTPUT->footer();