<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Code to be executed after the plugin's database scheme has been installed is defined here.
 *
 * @package     local_notemyprogress
 * @category    upgrade
 * @copyright   2020 Edisson Sigua <edissonf.sigua@gmail.com>, Bryan Aguilar <bryan.aguilar6174@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Custom code to be run on installing the plugin.
 */
function xmldb_local_notemyprogress_install()
{
    global $DB;
    //Create goals
    $goal1 = new stdClass();
    $goal2 = new stdClass();
    $goal3 = new stdClass();
    $goal1->description = "goal1";
    $goal2->description = "goal2";
    $goal3->description = "goal3";
    $DB->insert_record("notemyprogress_goals_ct", $goal1, true);
    $DB->insert_record("notemyprogress_goals_ct", $goal2, true);
    $DB->insert_record("notemyprogress_goals_ct", $goal3, true);


    //Add user questions (auto-evaluation)
    for ($i = 1; $i <= 24; $i++) {
        $question = new stdClass();
        $question->description = "qst" . $i;
        $DB->insert_record("notemyprogress_auto_qst", $question, true);
    }

    //Question 1
    $question = new stdClass();
    $question->enunciated = 'question_number_one';
    $questionid = $DB->insert_record("notemyprogress_questions", $question, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_one_option_one';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_one_option_two';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    //Question 2

    $question = new stdClass();
    $question->enunciated = 'question_number_two';
    $questionid = $DB->insert_record("notemyprogress_questions", $question, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_two_option_one';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_two_option_two';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_two_option_three';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    //Question 3

    $question = new stdClass();
    $question->enunciated = 'question_number_three';
    $questionid = $DB->insert_record("notemyprogress_questions", $question, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_three_option_one';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_three_option_two';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_three_option_three';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_three_option_four';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_three_option_five';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_three_option_six';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    //Question 4

    $question = new stdClass();
    $question->enunciated = 'question_number_four';
    $questionid = $DB->insert_record("notemyprogress_questions", $question, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_four_option_one';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_four_option_two';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_four_option_three';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    $answer = new stdClass();
    $answer->questionid = $questionid;
    $answer->enunciated = 'answers_number_four_option_four';
    $DB->insert_record("notemyprogress_answers", $answer, true);

    // TODO : rename the table FlipLearning to NoteMyProgress to keep the data from the versions older than 4.0
    //* fliplearning_clustering -> notemyprogress_clustering
    //* fliplearning_instances -> notemyprogress_instances
    //* fliplearning_logs -> notemyprogress_logs
    //* fliplearning_sections -> notemyprogress_sections
    //* fliplearning_weeks -> notemyprogress_weeks
    //? Algorithm idea :
    //? Initial state : table generated normally and old tables potentially present
    //? 1 : vefrifier if exist the old tables (Fliplearning)
    //? 2 : If they exist drop the new similar tables
    //? 3 : rename the old tables like the news that we have just deleted
    try {
        $table1 = $DB->get_record_sql("SELECT count(*) from {fliplearning_clustering}");
        $DB->execute("DROP TABLE {notemyprogress_clustering}");
        $DB->execute("RENAME TABLE {fliplearning_clustering}    TO {notemyprogress_clustering}");
    } catch (Exception $e) {
        //this table do not exist
    }
    try {
        $table1 = $DB->get_record_sql("SELECT count(*) from {fliplearning_instances}");
        $DB->execute("DROP TABLE {notemyprogress_instances}");
        $DB->execute("RENAME TABLE {fliplearning_instances}     TO {notemyprogress_instances}");
    } catch (Exception $e) {
        //this table do not exist
    }
    try {
        $table1 = $DB->get_record_sql("SELECT count(*) from {fliplearning_logs}");
        $DB->execute("DROP TABLE {notemyprogress_logs}");
        $DB->execute("RENAME TABLE {fliplearning_logs}          TO {notemyprogress_logs}");
    } catch (Exception $e) {
        //this table do not exist
    }
    try {
        $table1 = $DB->get_record_sql("SELECT count(*) from {fliplearning_sections}");
        $DB->execute("DROP TABLE {notemyprogress_sections}");
        $DB->execute("RENAME TABLE {fliplearning_sections}      TO {notemyprogress_sections}");
    } catch (Exception $e) {
        //this table do not exist
    }
    try {
        $table1 = $DB->get_record_sql("SELECT count(*) from {fliplearning_weeks}");
        $DB->execute("DROP TABLE {notemyprogress_weeks}");
        $DB->execute("RENAME TABLE {fliplearning_weeks}         TO {notemyprogress_weeks}");
    } catch (Exception $e) {
        //this table do not exist
    }
}
